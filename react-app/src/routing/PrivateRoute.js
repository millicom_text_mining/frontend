import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { getAuth } from '../context/auth';

// Función utilizada para controlar redireccionamiento a /change-password o /login según el estado del usuario
function PrivateRoute({ component: Component, ...rest }) {
    return (
        <Route
            {...rest}
            render={props =>
            // Se verifica si el usuario tiene Token
                getAuth() != null ? (
                    // Si change_password === 0, accede a la app, sino, requiere cambio de contraseña
                    getAuth().token.user.change_password === 0 ? <Component {...props} /> : <Redirect to={"/change-password"} />
                ) : (
                        // Si el usuario no tiene token, debe autenticarse
                        <Redirect to={"/login"} />
                    )} />
    );
}
export default PrivateRoute;