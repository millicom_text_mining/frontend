import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { getAuth } from '../context/auth';

/* Este componente permite el acceso a las diferentes vistas de la app basado en Roles o permisos **/

let pagesJson = {
    "modulePageUrl": "{{name}}",
    "actions": [
        {
            "action": "{{name}}",
            "actionUrl": "{{name}}"
        }
    ]
};

let pageString = JSON.stringify(pagesJson);

//Verificar si el usuario puede acceder a la vista
const checkPageAccess = (path, permittedPages) => {
    let isUserAuthorised = false;
    let url = path.replace(/\//g, "");
    let allowedActionsName = [];

    // If permittedPages exists
    if (Array.isArray(permittedPages) && permittedPages.length > 0) {
        isUserAuthorised = permittedPages.find(page => page.modulePageUrl === url);
        permittedPages.forEach(element => {
            // If Url exists
            if (element.modulePageUrl === url) {
                // If Action exists
                if ((Array.isArray(element.actions) && element.actions.length > 0)) {
                    // Prepare Action List
                    element.actions.forEach(item => {
                        allowedActionsName.push(item.actionUrl);
                    });
                }
            }
        });
    }
    return {
        isUserAuthorised,
        allowedActionsName
    };
};

//Crear el JSON con las Vistas a las que el usuario puede accede
function accessPages() {
    let dictionarySurvey;
    let journey;
    let stopwords;
    let dictionaryJourney;
    let permittedPages = [];
    if (getAuth() != null) {
        dictionarySurvey = getAuth().token.user.access.dictionary_screen.survey;
        //journey = getAuth().token.user.access.main_screen.journey_voc;
        journey = true;
        dictionaryJourney = getAuth().token.user.access.dictionary_screen.journey_voc;
        stopwords = getAuth().token.user.access.dictionary_screen.stopwords;
        if(dictionarySurvey) {
            var dictionaryName = pageString.replace("{{name}}", "testing");
            permittedPages.push(JSON.parse(dictionaryName));
        } 
        if(journey) {
            var journeyName = pageString.replace("{{name}}", "journey");
            permittedPages.push(JSON.parse(journeyName));
        } 
        if(dictionaryJourney) {
            var dictionaryJourneyName = pageString.replace("{{name}}", "testing-journey");
            permittedPages.push(JSON.parse(dictionaryJourneyName));
        } 
        if(stopwords) {
            var stopwordsName = pageString.replace("{{name}}", "stopwords");
            permittedPages.push(JSON.parse(stopwordsName));
        }
        return permittedPages;
    } else {
        return(<Redirect to={"/login"} />);
    }
}

const PrivateEditorRoute = props => {
    const { component: Component, path, stateOfuser, ...rest } = props;
    let permittedPages = accessPages();
    const checkAccess = checkPageAccess(path, permittedPages);
    //prettier-ignore
    return <Route
        {...rest}
        render={props =>
            (checkAccess.isUserAuthorised === false || typeof checkAccess.isUserAuthorised == 'undefined' ) ?
                (<Redirect to={{ pathname: "/" }} />) :
                (<Component {...props} actions={checkAccess.allowedActionsName} />)
        }
    />
};



export default PrivateEditorRoute;