import React, { Component } from 'react';
import CardStackedBar from '../components/stacked-bar/CardStackedBar';
import CardStatsMultilineChart from './../components/card-stats/CardStatsMultilineChart';
import CardFilter from './../components/card-filter/CardFilter';
import CardSummary from '../components/summary/CardSummary';
import CardWordCloudChart from './../components/word-cloud-chart/CardWordCloudChart';
import * as Constants from '../constants/constants';
import Highcharts from 'highcharts';
import axios from 'axios';
import { getAuth } from "../context/auth";
import DictionaryView from './dictionary/DictionaryView';

require('highcharts/modules/exporting.js')(Highcharts);
require('highcharts/modules/export-data.js')(Highcharts);

class Dashboard extends Component {
    constructor(props) {
        super(props);
        let authToken = getAuth();
        let defaultCountry = authToken.token.user.default_filter.country;
        let defaultChannel = authToken.token.user.default_filter.channel;
        let defaultTypeSurvey = authToken.token.user.default_filter.survey;
        this.state = {
            filterOptions: [],
            countrySelected: {
                value: defaultCountry.key,
                label: defaultCountry.value
            },
            channelSelected: {
                value: defaultChannel.key,
                label: defaultChannel.value
            },
            typeSurveySelected: [{
                value: defaultTypeSurvey.key,
                label: defaultTypeSurvey.value
            }],
            periodTimeSelected: {
                value: "six_months",
                label: "Últimos 6 meses"
            },
            dictionaryUpdateDate: null,
            segmentSelected: [],
            productTypeSelected: [],
            regionSelected: [],
            contractSelected: [],
            municipioSelected: [],
            segmentB2BSelected: [],
            filterSelected: {
                value: undefined,
                label: undefined
            }
        };
        localStorage.removeItem("Filter");
        localStorage.setItem("Filter", JSON.stringify(this.state));
        this.showDictionary = this.showDictionary.bind(this);
    }

    render() {
        return (
            <div className="content">
                <div className="row">
                    <CardFilter
                        id={'CardFilter'}
                        cardTitle={'Filtros'}
                        onClick={this.handleFilterData.bind(this)}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        filterSelected={this.state.filterSelected} />
                </div>
                {this.showDictionary()}
                <div className="row">
                    <CardSummary
                        id={'Summary'} cardTitle={'Resumen'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        typeSurveySelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected} 
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate} />

                </div>
                <div className="row">
                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Principales'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={1} />

                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Detractores'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={0} />
                </div>
                <div className="row">
                    <CardStackedBar
                        id={'CardStackedBarNPS'}
                        cardTitle={'Reporte de Tópicos Principales'}
                        size={'col-xl-12 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        contractSelected={this.state.contractSelected} 
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
                <div className="row">
                    <CardWordCloudChart
                        id={'WordCloud'}
                        cardTitle={'Nube de Frases'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}  
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        contractSelected={this.state.contractSelected} 
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
            </div>
        );
    }

    // Props es un arreglo: [0] countrySelected, [1] channelSelected, [2] typeSurveySelected, [3] periodTimeSelected
    handleFilterData(props) {
        this.setState({
            filterOptions: props,
            countrySelected: {
                value: props[0].value,
                label: props[0].label
            },
            channelSelected: {
                value: props[1].value,
                label: props[1].label
            },
            /*typeSurveySelected: {
                value: props[2].value,
                label: props[2].label
            },*/
            typeSurveySelected: props[2],
            periodTimeSelected: {
                value: props[3].value,
                label: props[3].label
            },
            segmentSelected: props[4],
            productTypeSelected: props[5],
            regionSelected: props[6],
            contractSelected: props[7],
            municipioSelected: props[8],
            segmentB2BSelected: props[9]
        },
            function () {
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
                // Se actualiza el título de Fecha Diccionario cuando se reciben props desde CardFilterDictionary
                this.getDictionaryUpdateDate();
            }
        );
    }

    showDictionary() {
        const isEditor = getAuth().token.user.access.dictionary_screen.view;
        if (isEditor) {
            return (
                <div className="row">
                    <DictionaryView
                        id={'#CardDictionary'}
                        cardTitle={'Diccionario'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected} />
                </div>
            );
        } else {
            return null
        }
    }

    getDictionaryUpdateDate() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DICTIONARY_UPDATE_DATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.state.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.state.channelSelected.label);
        for(let i = 0; i < this.state.typeSurveySelected.length; i++) {
            URL_FORMATTED += Constants.PARAM_SURVEY + encodeURIComponent(this.state.typeSurveySelected[i].label);
        }
        URL_FORMATTED += Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    const dictionaryDate = new Date(json.date * 1000);
                    const day = (dictionaryDate.getDate() < 10) ? "0" + dictionaryDate.getDate() : dictionaryDate.getDate();
                    const month = (dictionaryDate.getMonth() < 9) ? "0" + dictionaryDate.getMonth() : dictionaryDate.getMonth();
                    const hours = (dictionaryDate.getHours() < 10) ? "0" + dictionaryDate.getHours() : dictionaryDate.getHours();
                    const minutes = (dictionaryDate.getMinutes() < 10) ? "0" + dictionaryDate.getMinutes() : dictionaryDate.getMinutes();
                    const dateFull = day + "/" + (month + 1) + "/" + dictionaryDate.getFullYear() + " " + hours + ":" + minutes;
                    const dateMessage = "Última actualización en producción: " + dateFull;
                    this.setState({
                        dictionaryUpdateDate: dateMessage
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getDictionaryUpdateDate();
    }
}
export default Dashboard;