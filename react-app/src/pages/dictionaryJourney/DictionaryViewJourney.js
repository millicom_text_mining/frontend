import React, { Component } from 'react';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import '../../components/card-stats/CardStats.css';
import '../dictionary/Dictionary.css';
import downloadingGif from '../../downloading-file-black.gif';
import { getAuth } from "../../context/auth";
import $ from "jquery";
import TableDictionary from "../../components/data-table/TableDictionary";
import dataTable from "datatables.net";
import downloadIcon from '../../download-icon-black.svg';
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';

class DictionaryView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            /*journeySelected: {
                value: props.journeySelected.value,
                label: props.journeySelected.label
            },*/
            journeySelected: props.journeySelected,
            typeExecutionSelected: {
                value: "test",
                label: "Prueba"
            },
            dataColumns: [],
            dataRows: [],
            idTable: "dataTableDictionaryView",
            dictionaryUpdateDate: null,
            isLoading: false,
            tableIsLoading: false,
        };
        localStorage.removeItem("DictionaryFilter");
        localStorage.setItem("DictionaryFilter", JSON.stringify(this.state));
    }

    //Se muestra el icono de descarga o el icono de que ya se está descargando 
    showDownloadIconInTitle() {
        if (this.state.hasDictionary) {
            if (this.state.isLoading) {
                return (
                    <a href className="text-white mg-r-15 mg-t-4" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="mg-r-15" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        } else {
            return;
        }
    }

    exportDataOption() {
        if (this.props.location.pathname === "/") {
            if (this.state.isLoading) {
                return (
                    <a target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="mg-r-15" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        } else {
            return;
        }
    }

    render() {
        return (
            <div className={this.props.size}>
                <div className="card">
                    <div className="card-header">

                        {/* Título */}
                        <h4 className="card-header-title">{this.props.cardTitle}</h4>

                        {/* Fecha de última actualización */}
                        <h4 className="card-header-title text-right">

                            {this.state.dictionaryUpdateDate}
                            {/* Opción de descarga */}
                            {this.showDownloadIconInTitle()}
                        </h4>
                        <div className="card-header-btn mg-l-25">
                            <a href="#" data-toggle="collapse" className="btn card-collapse"
                                data-target="#dictionary" aria-expanded="true" title="Mostrar/Ocultar">
                                <i className="fa fa-chevron-down mg-l-5" />
                            </a>
                        </div>
                    </div>
                    <div className="collapse hide" id="dictionary">
                        <div className="card-body">
                            {this.checkTableBodyContent()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    // Método para verificar si se muestra tabla o mensaje No Diccionario
    checkTableBodyContent() {
        if (this.state.hasDictionary) {
            if (this.state.tableIsLoading) {
                return (
                    <div className="text-center">
                        <Loader
                            type={Constants.DEFAULT_SPINNER_TYPE}
                            color={Constants.DEFAULT_SPINNER_COLOR}
                            height={Constants.DEFAULT_SPINNER_HEIGHT}
                        />
                    </div>
                );
            } else {
                return (
                    <div id="Table">
                        <TableDictionary columns={this.state.dataColumns} data={this.state.dataRows} idTable={this.state.idTable} />
                    </div>);
            }
        } else {
            return (<h4><center>No se ha cargado ningún Diccionario</center></h4>);
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getDictionaryUpdateDate();
    }
    // Método para obtener la Fecha de última actualización del Diccionario de Pruebas
    getDictionaryUpdateDate() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DICTIONARY_UPDATE_DATE_ENDPOINT +
            Constants.JOURNEYS_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.state.countrySelected.label);

        for (let i = 0; i < this.state.journeySelected.length; i++) {
            URL_FORMATTED += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.state.journeySelected[i].value);
        }

        URL_FORMATTED += Constants.PARAM_DICTIONARY_TYPE;

        let dateMessage = "Última actualización en "

        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST
            dateMessage = dateMessage + "prueba: "
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION
            dateMessage = dateMessage + "producción: "
        }

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.date !== null) {
                    const dictionaryDate = new Date(json.date * 1000);
                    const day = (dictionaryDate.getDate() < 10) ? "0" + dictionaryDate.getDate() : dictionaryDate.getDate();
                    const month = (dictionaryDate.getMonth() < 9) ? "0" + (dictionaryDate.getMonth() + 1) : (dictionaryDate.getMonth() + 1);
                    const hours = (dictionaryDate.getHours() < 10) ? "0" + dictionaryDate.getHours() : dictionaryDate.getHours();
                    const minutes = (dictionaryDate.getMinutes() < 10) ? "0" + dictionaryDate.getMinutes() : dictionaryDate.getMinutes();
                    const dateFull = day + "/" + (month) + "/" + dictionaryDate.getFullYear() + " " + hours + ":" + minutes;
                    dateMessage = dateMessage + dateFull;
                    this.setState({
                        hasDictionary: true,
                        dictionaryUpdateDate: dateMessage,
                        tableIsLoading: false
                    });
                    this.getDictionaryView();
                } else {
                    this.setState({
                        hasDictionary: false,
                        dictionaryUpdateDate: "No hay Diccionario disponible",
                        tableIsLoading: false
                    });
                }
            }
            ).catch(error => {
                this.setState({
                    isLoading: false
                });
                //Mostrar mensaje de fallo en descarga de datos
                swal({
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON,
                });
            });
    }

    // Método que implementa petición GET
    getDictionaryView() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DICTIONARY_VIEW_ENDPOINT +
            Constants.JOURNEYS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + encodeURIComponent(this.state.countrySelected.value);

        for (let i = 0; i < this.state.journeySelected.length; i++) {
            URL_FORMATTED += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.state.journeySelected[i].value);
        }

        URL_FORMATTED += Constants.PARAM_DICTIONARY_TYPE;

        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION
        }

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        this.setState({
            tableIsLoading: true
        });
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.data.length !== 0) {
                    this.setState({
                        hasDictionary: true,
                        dataColumns: json.columns,
                        dataRows: json.data,
                        tableIsLoading: false
                    });
                    // convertir a datatable
                    $("#" + this.state.idTable).dataTable({
                        destroy: true,
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Buscar...',
                            sSearch: ''
                        }
                    });
                } else {
                    this.setState({
                        hasDictionary: false,
                        tableIsLoading: false
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    downloadFile = (e) => {
        // Se obtiene el token desde Auth
        const authToken = getAuth().token_id;
        // Se obtienen los filtros desde LocalStorage
        const storage = JSON.parse(localStorage.getItem("Filter"));
        // Se construye la URL
        let BASE_URL_FILE = Constants.BASE_API_URL + Constants.DOWNLOAD_DICTIONARY +
            Constants.JOURNEYS_ENDPOINT +
            Constants.PARAM_COUNTRY_CODE + encodeURIComponent(storage.countrySelected.value);

        for (let i = 0; i < this.state.journeySelected.length; i++) {
            BASE_URL_FILE += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.state.journeySelected[i].value);
        }

        BASE_URL_FILE += Constants.PARAM_DICTIONARY_TYPE;

        let fileName;
        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            BASE_URL_FILE = BASE_URL_FILE + Constants.PARAM_DICTIONARY_TYPE_TEST;
            fileName = "Dictionary_Test_";
        } else {
            BASE_URL_FILE = BASE_URL_FILE + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
            fileName = "Dictionary_Production_";
        }

        // Se construye el nombre del archivo
        fileName = fileName + (storage.countrySelected.value) + "_journey" /*+ (storage.journeySelected.label)
            + "_"*/ + Constants.EXT_EXCEL;

        this.setState({
            isLoading: true
        });
        // Se ejecuta petición GET        
        axios({
            url: BASE_URL_FILE,
            method: 'GET',
            headers: { 'authorization-tigo': authToken },
            responseType: 'blob', // important
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
            this.setState({
                isLoading: false
            });
        });
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.state.countrySelected ||
            nextProps.channelSelected !== this.state.channelSelected ||
            nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                },
                journeySelected: [{
                    value: nextProps.journeySelected.value,
                    label: nextProps.journeySelected.label
                }],
                tableIsLoading: true
            }, function () {
                // Se actualiza la sección de Diccionario cuando se reciben props desde CardFilterDictionary
                this.getDictionaryUpdateDate();
            });
        }
    }
}

export default withRouter(DictionaryView)