import React, { Component } from 'react';
import CardStackedBar from '../components/stacked-bar/CardStackedBar';
import CardStatsMultilineChart from '../components/card-stats/CardStatsMultilineChart';
import * as Constants from '../constants/constants';
import Highcharts from 'highcharts';
import axios from 'axios';
import { getAuth } from "../context/auth";
import CardFilterJourneyDictionary from '../components/card-filter/CardFilter2JourneyDictionary';
import CardSummaryJourney from '../components/summary/CardSummaryJourney';
import CardStackedBarJourney from '../components/stacked-bar/CardStackedJourney';
import DictionaryViewJourney from './dictionaryJourney/DictionaryViewJourney';
import CardWordCloudChart from '../components/word-cloud-chart/CardWordCloudChart';
import CurrentProcessJourneyView from './dictionaryJourney/CurrentProcessJourneyView';

require('highcharts/modules/exporting.js')(Highcharts);
require('highcharts/modules/export-data.js')(Highcharts);

class Journey extends Component {
    constructor(props) {
        super(props);
        let authToken = getAuth();
        let defaultCountry = authToken.token.user.default_filter.country;
        let defaultJourney = authToken.token.user.default_filter.journey_voc;
        this.state = {
            filterOptions: [],
            countrySelected: {
                value: defaultCountry.code,
                label: defaultCountry.value
            },
            journeySelected: [{
                value: defaultJourney.key,
                label: defaultJourney.value
            }],
            /*journeySelected: {
                value: defaultJourney.key,
                label: defaultJourney.value
            },*/
            periodTimeSelected: {
                value: "six_months",
                label: "Últimos 6 meses"
            },
            dictionaryUpdateDate: null,
            segmentSelected: [],
            productTypeSelected: [],
            regionSelected: [],
            contractSelected: [],
            municipioSelected:[],
            segmentB2BSelected: [],
            filterSelected: {
                value: undefined,
                label: undefined
            }
        };
        localStorage.removeItem("Filter");
        localStorage.setItem("Filter", JSON.stringify(this.state));
    }

    render() {
        return (
            <div className="content">
                <div className="row">
                    <CardFilterJourneyDictionary
                        id={'CardFilterJourney'}
                        cardTitle={'Filtros'}
                        onClick={this.handleFilterData.bind(this)}
                        countrySelected={this.state.countrySelected}
                        journeySelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        filterSelected={this.state.filterSelected} />
                </div>
                <div className="row mg-b-20">
                    <DictionaryViewJourney
                        id={'#CardDictionary'}
                        cardTitle={'Diccionario'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        journeySelected={this.state.journeySelected} />
                </div>
                <div className="row">
                    <CurrentProcessJourneyView
                        id={'#CardProcess'}
                        cardTitle={'Diccionarios en Proceso'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        journeySelected={this.state.journeySelected} />
                </div>
                <div className="row">
                    <CardStackedBarJourney
                        id={'CardStackedBarJourney'}
                        cardTitle={'NPS por Journey'}
                        size={'col-xl-12 py-4'}
                        countrySelected={this.state.countrySelected.label}
                        dynamicFilterSelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected.value}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
                <div className="row">
                    <CardSummaryJourney
                        id={'Summary_Journey'}
                        cardTitle={'Resumen'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected.label}
                        journeySelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected.value}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate} 

                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
                <div className="row">
                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Principales'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={1} />

                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Detractores'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={0} />
                </div>
                <div className="row">
                    <CardStackedBar
                        id={'CardStackedBarNPS'}
                        cardTitle={'Reporte de Tópicos Principales'}
                        size={'col-xl-12 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected} 
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />

                </div>
                <div className="row">
                    <CardWordCloudChart
                        id={'WordCloud'}
                        cardTitle={'Nube de Frases'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.journeySelected}
                        periodTimeSelected={this.state.periodTimeSelected} 
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
            </div>
        );
    }

    /* Props es un arreglo: [0] countrySelected, [1] journeySelected, [2] periodTimeSelected, [3] segmentSelected
        [4] productTypeSelected, [5] regionSelected, [6] contractSelected, [7] municipioSelected, [8] segmentB2BSelected
    **/
    handleFilterData(props) {
        this.setState({
            filterOptions: props,
            countrySelected: {
                value: props[0].value,
                label: props[0].label
            },
            /*journeySelected: {
                value: props[1].value,
                label: props[1].label
            },*/
            journeySelected: props[1],
            periodTimeSelected: {
                value: props[2].value,
                label: props[2].label
            },
            segmentSelected: props[3],
            productTypeSelected: props[4],
            regionSelected: props[5],
            contractSelected: props[6],
            municipioSelected: props[7],
            segmentB2BSelected: props[8]
        },
            function () {
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
                // Se actualiza el título de Fecha Diccionario cuando se reciben props desde CardFilterDictionary
                this.getDictionaryUpdateDate();
            }
        );
    }

    getDictionaryUpdateDate() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DICTIONARY_UPDATE_DATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.state.countrySelected.label);
        
        for(let i = 0; i < this.state.journeySelected.length; i++) {
            URL_FORMATTED += Constants.PARAM_JOURNEY + encodeURIComponent(this.state.journeySelected[i].label);
        }
        
        URL_FORMATTED += Constants.PARAM_TYPE + Constants.PARAM_DICTIONARY_TYPE_TEST;

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    const dictionaryDate = new Date(json.date * 1000);
                    const day = (dictionaryDate.getDate() < 10) ? "0" + dictionaryDate.getDate() : dictionaryDate.getDate();
                    const month = (dictionaryDate.getMonth() < 9) ? "0" + dictionaryDate.getMonth() : dictionaryDate.getMonth();
                    const hours = (dictionaryDate.getHours() < 10) ? "0" + dictionaryDate.getHours() : dictionaryDate.getHours();
                    const minutes = (dictionaryDate.getMinutes() < 10) ? "0" + dictionaryDate.getMinutes() : dictionaryDate.getMinutes();
                    const dateFull = day + "/" + (month + 1) + "/" + dictionaryDate.getFullYear() + " " + hours + ":" + minutes;
                    const dateMessage = "Última actualización en producción: " + dateFull;
                    this.setState({
                        dictionaryUpdateDate: dateMessage
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getDictionaryUpdateDate();
    }
}
export default Journey;