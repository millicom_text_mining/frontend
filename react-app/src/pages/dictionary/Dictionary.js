import React, { Component } from 'react';
import CardStackedBar from '../../components/stacked-bar/CardStackedBar';
import CardStatsMultilineChart from '../../components/card-stats/CardStatsMultilineChart';
import CardFilterDictionary from '../../components/card-filter/CardFilterDictionary';
import CardSummary from '../../components/summary/CardSummary';
import CardWordCloudChart from '../../components/word-cloud-chart/CardWordCloudChart';
import Highcharts from 'highcharts';
import { getAuth } from "../../context/auth";
import DictionaryView from './DictionaryView';
import CurrentProcessView from './CurrentProcessView';

require('highcharts/modules/exporting.js')(Highcharts);
require('highcharts/modules/export-data.js')(Highcharts);

class Dictionary extends Component {
    constructor(props) {
        super(props);
        let authToken = getAuth();
        let defaultCountry = authToken.token.user.default_filter.country;
        let defaultChannel = authToken.token.user.default_filter.channel;
        let defaultTypeSurvey = authToken.token.user.default_filter.survey;
        this.state = {
            filterOptions: [],
            countrySelected: {
                value: defaultCountry.code,
                label: defaultCountry.value
            },
            channelSelected: {
                value: defaultChannel.key,
                label: defaultChannel.value
            },
            typeSurveySelected: [{
                value: defaultTypeSurvey.key,
                label: defaultTypeSurvey.value
            }],
            periodTimeSelected: {
                value: "six_months",
                label: "Últimos 6 meses"
            },
            segmentSelected: [],
            productTypeSelected: [],
            regionSelected: [],
            contractSelected: [],
            municipioSelected: [],
            segmentB2BSelected: [],
            filterSelected: {
                value: undefined,
                label: undefined
            }
        };
        localStorage.removeItem("Filter");
        localStorage.setItem("Filter", JSON.stringify(this.state));
    }

    render() {
        return (
            <div className="content">
                <div className="row">
                    <CardFilterDictionary
                        id={'CardFilter'}
                        cardTitle={'Filtros'}
                        onClick={this.handleFilterData.bind(this)}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected}
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        filterSelected={this.state.filterSelected} />
                </div>
                <div className="row mg-b-20">
                    <DictionaryView
                        id={'#CardDictionary'}
                        cardTitle={'Diccionario'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected} />
                </div>
                <div className="row mg-b-20">
                    <CurrentProcessView
                        id={'#CardProcess'}
                        cardTitle={'Diccionarios en Proceso'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected} />
                </div>
                <div className="row">
                    <CardSummary
                        id={'Summary'}
                        cardTitle={'Resumen'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        typeSurveySelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        dictionaryUpdateDate={this.state.dictionaryUpdateDate}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />

                </div>
                <div className="row">
                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Principales'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={1} />

                    <CardStatsMultilineChart
                        id={'CardStatsMultilineChart'}
                        cardTitle={'Tópicos Detractores'}
                        size={'col-xl-6 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected}
                        typeChart={0} />
                </div>
                <div className="row">
                    {/*             <CardStats
              id={'LargeTreeMap'}
              cardTitle={'Navegación por Tópico'}
              size={'col-xl-6 py-4'}>
              <LargeTreeMap />
            </CardStats> */}

                    {/*             <CardStackedBar
              id={'CardStackedBarNPS'}
              cardTitle={'Reporte de Tópicos'}
              size={'col-xl-6 py-4'}
              countrySelected={this.state.countrySelected.label}
              typeSurveySelected={this.state.typeSurveySelected.label} />      
     */}
                    <CardStackedBar
                        id={'CardStackedBarNPS'}
                        cardTitle={'Reporte de Tópicos Principales'}
                        size={'col-xl-12 py-4'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />

                </div>
                <div className="row">
                    <CardWordCloudChart
                        id={'WordCloud'}
                        cardTitle={'Nube de Frases'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        dynamicFilterSelected={this.state.typeSurveySelected}
                        periodTimeSelected={this.state.periodTimeSelected}
                        segmentSelected={this.state.segmentSelected}
                        productTypeSelected={this.state.productTypeSelected}
                        regionSelected={this.state.regionSelected} 
                        contractSelected={this.state.contractSelected}
                        municipioSelected={this.state.municipioSelected}
                        segmentB2BSelected={this.state.segmentB2BSelected} />
                </div>
            </div>
        );
    }

    /*Props es un arreglo: [0] countrySelected, [1] channelSelected, [2] typeSurveySelected, 
                           [3] periodTimeSelected, [4] segmentSelected, [5] productTypeSelected,
                           [6] regionSelected, [7] contractSelected, [8] municipioSelected,
                           [9] segmentB2BSelected
    **/
    handleFilterData(props) {
        this.setState({
            filterOptions: props,
            countrySelected: {
                value: props[0].value,
                label: props[0].label
            },
            channelSelected: {
                value: props[1].value,
                label: props[1].label
            },
            typeSurveySelected: props[2],
            periodTimeSelected: {
                value: props[3].value,
                label: props[3].label
            },
            segmentSelected: props[4],
            productTypeSelected: props[5],
            regionSelected: props[6],
            contractSelected: props[7],
            municipioSelected: props[8],
            segmentB2BSelected: props[9]
        },
            function () {
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
            }
        );
    }
}
export default Dictionary;