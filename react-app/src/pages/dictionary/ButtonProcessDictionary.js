import React, { Component } from "react";
import * as Constants from '../../constants/constants';
import axios from 'axios';
import swal from 'sweetalert';
import './ButtonProcessDictionary.css';

export default class ButtonExecuteDictionary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    checkForm = () => {
        // Se verifican los Filtros obligatorios
        if (this.props.countrySelected != null && this.props.channelSelected != null &&
            this.props.typeSurveySelected[0] != null) {
            this.checkIsDictionaryRunning();
        } else {
            // Mensaje de información cuando no se han seleccionado todos los filtros
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_DICTIONARY_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON
            });
        }
    }

    checkIsDictionaryRunning = () => {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Validando Diccionario en ejecución..."]));
        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } }

        //Se construye la URL
        let URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_VALIDATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label);
        for(let i = 0; i < this.props.typeSurveySelected.length; i++) {
            URL_FILE += Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected[i].label);
        }

        // Se realiza petición GET para cargar archivo
        axios.get(URL_FILE, HEADERS)
            .then(response => {
                // Se verifica si se realizó la petición correctamente
                if (response.data.result.success) {
                    // Se verifica el estado del proceso del Diccionario
                    if (response.data.running) {
                        // Mensaje Error, El diccionario ya se está procesando
                        swal({
                            text: "¡Ups!, parece que en este momento se está ejecutando el Diccionario que intentas procesar.",
                            icon: Constants.ALERT_DEFAULT_WARNING_ICON,
                            button: Constants.ALERT_DEFAULT_OK_BUTTON
                        });
                    } else {
                        // Mensaje Diccionario validado correctamente
                        swal({
                            text: "Diccionario validado, clic en Continuar para procesar el Diccionario",
                            icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                            button: Constants.ALERT_DEFAULT_CONTINUE_BUTTON
                        }).then(() => {
                            // Se ejecuta método POST para cargar diccionario
                            this.processDictionary();
                        });
                    }
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    // Método para ejecutar PROCESAMIENTO DE DICCIONARIO
    processDictionary() {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Procesando Diccionario..."]));

        //Se construye la URL
        let  URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_EXECUTE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label);
        for(let i = 0; i < this.props.typeSurveySelected.length; i++) {
            URL_FILE += Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected[i].label);
        }
        URL_FILE += Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected.value);

        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } };

        axios.get(URL_FILE, HEADERS)
            .then(response => {
                if (response.data.result.success) {
                    // Mensaje Diccionario en proceso
                    swal({
                        text: Constants.ALERT_DICTIONARY_PROCESSED_TEXT,
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    }).then(() => {
                        // Se recarga página para actualizar tabla procesos
                        //window.location.reload();
                    });
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_PROCESSING_DICTIONARY_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    render() {
        const { loading } = this.state;
        return (
            <button type="button" className="btn btn-primary-process float-right" data-toggle="tooltip"
                data-trigger="hover" data-placement="bottom" title="" data-original-title="Diccionario actual"
                onClick={this.checkForm} disabled={loading}>
                {loading && (
                    <i
                        className="fa fa-spinner fa-spin"
                        style={{ marginRight: "5px" }}
                    />
                )}
                {loading && <span>Procesando Diccionario</span>}
                {!loading && <span>Procesar</span>}
            </button>
        );
    }
}