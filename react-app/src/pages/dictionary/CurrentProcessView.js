import React, { Component } from 'react';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import '../../components/card-stats/CardStats.css';
import './Dictionary.css';
import { getAuth } from "../../context/auth";
import $ from "jquery";
import dataTable from "datatables.net";
import TableProcess from "../../components/data-table/TableProcess";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';

class CurrentProcessView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            channelSelected: {
                value: props.channelSelected.value,
                label: props.channelSelected.label
            },
            /*typeSurveySelected: {
                value: props.typeSurveySelected.value,
                label: props.typeSurveySelected.label
            },*/
            typeSurveySelected: props.typeSurveySelected,
            typeExecutionSelected: {
                value: "test",
                label: "Prueba"
            },
            dataColumns: [],
            dataRows: [],
            idTable: "dataTableRunningView",
            dictionaryUpdateDate: null,
            isLoading: false,
            tableIsLoading: false,
        };
    }

    executeTimeOut() {
        // Cada 15 segundos se invoca el método getCurrentDictionaryProcess para actualizar tabla
        //window.setInterval(this.getCurrentDictionaryProcess, 10000);
        setInterval(
            function () {
                this.getCurrentDictionaryProcess();
            }
                .bind(this),
            10000
        );
    }

    render() {
        return (
            <div className={this.props.size}>
                <div className="card">
                    <div className="card-header">

                        {/* Título */}
                        <h4 className="card-header-title">{this.props.cardTitle}</h4>

                        <div className="card-header-btn mg-l-25">
                            <a href="true" data-toggle="collapse" className="btn card-collapse"
                                data-target="#process" aria-expanded="true" title="Mostrar/Ocultar">
                                <i className="fa fa-chevron-down mg-l-5" />
                            </a>
                        </div>
                    </div>
                    <div className="collapse hide" id="process">
                        <div className="card-body">
                            {this.checkTableBodyContent()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    // Método para verificar si se muestra tabla o mensaje No Diccionario
    checkTableBodyContent() {
        if (this.state.hasDictionary) {
            if (this.state.tableIsLoading) {
                return (
                    <div className="text-center">
                        <Loader
                            type={Constants.DEFAULT_SPINNER_TYPE}
                            color={Constants.DEFAULT_SPINNER_COLOR}
                            height={Constants.DEFAULT_SPINNER_HEIGHT}
                        />
                    </div>
                );
            } else {
                return (
                    <div id="Table">
                        <TableProcess columns={this.state.dataColumns} data={this.state.dataRows} idTable={this.state.idTable} />
                    </div>);
            }
        } else {
            return (<h4><center>No se está procesando ningún Diccionario</center></h4>);
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getCurrentDictionaryProcess();
        this.executeTimeOut();
    }

    // Método que implementa petición GET
    getCurrentDictionaryProcess() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DICTIONARY_RUNNING_ENDPOINT +
            "?type_execute=";

        if (this.props.location.pathname === Constants.PATH_DICTIONARY) {
            URL_FORMATTED = URL_FORMATTED + "survey";
        } else {
            URL_FORMATTED = URL_FORMATTED + "journey_voc";
        }

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        this.setState({
            tableIsLoading: true
        });
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    if (json.data.length > 0) {
                        var rows = [];
                        json.data.map((value, index) => {
                            const dictionaryDate = new Date(value[8]);
                            const day = (dictionaryDate.getDate() < 10) ? "0" + dictionaryDate.getDate() : dictionaryDate.getDate();
                            const month = (dictionaryDate.getMonth() < 9) ? "0" + (dictionaryDate.getMonth() + 1) : (dictionaryDate.getMonth() + 1);
                            const hours = (dictionaryDate.getHours() < 10) ? "0" + dictionaryDate.getHours() : dictionaryDate.getHours();
                            const minutes = (dictionaryDate.getMinutes() < 10) ? "0" + dictionaryDate.getMinutes() : dictionaryDate.getMinutes();
                            const dateFull = day + "/" + month + "/" + dictionaryDate.getFullYear() + " " + hours + ":" + minutes;
                            rows.push([value[0], value[1], value[2], value[3], value[4], value[5], value[6], value[7], dateFull, value[9], value[10]]);
                        });

                        this.setState({
                            hasDictionary: true,
                            dataColumns: json.columns,
                            dataRows: rows, //json.data,
                            tableIsLoading: false
                        });
                        // convertir a datatable
                        $("#" + this.state.idTable).dataTable({
                            destroy: true,
                            responsive: true,
                            language: {
                                searchPlaceholder: 'Buscar...',
                                sSearch: ''
                            }
                        });
                    } else {
                        this.setState({
                            hasDictionary: false,
                            tableIsLoading: false
                        });
                    }
                } else {
                    this.setState({
                        hasDictionary: false,
                        tableIsLoading: false
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.state.countrySelected ||
            nextProps.channelSelected !== this.state.channelSelected ||
            nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                },
                tableIsLoading: true
            }, function () {
                // Se actualiza la sección de Diccionario cuando se reciben props desde CardFilterDictionary
                this.getCurrentDictionaryProcess();
            });
        }
    }
}

export default withRouter(CurrentProcessView);