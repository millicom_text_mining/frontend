import React, { Component } from "react";
import * as Constants from '../../constants/constants';
import axios from 'axios';
import swal from 'sweetalert';
import './ButtonLoadDictionary.css';

export default class ButtonLoadDictionary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    //Verifica que si se ha seleccionado un diccionario y los filtros
    checkForm = () => {
        // Se verifican los Filtros obligatorios
        if (this.props.countrySelected != null && this.props.channelSelected != null &&
            this.props.typeSurveySelected[0] != null) {
            // Se verifica el archivo o diccionario
            if (this.props.selectedFile != null) {
                this.checkIsDictionaryRunning();
            } else {
                // Mensaje de información cuando no hay archivo adjunto
                swal({
                    text: Constants.ALERT_OBLIG_FILE_DICTIONARY_TEXT,
                    icon: Constants.ALERT_DEFAULT_INFO_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
            }
        } else {
            // Mensaje de información cuando no se han seleccionado todos los filtros
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_DICTIONARY_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON
            });
        }
    }

    checkIsDictionaryRunning = () => {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Validando Diccionario..."]));
        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } }

        //Se construye la URL
        let URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_VALIDATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label);
        for(let i = 0; i < this.props.typeSurveySelected.length; i++) {
            URL_FILE += Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected[i].label);
        }

        // Se realiza petición GET para cargar archivo
        axios.get(URL_FILE, HEADERS)
            .then(response => {
                // Se verifica si se realizó la petición correctamente
                if (response.data.result.success) {
                    // Se verifica el estado del proceso del Diccionario
                    if (response.data.running) {
                        // Mensaje Error, El diccionario ya se está procesando
                        swal({
                            text: Constants.ALERT_ERROR_EXECUTING_DICTIONARY_TEXT,
                            icon: Constants.ALERT_DEFAULT_WARNING_ICON,
                            button: Constants.ALERT_DEFAULT_OK_BUTTON
                        });
                    } else {
                        // Mensaje Diccionario se puede procesar
                        swal({
                            text: Constants.ALERT_SUCCESS_NO_EXECUTING_DICTIONARY,
                            icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                            button: Constants.ALERT_DEFAULT_CONTINUE_BUTTON
                        }).then(() => {
                            // Se ejecuta método POST para cargar diccionario
                            this.uploadDictionary();
                        });
                    }
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    // Se construye URL y se realiza petición POST para carga de diccionario
    executeRequest = () => {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Cargando Diccionario..."]));

        // Se ejecuta método POST para cargar diccionario
        this.uploadDictionary();
        // PRUEBA
        /*         setTimeout(() => {
                    swal({
                        text: "** Pasa validación **",
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    }).then(() => {
                        this.setState({ loading: false });
                    });
                }, 1000); */
    };

    // Método para ejecutar CARGA DE DICCIONARIO
    uploadDictionary() {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Cargando Diccionario..."]));

        // Se construye formData con los props recibidos
        let formData = new FormData();
        formData.append('file', this.props.selectedFile);
        formData.append('user_id', this.props.user_id);
        formData.append('country_code', this.props.countrySelected.value);
        formData.append('survey', this.props.typeSurveySelected.label);

        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'content-type': 'multipart/form-data' } }

        //Se construye la URL
        const URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_UPLOAD_ENDPOINT;
        // Se realiza petición POST para cargar archivo
        axios.post(URL_FILE, formData, HEADERS)
            .then(response => {
                if (response.data.result.success) {
                    // Mensaje Diccionario cargado correctamente
                    /*swal({
                        text: Constants.ALERT_DICTIONARY_LOADED_TEXT,
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_CONTINUE_BUTTON
                    }).then(() => {*/
                        // Se ejecuta método POST para procesar diccionario
                        
                        this.processDictionary();
                    //});
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_FORMAT_DICTIONARY_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    // Método para ejecutar PROCESAMIENTO DE DICCIONARIO
    processDictionary() {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Procesando Diccionario..."]));

        //Se construye la URL
        let URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_EXECUTE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label);
        for(let i = 0; i < this.props.typeSurveySelected.length; i++) {
            URL_FILE += Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected[i].label);
        }
        URL_FILE += Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected.value);

        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } };

        axios.get(URL_FILE, HEADERS)
            .then(response => {
                if (response.data.result.success) {
                    // Mensaje Diccionario en proceso
                    swal({
                        text: Constants.ALERT_DICTIONARY_PROCESSED_TEXT,
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    }).then(() => {
                        // Se recarga página para actualizar tabla procesos
                        //window.location.reload();
                    });
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_PROCESSING_DICTIONARY_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    render() {
        const { loading } = this.state;
        return (
            <button type="button" className="btn btn-primary-process float-right" data-toggle="tooltip"
                data-trigger="hover" data-placement="bottom" title="" data-original-title="Diccionario nuevo"
                onClick={this.checkForm} disabled={loading}>
                {loading && (
                    <i
                        className="fa fa-spinner fa-spin"
                        style={{ marginRight: "5px" }}
                    />
                )}
                {loading && <span>Cargando Diccionario</span>}
                {!loading && <span>Cargar y Procesar</span>}
            </button>
        );
    }
}