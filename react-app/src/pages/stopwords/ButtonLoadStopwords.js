import React, { Component } from "react";
import * as Constants from '../../constants/constants';
import axios from 'axios';
import swal from 'sweetalert';
import '../dictionary/ButtonLoadDictionary.css';

export default class ButtonLoadStopwords extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    checkForm = () => {
        // Se verifican los Filtros obligatorios
        if (this.props.countrySelected != null && this.props.channelSelected != null &&
            this.props.typeSurveySelected != null) {
            // Se verifica el archivo o stopwords
            if (this.props.selectedFile != null) {
                this.checkAreStopwordsRunning();
            } else {
                // Mensaje de información cuando no hay archivo adjunto
                swal({
                    text: Constants.ALERT_OBLIG_FILE_DICTIONARY_TEXT,
                    icon: Constants.ALERT_DEFAULT_INFO_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
            }
        } else {
            // Mensaje de información cuando no se han seleccionado todos los filtros
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_DICTIONARY_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON
            });
        }
    }

    checkAreStopwordsRunning = () => {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Validando Stopwords..."]));
        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } }

        //Se construye la URL
        const URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_VALIDATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label) +
            Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected.label);

        // Se realiza petición GET para cargar archivo
        axios.get(URL_FILE, HEADERS)
            .then(response => {
                // Se verifica si se realizó la petición correctamente
                if (response.data.result.success) {
                    // Se verifica el estado del proceso de los Stopwords
                    if (response.data.running) {
                        // Mensaje Error, Los stopwords ya se están procesando
                        swal({
                            text: Constants.ALERT_ERROR_EXECUTING_STOPWORDS_TEXT,
                            icon: Constants.ALERT_DEFAULT_WARNING_ICON,
                            button: Constants.ALERT_DEFAULT_OK_BUTTON
                        });
                    } else {
                        // Mensaje Los stopwords se pueden procesar
                        swal({
                            text: Constants.ALERT_SUCCESS_NO_EXECUTING_STOPWORDS,
                            icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                            button: Constants.ALERT_DEFAULT_CONTINUE_BUTTON
                        }).then(() => {
                            // Se ejecuta método POST para cargar stopwords
                            this.uploadStopwords();
                        });
                    }
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    // Se construye URL y se realiza petición POST para carga stopwords
    executeRequest = () => {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Cargando Stopwords..."]));

        // Se ejecuta método POST para cargar stopwords
        this.uploadStopwords();
        // PRUEBA
        /*         setTimeout(() => {
                    swal({
                        text: "** Pasa validación **",
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    }).then(() => {
                        this.setState({ loading: false });
                    });
                }, 1000); */
    };

    // Método para ejecutar CARGA DE STOPWORDS
    uploadStopwords() {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Cargando Stopwords..."]));

        // Se construye formData con los props recibidos
        let formData = new FormData();
        formData.append('file', this.props.selectedFile);
        formData.append('user_id', this.props.user_id);
        formData.append('country_code', this.props.countrySelected.value);
        formData.append('survey', this.props.typeSurveySelected.label);

        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'content-type': 'multipart/form-data' } }

        //Se construye la URL
        const URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_UPLOAD_ENDPOINT;
        // Se realiza petición POST para cargar archivo
        axios.post(URL_FILE, formData, HEADERS)
            .then(response => {
                if (response.data.result.success) {
                    // Mensaje Stopwords cargadas correctamente
                    swal({
                        text: Constants.ALERT_STOPWORDS_LOADED_TEXT,
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_CONTINUE_BUTTON
                    }).then(() => {
                        // Se ejecuta método POST para procesar stopwords
                        this.processStopwords();
                    });
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_FORMAT_STOPWORDS_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    // Método para ejecutar PROCESAMIENTO DE STOPWORDS
    processStopwords() {
        this.setState({ loading: true }, this.props.onLoadingStatus([true, "Procesando Stopwords..."]));

        //Se construye la URL
        const URL_FILE = Constants.BASE_API_URL + Constants.DICTIONARY_EXECUTE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.props.channelSelected.label) +
            Constants.PARAM_SURVEY + encodeURIComponent(this.props.typeSurveySelected.label);

        // Se definen los HEADERS
        const HEADERS = { headers: { 'authorization-tigo': this.props.token, 'Cache-Control': 'no-cache' } };

        axios.get(URL_FILE, HEADERS)
            .then(response => {
                if (response.data.result.success) {
                    // Mensaje Stopwords en proceso
                    swal({
                        text: Constants.ALERT_STOPWORDS_PROCESSED_TEXT,
                        icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    }).then(() => {
                        // Se recarga página para actualizar tabla procesos
                        //window.location.reload();
                    });
                } else {
                    // Mensaje Error, intentar nuevamente o más tarde
                    swal({
                        text: Constants.ALERT_ERROR_PROCESSING_STOPWORDS_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON
                    });
                }
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            })
            .catch(error => {
                swal({
                    // Mensaje Error, intentar nuevamente o más tarde
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                });
                this.setState({ loading: false }, this.props.onLoadingStatus([false]));
            });
    }

    render() {
        const { loading } = this.state;
        return (
            <button type="button" className="btn btn-primary-process float-right" data-toggle="tooltip"
                data-trigger="hover" data-placement="bottom" title="" data-original-title="Nuevas stopwords"
                onClick={this.checkForm} disabled={loading}>
                {loading && (
                    <i
                        className="fa fa-spinner fa-spin"
                        style={{ marginRight: "5px" }}
                    />
                )}
                {loading && <span>Cargando Stopwords</span>}
                {!loading && <span>Cargar</span>}
            </button>
        );
    }
}