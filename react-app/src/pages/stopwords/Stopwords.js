import React, { Component } from 'react';
import { getAuth } from "../../context/auth";
import CardFilterStopwords from '../../components/card-filter/CardFilterStopwords';
import StopwordsView from './StopwordsView';

class Stopwords extends Component {
    constructor(props) {
        super(props);
        let authToken = getAuth();
        let defaultCountry = authToken.token.user.default_filter.country;
        let defaultChannel = authToken.token.user.default_filter.channel;
        let defaultTypeSurvey = authToken.token.user.default_filter.survey;
        this.state = {
            filterOptions: [],
            countrySelected: {
                value: defaultCountry.code,
                label: defaultCountry.value
            },
            channelSelected: {
                value: defaultChannel.key,
                label: defaultChannel.value
            },
            typeSurveySelected: {
                value: defaultTypeSurvey.key,
                label: defaultTypeSurvey.value
            }
        };
        localStorage.removeItem("Filter");
        localStorage.setItem("Filter", JSON.stringify(this.state));
    }

    render() {
        return (
            <div className="content">
                <div className="row">
                    <CardFilterStopwords
                        id={'CardFilter'}
                        cardTitle={'Filtros'}
                        onClick={this.handleFilterData.bind(this)}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected} />
                </div>
                <div className="row mg-b-20">
                    <StopwordsView
                        id={'#CardStopwords'}
                        cardTitle={'Stop Words'}
                        size={'col-xl-12'}
                        countrySelected={this.state.countrySelected}
                        channelSelected={this.state.channelSelected}
                        typeSurveySelected={this.state.typeSurveySelected} />
                </div>
            </div>
        );
    }

    // Props es un arreglo: [0] countrySelected, [1] channelSelected, [2] typeSurveySelected
    handleFilterData(props) {
        this.setState({
            filterOptions: props,
            countrySelected: {
                value: props[0].value,
                label: props[0].label
            },
            channelSelected: {
                value: props[1].value,
                label: props[1].label
            },
            typeSurveySelected: {
                value: props[2].value,
                label: props[2].label
            }
        },
            function () {
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
            }
        );
    }
}

export default Stopwords;