import React, { Component } from 'react';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import '../../components/card-stats/CardStats.css';
import '../dictionary/Dictionary.css';
import downloadingGif from '../../downloading-file-black.gif';
import { getAuth } from "../../context/auth";
import $ from "jquery";
import TableStopwords from "../../components/data-table/TableStopwords";
import downloadIcon from '../../download-icon-black.svg';
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';

class StopwordsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            channelSelected: {
                value: props.channelSelected.value,
                label: props.channelSelected.label
            },
            typeSurveySelected: {
                value: props.typeSurveySelected.value,
                label: props.typeSurveySelected.label
            },
            typeExecutionSelected: {
                value: "test",
                label: "Prueba"
            },
            dataColumns: [],
            dataRows: [],
            idTable: "dataTableStopwordsView",
            stopwordsUpdateDate: null,
            isLoading: false,
            tableIsLoading: false,
        };
        localStorage.removeItem("StopwordsFilter");
        localStorage.setItem("StopwordsFilter", JSON.stringify(this.state));
    }

    showDownloadIconInTitle() {
        if (this.state.hasStopwords) {
            if (this.state.isLoading) {
                return (
                    <a href className="text-white mg-r-15 mg-t-4" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="mg-r-15" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        } else {
            return;
        }
    }

    exportDataOption() {
        if (this.props.location.pathname === "/") {
            if (this.state.isLoading) {
                return (
                    <a target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="mg-r-15" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        } else {
            return;
        }
    }

    render() {
        return (
            <div className={this.props.size}>
                <div className="card">
                    <div className="card-header">

                        {/* Título */}
                        <h4 className="card-header-title">{this.props.cardTitle}</h4>

                        {/* Fecha de última actualización */}
                        <h4 className="card-header-title text-right">

                            {this.state.stopwordsUpdateDate}
                            {/* Opción de descarga */}
                            {this.showDownloadIconInTitle()}
                        </h4>
                    </div>
                    <div id="stopwords">
                        <div className="card-body">
                            {this.checkTableBodyContent()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    // Método para verificar si se muestra tabla o mensaje No Diccionario
    checkTableBodyContent() {
        if (this.state.hasStopwords) {
            if (this.state.tableIsLoading) {
                return (
                    <div className="text-center">
                        <Loader
                            type={Constants.DEFAULT_SPINNER_TYPE}
                            color={Constants.DEFAULT_SPINNER_COLOR}
                            height={Constants.DEFAULT_SPINNER_HEIGHT}
                        />
                    </div>
                );
            } else {
                return (
                    <div id="Table">
                        <TableStopwords columns={this.state.dataColumns} data={this.state.dataRows} idTable={this.state.idTable} />
                    </div>);
            }
        } else {
            return (<h4><center>No se ha cargado ningún archivo de Stop Words</center></h4>);
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getStopwordsUpdateDate();
    }
    // Método para obtener la Fecha de última actualización del Diccionario de Pruebas
    getStopwordsUpdateDate() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.STOPWORDS_UPDATE_DATE_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.state.countrySelected.label) +
            Constants.PARAM_CHANNEL + encodeURIComponent(this.state.channelSelected.label) +
            Constants.PARAM_SURVEY + encodeURIComponent(this.state.typeSurveySelected.label) +
            Constants.PARAM_DICTIONARY_TYPE

        let dateMessage = "Última actualización en "

        if (this.props.location.pathname === Constants.PATH_STOPWORDS) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST
            dateMessage = dateMessage + "prueba: "
        }

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.date !== null) {
                    const dictionaryDate = new Date(json.date * 1000);
                    const day = (dictionaryDate.getDate() < 10) ? "0" + dictionaryDate.getDate() : dictionaryDate.getDate();
                    const month = (dictionaryDate.getMonth() < 9) ? "0" + (dictionaryDate.getMonth() + 1) : (dictionaryDate.getMonth() + 1);
                    const hours = (dictionaryDate.getHours() < 10) ? "0" + dictionaryDate.getHours() : dictionaryDate.getHours();
                    const minutes = (dictionaryDate.getMinutes() < 10) ? "0" + dictionaryDate.getMinutes() : dictionaryDate.getMinutes();
                    const dateFull = day + "/" + (month) + "/" + dictionaryDate.getFullYear() + " " + hours + ":" + minutes;
                    dateMessage = dateMessage + dateFull;
                    this.setState({
                        hasStopwords: true,
                        stopwordsUpdateDate: dateMessage,
                        tableIsLoading: false
                    });
                    this.getStopwordsView();
                } else {
                    this.setState({
                        hasStopwords: false,
                        stopwordsUpdateDate: "No hay Diccionario disponible",
                        tableIsLoading: false
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Método que implementa petición GET
    getStopwordsView() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.STOPWORDS_UPDATE_DATE_ENDPOINT +
            Constants.PARAM_COUNTRY_CODE + encodeURIComponent(this.state.countrySelected.value) +
            Constants.PARAM_CHANNEL_ID + encodeURIComponent(this.state.channelSelected.value) +
            Constants.PARAM_SURVEY_ID + encodeURIComponent(this.state.typeSurveySelected.value) +
            Constants.PARAM_DICTIONARY_TYPE;

        if (this.props.location.pathname === Constants.PATH_STOPWORDS) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST
        }

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        this.setState({
            tableIsLoading: true
        });
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.data.length !== 0) {
                    this.setState({
                        hasStopwords: true,
                        dataColumns: json.columns,
                        dataRows: json.data,
                        tableIsLoading: false
                    });
                    // convertir a datatable
                    $("#" + this.state.idTable).dataTable({
                        destroy: true,
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Buscar...',
                            sSearch: ''
                        }
                    });
                } else {
                    this.setState({
                        hasStopwords: false,
                        tableIsLoading: false
                    });
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    downloadFile = (e) => {
        // Se obtiene el token desde Auth
        const authToken = getAuth().token_id;
        // Se obtienen los filtros desde LocalStorage
        const storage = JSON.parse(localStorage.getItem("Filter"));
        // Se construye la URL
        let BASE_URL_FILE = Constants.BASE_API_URL + Constants.DOWNLOAD_STOPWORDS +
            Constants.PARAM_COUNTRY_CODE + encodeURIComponent(storage.countrySelected.value) +
            Constants.PARAM_CHANNEL_ID + encodeURIComponent(storage.channelSelected.value) +
            Constants.PARAM_SURVEY_ID + encodeURIComponent(storage.typeSurveySelected.value) +
            Constants.PARAM_DICTIONARY_TYPE;

        let fileName;
        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_STOPWORDS) {
            BASE_URL_FILE = BASE_URL_FILE + Constants.PARAM_DICTIONARY_TYPE_TEST;
            fileName = "Stopwords_Test_";
        }

        // Se construye el nombre del archivo
        fileName = fileName + (storage.countrySelected.value) + "_" + (storage.channelSelected.label)
            + "_" + (storage.typeSurveySelected.label) + Constants.EXT_EXCEL;
        this.setState({
            isLoading: true
        });
        // Se ejecuta petición GET        
        axios({
            url: BASE_URL_FILE,
            method: 'GET',
            headers: { 'authorization-tigo': authToken },
            responseType: 'blob', // important
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
            this.setState({
                isLoading: false
            });
        });
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.state.countrySelected ||
            nextProps.channelSelected !== this.state.channelSelected ||
            nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                },
                channelSelected: {
                    value: nextProps.channelSelected.value,
                    label: nextProps.channelSelected.label
                },
                typeSurveySelected: {
                    value: nextProps.typeSurveySelected.value,
                    label: nextProps.typeSurveySelected.label
                },
                tableIsLoading: true
            }, function () {
                // Se actualiza la sección de Diccionario cuando se reciben props desde CardFilterDictionary
                this.getStopwordsUpdateDate();
            });
        }
    }
}

export default withRouter(StopwordsView)