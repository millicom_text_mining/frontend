import { createContext, useContext } from 'react';
import * as Constants from '../constants/constants';
import axios from 'axios';

export const AuthContext = createContext();

export function useAuth() {
    return useContext(AuthContext);
}

//Obtener el Token
export function getAuth() {
    return JSON.parse(localStorage.getItem(Constants.TOKEN));
}

//Limpiar el local storage
export function clearLocalStorage(){    
    localStorage.clear();
    window.location.reload();
}

//Refrescar el token
export function refreshToken() {
    const TOKEN_REFRESH_URL = Constants.BASE_API_URL + Constants.REFRESH_TOKEN_ENDPOINT;
    const authToken = getAuth().token_id;
    const HEADERS = { headers: { 'authorization-tigo': authToken } }
    axios.post(TOKEN_REFRESH_URL, null, HEADERS)
        .then(result => {
            if (result.status === 200) {
                localStorage.removeItem(Constants.TOKEN);
                localStorage.setItem(Constants.TOKEN, JSON.stringify(result.data));
            } else {
                this.clearLocalStorage();
            }
        }).catch(e => {
            
        });
}