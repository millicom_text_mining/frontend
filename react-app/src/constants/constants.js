// Base URL
export const BASE_API_URL = "http://api.textmining-stg-web.tigocloud.net/v1";

//File Format
export const EXT_EXCEL = ".xlsx";

//PATHS
export const PATH_DASHBOARD = "/";
export const PATH_DICTIONARY = "/testing";
export const PATH_JOURNEY = "/journey";
export const PATH_LOGIN = "/login";
export const PATH_CHANGE_PASS = "/change-password";
export const PATH_DICTIONARY_JOURNEY = "/testing-journey";
export const PATH_STOPWORDS = "/stopwords";
export const PAGE_DASHBOARD = "dashboard_page"
export const PAGE_DICTIONARY = "dictionary_page"

// ENDPOINTS
export const STOPWORDS_UPDATE_DATE_ENDPOINT = "stopwords/view";
export const DOWNLOAD_STOPWORDS = "/export/stopwords/excel";
export const JOURNEYS_ENDPOINT = "/journey/voc";
export const COUNTRIES_ENDPOINT = "/countries";
export const CHANNELS_ENDPOINT = "/channels";
export const SURVEYS_ENDPOINT = "/surveys";
export const SUMMARY_ENDPOINT = "/summary";
export const LOGIN_ENDPOINT = "/login";
export const CHANGE_PASSWORD_ENDPOINT = "/user/change_password";
export const REFRESH_TOKEN_ENDPOINT = "/refresh_token";
export const TOTAL_TOPICS_ENDPOINT = "/topic/weight";
export const TOTAL_TOPICS_DETRACTORS_ENDPOINT = "/topic/detractors";
export const TOPICS_BAR_ENDPOINT = "/topic/nps";
export const WORDCLOUD_ENDPOINT = "/word/cloud";
export const DICTIONARY_UPLOAD_ENDPOINT = "/dictionary/upload";
export const DICTIONARY_VIEW_ENDPOINT = "/dictionary/view";
export const DICTIONARY_UPDATE_DATE_ENDPOINT = "/dictionary/update_date";
export const DICTIONARY_EXECUTE_ENDPOINT = "/tm/execute";
export const DICTIONARY_RUNNING_ENDPOINT = "/tm/running";
export const DICTIONARY_VALIDATE_ENDPOINT = "/tm/validate";
export const DOWNLOAD_DICTIONARY = "/export/dictionary/excel";
export const DOWNLOAD_DATA = "/export/survey/excel";
export const DOWNLOAD_DATA_JOURNEY = "/export/journey/voc/excel";
export const DYNAMIC_FILTERS_ENDPOINT = "/filter/dynamic";
export const FILTER_OPTIONS_ENDPOINT = "/filter";

// PARAM
export const PARAM_JOURNEY = "&journey_voc=";
export const PARAM_JOURNEY_ID = "&journey_voc_id=";
export const PARAM_COUNTRY_ID = "?country_id=";
export const PARAM_COUNTRY_CODE = "?country_code=";
export const PARAM_COUNTRY_ID_DICTIONARY = "&country_id=";
export const PARAM_COUNTRY = "?country=";
export const PARAM_CHANNEL_ID = "&channel_id=";
export const PARAM_CHANNEL = "&channel=";
export const PARAM_SURVEY = "&survey=";
export const PARAM_SURVEY_ID = "&survey_id=";
export const PARAM_PERIOD = "&period=";
export const PARAM_CODE_COUNTRY = "?type=code";
export const PARAM_TYPE = "&type=";
export const PARAM_DICTIONARY_TYPE = "&dictionary_type=";
export const PARAM_DICTIONARY_TYPE_TEST = "test";
export const PARAM_DICTIONARY_TYPE_PRODUCTION = "production";
export const PARAM_FIELD = "?field=";
export const PARAM_FIELD_CONCAT = "&field=";
export const PARAM_SEGMENT = "&segmento=";
export const PARAM_PRODUCT_TYPE = "&tipo_producto=";
export const PARAM_REGION = "&region=";
export const PARAM_CONTRACT="&tipo_contrato=";
export const PARAM_MUNICIPIO="&municipio=";
export const PARAM_SEGMENTB2B="&segmento_b2b=";


// Login params data user
export const PARAM_LOGIN_USERNAME = "user_name";
export const PARAM_LOGIN_PASSWORD = "password";
export const PARAM_CHANGE_PASS_NEWPASS = "new_password";
export const PARAM_CHANGE_PASS_CONFIRMPASS = "confirm_password";

// Token Header param
export const AUTHORIZATION = "authorization-tigo";
export const TOKEN = "token";

// Color por defecto en Gráficas
export const DEFAULT_COLOR_YELLOW = "#FDFF00";
export const DEFAULT_COLOR_BLUE = "#396BB4";
export const DEFAULT_COLOR_RED = "#DF5353";

// Configuración de Loader
export const DEFAULT_SPINNER_TYPE = "Bars";
export const DEFAULT_SPINNER_COLOR = "#000640";
export const DEFAULT_SPINNER_HEIGHT = 300;

// *** Mensajes Sweet Alert en Diccionario *** //
// Default Swal values
export const ALERT_DEFAULT_OK_BUTTON = "Ok";
export const ALERT_DEFAULT_CONTINUE_BUTTON = "Continuar";
export const ALERT_DEFAULT_WARNING_ICON = "warning";
export const ALERT_DEFAULT_SUCCESS_ICON = "success";
export const ALERT_DEFAULT_INFO_ICON = "info";
export const ALERT_DEFAULT_ERROR_ICON = "error";

// Campos obligatorios
export const ALERT_OBLIG_FIELDS_TEXT = "Todos los campos son obligatorios";

// Campos obligatorios Diccionario
export const ALERT_OBLIG_FIELDS_DICTIONARY_TEXT = "Los campos: País, Canal y Tipo de encuesta son obligatorios";
export const ALERT_OBLIG_FILE_DICTIONARY_TEXT = "Es necesario adjuntar el archivo";

// Mensaje de error
export const ALERT_ERROR_TEXT = "¡Ups!, ha ocurrido un error. Por favor intenta nuevamente o más tarde";
export const ALERT_ERROR_UPLOADING_DICTIONARY_TEXT = "¡Ups!, ha ocurrido un error con la CARGA del diccionario. \n Intenta nuevamente o más tarde";
export const ALERT_ERROR_PROCESSING_DICTIONARY_TEXT = "¡Ups!, ha ocurrido un error con el PROCESAMIENTO del diccionario. \n Intenta nuevamente o más tarde";
export const ALERT_ERROR_EXECUTING_DICTIONARY_TEXT = "¡Ups!, parece que en este momento se está ejecutando el Diccionario que intentas procesar";
export const ALERT_ERROR_FORMAT_DICTIONARY_TEXT = "¡Ups!. Por favor verifique el formato y estructura del Diccionario"
export const ALERT_ERROR_EXECUTING_STOPWORDS_TEXT = "¡Ups!, parece que en este momento se están ejecutando el archivo de stopwords que intentas procesar";
export const ALERT_ERROR_FORMAT_STOPWORDS_TEXT = "¡Ups!. Por favor verifique el formato y estructura del archivo de stopwords"
export const ALERT_ERROR_PROCESSING_STOPWORDS_TEXT = "¡Ups!, ha ocurrido un error con el PROCESAMIENTO del archivo de Stopwords. \n Intenta nuevamente o más tarde";


// Mensaje de información carga/proceso Diccionario
export const ALERT_SUCCESS_NO_EXECUTING_DICTIONARY = "El Diccionario no se encuentra en proceso, clic en Continuar para cargar y procesar el Diccionario";
export const ALERT_SUCCESS_TEXT = "¡Ups!, ha ocurrido un error. Intenta nuevamente o más tarde";
export const ALERT_SUCCESS_NO_EXECUTING_STOPWORDS = "El archivo de stopwords no se encuentra en proceso, clic en Continuar para cargar y procesar el archivo";

// Mensaje de actualización de contraseña
export const ALERT_PASS_UPDATE_TEXT = "Su contraseña ha sido actualizada correctamente";

// Token expirado
export const ALERT_EXPIRED_TOKEN_TEXT = "Su sesión ha caducado por favor vuelva a iniciar sesión";

// Token próximo a vencer
export const ALERT_TOKEN_WILL_EXPIRE_TEXT = "En 2 minutos expira su sesión, ¿Desea continuar?";

// Carga de Diccionario
export const ALERT_DICTIONARY_LOADED_TEXT = "El diccionario ha sido cargado correctamente. \n Clic en Continuar para procesar últimos 4 meses";

//Carga de Stopwords
export const ALERT_STOPWORDS_LOADED_TEXT = "El archivo de stopwords ha sido cargado correctamente. \n Clic en Continuar para procesar las stopwords";

// Procesamiento de Diccionario
export const ALERT_DICTIONARY_PROCESSED_TEXT = "El diccionario se está procesando, podrá ver su estado en la sección: Diccionarios en Proceso";

// Procesamiento de Stopwords
export const ALERT_STOPWORDS_PROCESSED_TEXT = "El archivo de stopwords se está procesando";


// Campos de contraseña diferentes
export const ALERT_CHANGE_PASS_ERROR_EQUAL = "Por favor verifique que las contraseñas sean iguales";

// Contraseña menor a 6 caracteres
export const ALERT_CHANGE_PASS_ERROR_LENGTH = "La contraseña debe tener mínimo 6 caracteres";

// Error de autenticación en LOGIN
export const ALERT_LOGIN_ERROR_TEXT = "Por favor verifique su usuario y contraseña";

// Configuración Drag and Drop
export const DRAGANDDROP_DEFAULT = "Arrastrar y soltar el archivo o clic aquí";
export const DRAGANDDROP_REPLACE = "Arrastrar y soltar el archivo o clic para reemplazar";
export const DRAGANDDROP_REMOVE = "Eliminar";
export const DRAGANDDROP_ERROR = "¡Ups!. Ha ocurrido un error. Por favor verifica el archivo";
export const DRAGANDDROP_ERROR_FILE_FORMAT = "Sólo se permiten archivos con formato {{ value }}";