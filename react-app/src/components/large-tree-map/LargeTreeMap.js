import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import testData from './testDataLTM.json'
require('highcharts/modules/treemap.js')(Highcharts);
require('highcharts/modules/heatmap.js')(Highcharts);


class LargeTreeMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartOptions: {
                colorAxis: {
                    minColor: '#2548B2',
                    maxColor: Highcharts.getOptions().colors[0]
                },
                series: [{
                    type: 'treemap',
                    layoutAlgorithm: 'squarified',
                    allowDrillToNode: true,
                    animationLimit: 1000,
                    dataLabels: {
                        enabled: false
                    },
                    levelIsConstant: false,
                    levels: [{
                        level: 1,
                        dataLabels: {
                            enabled: true
                        },
                        borderWidth: 3
                    }],
                    data: null
                }],
                title: {
                    text: null
                }
            }
        };
        this.processData = this.processData.bind(this);
    }

    componentDidMount() {
        var chartData = [];
        for (let i = 0; i < testData.length; i++) {
            let item = testData[i];
            let obj = {
                'id': item.id,
                'name': item.name,
                'value': item.value,
                'level': item.level,
            };
            if (item.level === 1) {
                let indexColorFirstLevel = 0;
                obj['color'] = Highcharts.getOptions().colors[indexColorFirstLevel];
                indexColorFirstLevel++;
            }
            if (item.level === 2) {
                switch (item.name) {
                    case "Promotores":
                        obj['color'] = Highcharts.getOptions().colors[5];
                        break;
                    case "Neutros":
                        obj['color'] = Highcharts.getOptions().colors[6];
                        break;
                    case "Detractores":
                        obj['color'] = Highcharts.getOptions().colors[7];
                        break;
                    default:
                        break;
                }
            }
            chartData.push(obj);
        }
        //this.processData();
        this.setState({
            chartOptions: {
                series: [{
                    data: testData
                }]
            }
        });
    }

    processData(testData) {

        var points = [],
            regionP,
            regionVal,
            regionI = 0,
            countryP,
            countryI,
            causeP,
            causeI,
            region,
            country,
            cause,
            causeName = {
                'Communicable & other Group I': 'Communicable diseases',
                'Noncommunicable diseases': 'Non-communicable diseases',
                Injuries: 'Injuries'
            };
        for (region in testData) {
            if (testData.hasOwnProperty(region)) {
                regionVal = 0;
                regionP = {
                    id: 'id_' + regionI,
                    name: region,
                    color: Highcharts.getOptions().colors[regionI]
                };
                countryI = 0;
                for (country in testData[region]) {
                    if (testData[region].hasOwnProperty(country)) {
                        countryP = {
                            id: regionP.id + '_' + countryI,
                            name: country,
                            parent: regionP.id
                        };
                        points.push(countryP);
                        causeI = 0;
                        for (cause in testData[region][country]) {
                            if (testData[region][country].hasOwnProperty(cause)) {
                                causeP = {
                                    id: countryP.id + '_' + causeI,
                                    name: causeName[cause],
                                    parent: countryP.id,
                                    value: Math.round(+testData[region][country][cause])
                                };
                                regionVal += causeP.value;
                                points.push(causeP);
                                causeI = causeI + 1;
                            }
                        }
                        countryI = countryI + 1;
                    }
                }
                regionP.value = Math.round(regionVal / countryI);
                points.push(regionP);
                regionI = regionI + 1;
            }
        }

        this.setState({
            chartOptions: {
                series: [{
                    data: points
                }]
            }
        });
    }

    render() {
        const { chartOptions } = this.state;
        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}
export default LargeTreeMap