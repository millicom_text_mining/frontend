import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import highchartsMore from "highcharts/highcharts-more.js"
import solidGauge from "highcharts/modules/solid-gauge.js";
import * as Constants from '../../constants/constants';

highchartsMore(Highcharts);
solidGauge(Highcharts);

class TypificationComp extends Component {
    constructor(props) {
        super(props);

        // El estado recibe directamente props del componente padre para actualizar TIPIFICACIÓN en <series>
        this.state = {
            chartOptions: {
                chart: {
                    type: 'solidgauge',
                    height: 300
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG','downloadJPEG','downloadXLS']
                        }
                    }
                },
                title: {
                    text: ''
                },
                pane: {
                    center: ['50%', '70%'],
                    size: '85%',
                    startAngle: -110,
                    endAngle: 110,
                    background: {
                        innerRadius: '100%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },
                tooltip: {
                    enabled: false
                },
                // the value axis
                yAxis: {
                    min: 0,
                    max: 100,
                    // Los stops se usan para definir un color fijo dentro de un intervalo en el INDICADOR de TIPIFICACIÓN
                    stops: [
                        [0, Constants.DEFAULT_COLOR_BLUE], // blue
                        [1, Constants.DEFAULT_COLOR_BLUE] // blue
                    ],
                    lineWidth: 3,
                    minorTickInterval: 10,
                    title: {
                        y: -80
                    },
                    labels: {
                        y: 0,
                        format: '{value}%'
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: '',
                    data: [props.typificationPercent]
                }],
                plotOptions: {
                    solidgauge: {
                        innerRadius: '110%',
                        dataLabels: {
                            y: 35,
                            borderWidth: 0,
                            useHTML: true,
                            formatter: function () {
                                return '<div style="text-align:center">' +
                                    '<span style="font-size:12px;opacity:0.6">Tipificación</span><br/>' +
                                    '<span style="font-size:20px">' + this.point.y + '%</span>' +
                                    '</div>';
                            }
                        }
                    }
                }
            }
        };
    }
    // Función llamada cuando se cambian los filtros y se requiere actualizar el gráfico
    componentWillReceiveProps(nextProps) {
        this.setState({
            chartOptions: {
                series: [{
                    data: [nextProps.typificationPercent]
                }]
            }
        });
    }
    render() {
        const { chartOptions } = this.state;
        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default TypificationComp;