import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import highchartsMore from "highcharts/highcharts-more.js";
import solidGauge from "highcharts/modules/solid-gauge.js";
import * as Constants from '../../constants/constants';

highchartsMore(Highcharts);
solidGauge(Highcharts);

class NpsComp extends Component {
    constructor(props) {
        super(props);
        // El estado recibe directamente props del componente padre para actualizar NPS en <series>
        this.state = {
            chartOptions: {
                chart: {
                    type: 'solidgauge',
                    height: 300
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG','downloadJPEG','downloadXLS']
                        }
                    }
                },
                title: {
                    text: ''
                },
                pane: {
                    center: ['50%', '70%'],
                    size: '85%',
                    startAngle: -110,
                    endAngle: 110,
                    background: {
                        innerRadius: '100%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },
                tooltip: {
                    enabled: false
                },
                yAxis: {
                    min: -100,
                    max: 100,
                    // Los plotBands se usan para colorear la REGLA de medida.
                    plotBands: [{
                        from: -100,
                        to: 0,
                        color: Constants.DEFAULT_COLOR_RED // red
                    }, {
                        from: 0,
                        to: 20,
                        color: Constants.DEFAULT_COLOR_YELLOW // yellow
                    }, {
                        from: 20,
                        to: 100,
                        color: Constants.DEFAULT_COLOR_BLUE // blue
                    }],
                    // Los stops se usan para definir un color fijo dentro de un intervalo en el INDICADOR de NPS
                    stops: [
                        [0.1, Constants.DEFAULT_COLOR_RED], // red
                        [0.5, Constants.DEFAULT_COLOR_RED], // red
                        [0.5, Constants.DEFAULT_COLOR_YELLOW], // yellow
                        [0.6, Constants.DEFAULT_COLOR_YELLOW], // yellow
                        [0.6, Constants.DEFAULT_COLOR_BLUE], // blue
                        [1, Constants.DEFAULT_COLOR_BLUE] // blue
                    ],
                    lineWidth: 3,
                    minorTickInterval: 10,
                    labels: {
                        y: 0
                    }
                },
                credits: {
                    enabled: false
                },
                // Sección donde se actualiza el NPS recibido desde el componente padre
                series: [{
                    name: '',
                    data: [props.nps]
                }],
                plotOptions: {
                    solidgauge: {
                        innerRadius: '110%',
                        dataLabels: {
                            y: 35,
                            borderWidth: 0,
                            enabled: true,
                            useHTML: true,
                            formatter: function () {
                                return '<div style="text-align:center">' +
                                    '<span style="font-size:12px;opacity:0.6">NPS</span><br/>' +
                                    '<span style="font-size:20px">' + this.point.y + '</span>' +
                                    '</div>';
                            }
                        },

                    }
                }
            }
        };
    }
    // Función llamada cuando se cambian los filtros y se requiere actualizar el gráfico
    componentWillReceiveProps(nextProps) {
        this.setState({
            chartOptions: {
                series: [{
                    data: [nextProps.nps]
                }]
            }
        });
    }
    render() {
        const { chartOptions } = this.state;
        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default NpsComp;