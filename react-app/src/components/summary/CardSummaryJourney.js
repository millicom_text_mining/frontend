import React, { Component } from 'react';
import '../card-stats/CardStats.css';
import NpsComp from './NpsComp';
import * as Constants from '../../constants/constants';
import TypificationComp from './TypificationComp';
import CommentsColumnChart from './CommentsColumnChart';
import PieChartLegend from '../pie-chart/PieChart';
import axios from 'axios';
import { getAuth } from "../../context/auth";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';
import MessageError from '../message-error-chart/MessageErrorChart';


class CardSummaryJourney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: props.countrySelected,
            journeySelected: props.journeySelected,
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            nps: null,
            typificationPercent: null,
            arrCommentsData: null,
            arrPeriodTime: null,
            isLoadingSummary: true
        };
        this.updateSummaryData = this.updateSummaryData.bind(this);
        this.getSummaryData = this.getSummaryData.bind(this);
    }

    // Petición GET para obtener datos de la sección Resumen
    getSummaryData() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.SUMMARY_ENDPOINT +
            Constants.JOURNEYS_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected);

        if (this.props.journeySelected !== null) {
            for (let i = 0; i < this.props.journeySelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.props.journeySelected[i].value);
            }
        }

        URL_FORMATTED += Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected);

        if (this.props.segmentSelected !== null) {
            for (let i = 0; i < this.props.segmentSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENT + this.props.segmentSelected[i].value;
            }
        }
        if (this.props.productTypeSelected !== null) {
            for (let i = 0; i < this.props.productTypeSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_PRODUCT_TYPE + this.props.productTypeSelected[i].value;
            }
        }
        if (this.props.regionSelected !== null) {
            for (let i = 0; i < this.props.regionSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.regionSelected[i].value;
            }
        }
        if (this.props.contractSelected !== null) {
            for (let i = 0; i < this.props.contractSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_CONTRACT + this.props.contractSelected[i].value;
            }
        }
        if (this.props.municipioSelected !== null) {
            for (let i = 0; i < this.props.municipioSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_MUNICIPIO + this.props.municipioSelected[i].value;
            }
        }
        if (this.props.segmentB2BSelected !== null) {
            for (let i = 0; i < this.props.segmentB2BSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENTB2B + this.props.segmentB2BSelected[i].value;
            }
        }

        URL_FORMATTED = URL_FORMATTED + Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        //Se captura el token actual
        const authToken = getAuth().token_id;
        //Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        //Petición GET con librería Axios
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.updateSummaryData(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    // Método para cambiar estados del Gráfico y del Spinner
    updateSummaryData(graphic) {
        this.setState({
            nps: graphic.nps,
            typificationPercent: graphic.typificationPercent,
            arrCommentsData: graphic.arrCommentsData,
            arrPeriodTime: graphic.arrPeriodTime,
            //arrJourneyPie: graphic.arrJourneyPie,
            isLoadingSummary: false,
            graphicContainsData: true
        });
    }


    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.journeySelected !== this.props.journeySelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected !== this.props.segmentSelected
            || nextProps.productTypeSelected !== this.props.productTypeSelected
            || nextProps.regionSelected !== this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                journeySelected: nextProps.journeySelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                municipioSelected: nextProps.municipioSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoadingSummary: true
            }, function () { this.getSummaryData() });
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getSummaryData();
    }

    // Método para decidir renderizado de Loader o Componentes de Summary
    renderSummaryComponent(compId) {
        if (this.state.isLoadingSummary) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            );
        } else {
            if (this.state.graphicContainsData) {
                switch (compId) {
                    case 1:
                        return (
                            <NpsComp nps={this.state.nps} />
                        );
                    case 2:
                        return (
                            <TypificationComp typificationPercent={this.state.typificationPercent} />
                        );
                    case 3:
                        return (
                            <CommentsColumnChart arrCommentsData={this.state.arrCommentsData} arrPeriodTime={this.state.arrPeriodTime} />
                        );
                    default:
                        break
                }
            } else {
                return (
                    <MessageError />
                );
            }
        }
    }

    // Retorna título para Card según la página (Resumen o Diccionario)
    titleIsDictionaryPage() {
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            return <font color="red">(Ambiente de pruebas)</font>;
        } else {
            return null;
        }
    }

    // Método del ciclo de vida del componente
    render() {
        return (
            <div className={this.props.size}>
                <div className="card h-100 mg-t-20">
                    <div className="card-header">
                        <h4 className="card-header-title">{this.props.cardTitle} {this.titleIsDictionaryPage()}</h4>
                    </div>
                    <div className="card-body pd-0" id={this.props.id}>
                        <div id="chart">
                            <div className="row justify-content-center">
                                <div className="col-sm-3">
                                    {this.renderSummaryComponent(1)}
                                </div>
                                <div className="col-sm-3">
                                    {this.renderSummaryComponent(2)}
                                </div>
                                <div className="col-sm-5">
                                    <PieChartLegend
                                        countrySelected={this.props.countrySelected}
                                        journeySelected={this.props.journeySelected}
                                        periodTimeSelected={this.props.periodTimeSelected}
                                        segmentSelected={this.state.segmentSelected}
                                        productTypeSelected={this.state.productTypeSelected}
                                        regionSelected={this.state.regionSelected}
                                        contractSelected={this.state.contractSelected}
                                        municipioSelected={this.state.municipioSelected}
                                        segmentB2BSelected={this.state.segmentB2BSelected} />
                                </div>
                            </div>
                            <div className="row justify-content-center">
                                <div className="col-sm-11">
                                    {this.renderSummaryComponent(3)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
export default withRouter(CardSummaryJourney)