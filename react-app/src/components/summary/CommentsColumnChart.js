import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts'
import * as Constants from '../../constants/constants';
import imgGoBack from '../../goBack.svg';

require('highcharts/modules/drilldown.js')(Highcharts);
require('highcharts/modules/accessibility.js')(Highcharts);

class CommentsColumnChart extends Component {
    constructor(props) {
        super(props);
        // El estado recibe directamente props del componente padre para actualizar <xAxis> y <series>
        this.state = {
            chartOptions: {
                title: {
                    text: 'Comentarios válidos',
                    alignValue: 'center',
                },
                lang: {
                    drillUpText: ''
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG', 'downloadJPEG', 'downloadXLS']
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    margin: [52, 30, 60, 50],
                    padding: [0, 0, 0, 0],
                    height: 300,
                    events: {
                        load: function () {
                            var chart = this,
                                renderer = chart.renderer,
                                pattern = renderer.createElement('pattern').add(renderer.defs).attr({
                                    width: 1,
                                    height: 1,
                                    id: 'arrow'
                                });

                            renderer.image(imgGoBack, 10, 0, 45, 30).add(pattern);

                            pattern = renderer.createElement('pattern').add(renderer.defs).attr({
                                width: 1,
                                height: 1,
                                id: 'arrow'
                            });

                            renderer.rect(0, 0, 70, 32)
                                .add(pattern);
                            renderer.image(imgGoBack, 10, 0, 45, 30).add(pattern);
                        }
                    }
                },
                tooltip: {
                    enabled: false
                },
                xAxis: {
                    categories: props.arrPeriodTime,
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    gridLineColor: 'rgba(0, 0, 0, 0)',
                    labels: {
                        style: {
                            color: 'rgba(0, 0, 0, 0)'
                        }
                    }
                },
                plotOptions: {
                    series: {
                        //borderWidth: 0,
                        dataLabels: {
                            //enabled: this.state.keyPeriodTimeSelected == 0 || this.state.keyPeriodTimeSelected == 1 ? true : false,
                            enabled: true,
                            color: '#fff',
                            inside: true
                        },
                        showInLegend: false
                    }
                },
                series: [
                    {
                        data: props.arrCommentsData,
                        name: "Comentarios",
                        //colorByPoint: true,
                        color: Constants.DEFAULT_COLOR_BLUE,
                    }
                ],
                //Drill down a promotores, neutros y detractores
                drilldown: {
                    drillUpButton: {
                        relativeTo: 'spacingBox',
                        position: {
                            width: 50,
                            height: 60
                        },
                        theme: {
                            fill: 'url(#arrow)',
                            'stroke-width': 0,
                            stroke: 'white',
                            r: 2,
                            states: {
                                hover: {
                                    fill: 'url(#arrow)'
                                },
                            }
                        }
                    },
                    series: props.arrSegmentosNPS
                }
            }
        };
    }
    // Función llamada cuando se cambia el período de tiempo seleccionado
    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando las variables no cambian
        this.setState({
            chartOptions: {
                xAxis: {
                    categories: nextProps.arrPeriodTime
                },
                series: [{
                    data: nextProps.arrCommentsData
                }]
            }
        });
    }

    render() {
        const { chartOptions } = this.state;
        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default CommentsColumnChart