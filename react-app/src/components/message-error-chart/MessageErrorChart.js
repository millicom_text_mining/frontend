import React from 'react';
import "./MessageErrorChart.css";
import imgErrorConnection from '../../cloud-error-connection.png';

function MessageErrorChart() {

    return (
        <div className="imgHeight">
            <div className="centerCardError">
                <div className="row">
                    <img src={imgErrorConnection} className="wd-150 ht-150 mx-auto" alt="Error" />
                </div>
                <div>
                    <p align="center">Lo sentimos, ha ocurrido un error con el gráfico</p>
                </div>
            </div>
        </div>
    );
}
export default MessageErrorChart;