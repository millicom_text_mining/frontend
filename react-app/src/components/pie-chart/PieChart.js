import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts'
import * as Constants from '../../constants/constants';
import MessageError from '../message-error-chart/MessageErrorChart';
import Loader from 'react-loader-spinner';
import { getAuth } from "../../context/auth";
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import './PieChart.css';
import imgGoBack from '../../goBack.svg';

require('highcharts/modules/drilldown.js')(Highcharts);
require('highcharts/modules/accessibility.js')(Highcharts);


class PieChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartOptions: null,
            countrySelected: props.countrySelected,
            journeySelected: props.journeySelected,
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            isLoading: true
        };
        this.getPieChartData = this.getPieChartData.bind(this);
    }

    // Petición GET para obtener Canales
    getPieChartData() {
        // Se construye la URL
        let URL_FORMATTED = Constants.BASE_API_URL + '/survey' +
            Constants.JOURNEYS_ENDPOINT +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected);

        if (this.props.journeySelected !== null) {
            for (let i = 0; i < this.props.journeySelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.props.journeySelected[i].value)
            }
        }

        URL_FORMATTED += Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected);

        if (this.props.segmentSelected !== null) {
            for (let i = 0; i < this.props.segmentSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENT + this.props.segmentSelected[i].value;
            }
        }
        if (this.props.productTypeSelected !== null) {
            for (let i = 0; i < this.props.productTypeSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_PRODUCT_TYPE + this.props.productTypeSelected[i].value;
            }
        }
        if (this.props.regionSelected !== null) {
            for (let i = 0; i < this.props.regionSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.regionSelected[i].value;
            }
        }
        if (this.props.contractSelected !== null) {
            for (let i = 0; i < this.props.contractSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_CONTRACT + this.props.contractSelected[i].value;
            }
        }
        if (this.props.municipioSelected !== null) {
            for (let i = 0; i < this.props.municipioSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_MUNICIPIO + this.props.municipioSelected[i].value;
            }
        }
        if (this.props.segmentB2BSelected !== null) {
            for (let i = 0; i < this.props.segmentB2BSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENTB2B + this.props.segmentB2BSelected[i].value;
            }
        }

        URL_FORMATTED = URL_FORMATTED + Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        // Se captura el token actual
        const authToken = getAuth().token_id;

        // Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }

        // Se implementa la petición GET
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    // Se cargan los datos obtenidos
                    this.loadJsonDataInStat(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    // Método para cambiar estados del Gráfico y del Spinner
    loadJsonDataInStat(graphic) {
        var indigo = ['#004388', '#005b9e', '#0073b3', '#008cc9', '#00a4de',
            '#00c8ff', '#40d6ff', '#6adfff', '#95e8ff', '#bff1ff'];

        // Se actualizan valores de la gráfica (xAxis y series) dentro de <charOptions> y se cambia estado isLoading
        this.setState({
            chartOptions: {
                chart: {
                    type: 'pie',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    height: 300,
                    events: {
                        load: function () {
                            var chart = this,
                                renderer = chart.renderer,
                                pattern = renderer.createElement('pattern').add(renderer.defs).attr({
                                    width: 1,
                                    height: 1,
                                    id: 'arrow'
                                });

                            renderer.image(imgGoBack, 10, 0, 45, 30).add(pattern);

                            pattern = renderer.createElement('pattern').add(renderer.defs).attr({
                                width: 1,
                                height: 1,
                                id: 'arrow'
                            });

                            renderer.rect(0, 0, 70, 32)
                                .add(pattern);
                            renderer.image(imgGoBack, 10, 0, 45, 30).add(pattern);
                        }
                    }
                },
                title: {
                    text: ''
                },
                accessibility: {
                    announceNewData: {
                        enabled: false
                    }
                },
                lang: {
                    drillUpText: ''
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        colors: indigo,
                        dataLabels: {
                            enabled: true,
                            format: "<b>{point.name}</b><br>{point.y}",
                        },
                        showInLegend: false,
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },
                series: [
                    {
                        name: "Canales",
                        colorByPoint: true,
                        data: graphic.arrChannels
                    }
                ],
                drilldown: {
                    drillUpButton: {
                        relativeTo: 'spacingBox',
                        position: {
                            width: 50,
                            height: 60
                        },
                        theme: {
                            fill: 'url(#arrow)',
                            'stroke-width': 0,
                            stroke: 'white',
                            r: 2,
                            states: {
                                hover: {
                                    fill: 'url(#arrow)'
                                },
                            }
                        }
                    },
                    series: graphic.arrSurveys
                },
                credits: {
                    enabled: false
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG', 'downloadJPEG', 'downloadXLS']
                        }
                    }
                }
            },
            // Se cambia estado para ocultar Spinner en render
            isLoading: false,
            graphicContainsData: true

        });
    }

    // Función llamada después de renderizado (Pertenece al ciclo de vida del Componente)
    componentDidMount() {
        this.getPieChartData();
    }

    //Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando la variable no cambia
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.journeySelected !== this.props.journeySelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected !== this.props.segmentSelected
            || nextProps.productTypeSelected !== this.props.productTypeSelected
            || nextProps.regionSelected !== this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                journeySelected: nextProps.journeySelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                municipioSelected: nextProps.municipioSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoading: true
            }, function () { this.getPieChartData() });
        }
    }

    render() {
        const { chartOptions } = this.state;
        if (this.state.isLoading) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            )
        } else {
            if (this.state.graphicContainsData) {
                return (
                    <div>
                        <HighchartsReact
                            Highcharts={Highcharts}
                            options={chartOptions}
                        />
                    </div>
                );
            } else {
                return (
                    <MessageError />
                );
            }
        }
    }
}
export default withRouter(PieChart)

