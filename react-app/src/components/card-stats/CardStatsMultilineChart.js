import React, { Component } from 'react';
import './CardStats.css'
import TopicDetractorsChart from '../topic-detractors-chart/TopicDetractorsChart';
import TopicTotalsChart from '../topic-totals/TopicTotalsChart';
import * as Constants from '../../constants/constants';
import { withRouter } from 'react-router-dom';

class CardStatsMultilineChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: props.countrySelected,
            dynamicFilterSelected: props.dynamicFilterSelected,
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected
        };
    }

    // Retorna título para Card según la página (Dashboard o Diccionario)
    titleIsDictionaryPage() {
        if (this.props.location.pathname === Constants.PATH_DICTIONARY
            || this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            return (<font color="red">(Ambiente de pruebas)</font>);
        } else {
            return null;
        }
    }

    render() {
        if (this.props.typeChart === 0) {
            return (
                <div className={this.props.size}>
                    <div className="card h-100 mg-b-20">
                        <div className="card-header">
                            <h4 className="card-header-title">
                                {this.props.cardTitle} {this.titleIsDictionaryPage()}
                            </h4>
                        </div>
                        <div className="card-body pd-0" id={this.props.id}>
                            <div id="chart">
                                <TopicDetractorsChart
                                    countrySelected={this.props.countrySelected}
                                    dynamicFilterSelected={this.props.dynamicFilterSelected}
                                    periodTimeSelected={this.props.periodTimeSelected}
                                    segmentSelected={this.props.segmentSelected}
                                    productTypeSelected={this.props.productTypeSelected}
                                    regionSelected={this.props.regionSelected}
                                    contractSelected={this.props.contractSelected}
                                    municipioSelected={this.props.municipioSelected}
                                    segmentB2BSelected={this.props.segmentB2BSelected} />
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className={this.props.size}>
                    <div className="card h-100 mg-b-20">
                        <div className="card-header">
                            <h4 className="card-header-title">
                                {this.props.cardTitle} {this.titleIsDictionaryPage()}
                            </h4>
                        </div>
                        <div className="card-body pd-0" id={this.props.id}>
                            <div id="chart">
                                <TopicTotalsChart
                                    countrySelected={this.props.countrySelected}
                                    dynamicFilterSelected={this.props.dynamicFilterSelected}
                                    periodTimeSelected={this.props.periodTimeSelected}
                                    segmentSelected={this.props.segmentSelected}
                                    productTypeSelected={this.props.productTypeSelected}
                                    regionSelected={this.props.regionSelected}
                                    contractSelected={this.props.contractSelected} 
                                    municipioSelected={this.props.municipioSelected}
                                    segmentB2BSelected={this.props.segmentB2BSelected} />
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }


}
export default withRouter(CardStatsMultilineChart)