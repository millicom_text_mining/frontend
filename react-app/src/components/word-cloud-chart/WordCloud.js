import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts'
require('highcharts/modules/wordcloud.js')(Highcharts);

class WordCloud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartOptions: {
                chart: {
                    height: 400
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG','downloadJPEG','downloadXLS']
                        }
                    }
                },
                series: [{
                    type: 'wordcloud',
                    data: props.arrCommonWords,
                    name: 'Cantidad'
                }],
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
            }
        };
    }

    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando la variable no cambia
        if (nextProps.arrCommonWords !== this.state.arrCommonWords) {
            this.setState({
                chartOptions: {
                    series: [{
                        data: nextProps.arrCommonWords
                    }
                    ]
                }
            });
        }
    }

    render() {
        const { chartOptions } = this.state;

        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default WordCloud;