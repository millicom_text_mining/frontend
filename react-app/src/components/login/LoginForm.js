import React, { useState } from 'react';
import logo from '../../logo-tigo-millicom-blue.svg';
import { Redirect } from "react-router-dom";
import { useAuth, getAuth } from "../../context/auth";
import ButtonLogin from "./ButtonLogin";
import swal from 'sweetalert';
import * as Constants from '../../constants/constants';

function LoginForm() {
    // React Hooks para controlar estados de variables locales
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const { setAuthToken } = useAuth();
    const authToken = getAuth();

    // Función que recibe props después de validar usuario en ButtonLogin
    function handleSubmit(props) {
        // propIsError= true, entonces el usuario no se autenticó correctamente
        let propsIsError = props[0];
        // propsData recibe response.data si el usuario se autentica correctamente
        let propsData = props[1];
        // propIsConnectionError= true si ocurrió un error de conexión
        let propsIsConnectionError = props[2];

        // Se muestra mensaje si hubo error en datos de usuario
        if (propsIsError) {
            swal({
                text: Constants.ALERT_LOGIN_ERROR_TEXT,
                icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        } else {
            // Se muestra mensaje si hubo error en conexión
            if (propsIsConnectionError) {
                swal({
                    text: Constants.ALERT_ERROR_TEXT,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON,
                });
            } else {
                // Usuario válido, se asigna token a variable global
                setAuthToken(propsData);
                // Se redirige a /
                return (<Redirect to={"/"} />);
            }
        }
        // Se restablece input de contraseña cuando el usuario presiona ButtonLogin
        setPassword("");
    }

    if (authToken) {
        // Si el usuario ya tiene token válido, se redirige a / para saltar /login
        return <Redirect to={"/"} />;
    }

    return (
        <div>
            <div className="d-flex">
                <div className="card shadow-none pd-20 mx-auto wd-300 text-center bd-1 align-self-center">
                    <img src={logo} alt="Tigo"/>
                    <hr />
                    <h4 className="card-title mt-3 text-center">Inicio de Sesión</h4>
                    <p className="text-center">Ingresa a tu cuenta</p>
                    <form>
                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                            </div>
                            <input
                                className="form-control form-control-sm"
                                placeholder="Usuario"
                                value={userName}
                                type="text"
                                onChange={e => {
                                    setUserName(e.target.value);
                                }} />
                        </div>
                        <div className="form-group input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                            </div>
                            <input
                                className="form-control form-control-sm"
                                placeholder="Contraseña"
                                value={password}
                                type="password"
                                onChange={e => {
                                    setPassword(e.target.value);
                                }} />
                        </div>
                        <div className="form-group">
                            <ButtonLogin onClick={handleSubmit} user={userName} pass={password} />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default LoginForm;