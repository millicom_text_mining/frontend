import React, { Component } from "react";
import * as Constants from '../../constants/constants';
import axios from 'axios';
import swal from "sweetalert";
import { getAuth, clearLocalStorage } from "../../context/auth";

export default class ButtonResetPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    validatePasswords = () => {
        this.setState({ loading: true });

        setTimeout(() => {
            const newPass = this.props.newPassword;
            const confirmPass = this.props.confirmPassword;
            let isPasswordError = false;
            let isLengthError = false;
            let isConnectionError = false;
            // Se verifica si contraseñas son diferentes
            if (newPass !== confirmPass) {
                // onClick [0]: isPasswordError, [1]: isLengthError, [2]: isConnectionError
                isPasswordError = true;
                isLengthError = false;
                isConnectionError = false;
                this.setState({ loading: false });
                // Se retorna onClick para mensaje de contraseñas diferentes
                this.props.onClick([isPasswordError, isLengthError, isConnectionError]);
            } else {
                // Se verifica si contraseña tiene menos de 6 caracteres
                if (newPass.length < 6) {
                    isPasswordError = false;
                    isLengthError = true;
                    isConnectionError = false;
                    this.setState({ loading: false });
                    // Se retorna onClick para mensaje de contraseña corta
                    this.props.onClick([isPasswordError, isLengthError, isConnectionError]);
                } else {
                    // Se construyen variables para petición POST
                    const LOGIN_URL = Constants.BASE_API_URL + Constants.CHANGE_PASSWORD_ENDPOINT;
                    const data = {
                        [Constants.PARAM_CHANGE_PASS_NEWPASS]: newPass,
                        [Constants.PARAM_CHANGE_PASS_CONFIRMPASS]: confirmPass
                    }
                    //Se construye HEADERS
                    const HEADERS = { headers: { 'authorization-tigo': getAuth().token_id } }
                    // Se ejecuta método POST
                    axios.post(LOGIN_URL, data, HEADERS)
                        .then(response => {
                            if (response.status === 200) {
                                // Se actualiza contraseña
                                swal({
                                    text: Constants.ALERT_PASS_UPDATE_TEXT,
                                    icon: Constants.ALERT_DEFAULT_SUCCESS_ICON,
                                    button: Constants.ALERT_DEFAULT_OK_BUTTON
                                }).then(() => {
                                    // Se elimina Token para inicio de sesión obligatorio
                                    clearLocalStorage();
                                });
                                isConnectionError = false;
                            } else {
                                isConnectionError = true;
                            }
                            this.setState({ loading: false });
                            // Se retorna onClick con todo OK
                            this.props.onClick([isPasswordError, isLengthError, isConnectionError]);
                        }).catch(e => {
                            this.setState({ loading: false });
                            // Se retorna onClick para mensaje de error en conexión
                            this.props.onClick([false, isLengthError, isConnectionError]);
                        });
                }
            }
        }, 500);
    };

    render() {
        const { loading } = this.state;
        return (
            <div style={{ marginTop: "15px" }}>
                <button type="submit" className="btn btn-custom-primary btn-block tx-13 hover-white"
                    onClick={this.validatePasswords} disabled={loading}>
                    {loading && (
                        <i
                            className="fa fa-spinner fa-spin"
                            style={{ marginRight: "5px" }}
                        />
                    )}
                    {loading && <span>Validando Contraseña</span>}
                    {!loading && <span>Cambiar Contraseña</span>}
                </button>
            </div>
        );
    }
}