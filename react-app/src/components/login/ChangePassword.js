import React, { useState } from 'react';
import logo from '../../logo-tigo-millicom-blue.svg';
import { Redirect } from "react-router-dom";
import { getAuth, clearLocalStorage } from "../../context/auth";
import ButtonResetPass from "./ButtonResetPass";
import swal from 'sweetalert';
import * as Constants from '../../constants/constants';

function ChangePassword() {
    // React Hooks para controlar estados de variables locales
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    // Función que recibe props desde ButtonResetPass
    function handleSubmit(props) {
        // propsIsPasswordError= true, entonces no coinciden las contraseñas
        let propsIsPasswordError = props[0];
        // propIsLengthError= true, entonces la contraseña tiene menos de 6 caracteres
        let propIsLengthError = props[1];
        // propsIsConnectionError= true, entonces ocurrió un error de conexión
        let propsIsConnectionError = props[2];

        // Se muestra mensaje si las contraseñas son diferentes
        if (propsIsPasswordError) {
            swal({
                text: Constants.ALERT_CHANGE_PASS_ERROR_EQUAL,
                icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        } else {
            // Se muestra mensaje si contraseña es menor a 6 caracteres
            if (propIsLengthError) {
                swal({
                    text: Constants.ALERT_CHANGE_PASS_ERROR_LENGTH,
                    icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                    button: Constants.ALERT_DEFAULT_OK_BUTTON,
                });
            } else {
                // Se muestra mensaje si hubo error en conexión
                if (propsIsConnectionError) {
                    swal({
                        text: Constants.ALERT_ERROR_TEXT,
                        icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                        button: Constants.ALERT_DEFAULT_OK_BUTTON,
                    });
                }
            }
        }
        // Se reinician los campos
        setNewPassword("");
        setConfirmPassword("");
    }

    // Se invoca función de Auth para eliminar token
    function handleLogout() {
        clearLocalStorage();
    }

    // Se verifica si el usuario tiene Token
    if (getAuth() != null) {
        // Se verifica si el usuario tiene que cambiar contraseña
        if (getAuth().token.user.change_password === 0) {
            // Se redirige a /
            return (<Redirect to={"/"} />);
        } else {
            // Se renderiza formulario para cambio de contraseña
            return (
                <div>
                    <div className="d-flex">
                        <div className="card shadow-none pd-20 mx-auto wd-300 text-center bd-1 align-self-center">
                            <img src={logo} alt="tigo" />
                            <hr />
                            <p className="text-center">Por motivos de seguridad debes cambiar tu contraseña</p>
                            <form>
                                <div className="form-group input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                                    </div>
                                    <input
                                        className="form-control form-control-sm"
                                        placeholder="Nueva Contraseña"
                                        value={newPassword}
                                        type="password"
                                        onChange={e => {
                                            setNewPassword(e.target.value);
                                        }} />
                                </div>
                                <div className="form-group input-group mg-b-30">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                                    </div>
                                    <input
                                        className="form-control form-control-sm"
                                        placeholder="Confirmar Contraseña"
                                        value={confirmPassword}
                                        type="password"
                                        onChange={e => {
                                            setConfirmPassword(e.target.value);
                                        }} />
                                </div>
                                <div className="form-group">
                                    <ButtonResetPass onClick={handleSubmit} newPassword={newPassword} confirmPassword={confirmPassword} />
                                </div>
                                <button className="btn btn-link" type="Button" onClick={handleLogout} >Salir</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    } else {
        // Si el usuario no tiene Token, se redirige a /login
        return (<Redirect to={"/login"} />);
    }
}
export default ChangePassword;