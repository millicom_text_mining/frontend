import React, { Component } from "react";
import * as Constants from '../../constants/constants';
import axios from 'axios';

export default class ButtonLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    // Función llamada cuando se presiona el botón de Login
    validateUser = () => {
        this.setState({ loading: true });

        // Se inicia timeOut de 0.5s para animación de botón
        setTimeout(() => {
            // Se consntruye la URL de LOGIN
            const LOGIN_URL = Constants.BASE_API_URL + Constants.LOGIN_ENDPOINT;
            // Se construye la data a enviar
            const data = {
                [Constants.PARAM_LOGIN_USERNAME]: this.props.user,
                [Constants.PARAM_LOGIN_PASSWORD]: this.props.pass
            }
            // Se ejecuta método POST
            axios.post(LOGIN_URL, data)
                .then(response => {
                    // onClick [0]: propsIsError, [1]: propsData, [2]: propsIsConnectionError (Ver LoginForm para definición)
                    if (response.status === 200) {
                        // Usuario autenticado correctamente
                        this.props.onClick([false, response.data, false]);
                    } else {
                        // Error en usuario o contraseña
                        this.props.onClick([true, null, false]);
                    }
                    this.setState({ loading: false });
                }).catch(e => {
                    // Error en conexión
                    this.props.onClick([true, null, true]);
                    this.setState({ loading: false });
                });
        }, 500);
    };

    render() {
        const { loading } = this.state;
        return (
            <div style={{ marginTop: "15px" }}>
                <button type="submit" className="btn btn-custom-primary btn-block tx-13 hover-white"
                    onClick={this.validateUser} disabled={loading}>
                    {loading && (
                        <i
                            className="fa fa-spinner fa-spin"
                            style={{ marginRight: "5px" }}
                        />
                    )}
                    {loading && <span>Validando Usuario</span>}
                    {!loading && <span>Ingresar</span>}
                </button>
            </div>
        );
    }
}