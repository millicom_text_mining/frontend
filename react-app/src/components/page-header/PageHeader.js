import React, { Component } from 'react';
import './PageHeader.css';
import logo from '../../logo-tigo-millicom-white.svg';
import logoBlue from '../../logo-tigo-millicom-blue.svg';
import downloadIcon from '../../download-icon-white.svg';
import downloadingGif from '../../downloading-file-white.gif';
import userIcon from '../../login_user_icon.svg';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import swal from 'sweetalert';
import { getAuth, refreshToken, clearLocalStorage } from "../../context/auth";
import { withRouter, NavLink, Link } from 'react-router-dom';
import $ from "jquery";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";
import { bubble as Menu } from 'react-burger-menu';

// Estilos para el menú hamburguesa que se muestra en el navbar en dispositivos pequeños
let styles = {
    bmBurgerButton: {
        position: 'fixed',
        width: '26px',
        height: '20px',
        left: '18px',
        top: '18px',
        color: '#ffffff'
    },
    bmBurgerBars: {
        background: '#ffffff'
    },
    bmBurgerBarsHover: {
        background: '#ffffff'
    },
    bmCrossButton: {
        height: '24px',
        width: '24px'
    },
    bmCross: {
        background: '#ffffff'
    },
    bmMenuWrap: {
        position: 'fixed',
        height: '100%'
    },
    bmMenu: {
        background: '#ffffff',
        padding: '20px 1.5em',
        fontSize: '1.15em'
    },
    bmMorphShape: {
        fill: '#ffffff'
    },
    bmItemList: {
        color: '#ffffff'
    },
    bmItem: {
        display: 'inline-block',
        border: '0px',
        outline: '0px'
    }
};

class PageHeader extends Component {
    showSettings(event) {
        event.preventDefault();
    }

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isActive: false
        };
    }

    render() {
        return (

            <div className="page-header">
                {/*================================*/}
                {/* Page Header  Start */}
                {/*================================*/}
                {this.renderResponsive()}
                <div>
                    {/* Botón de Go Up **/}
                    <ScrollUpButton
                        StopPosition={0}
                        ShowAtPosition={150}
                        EasingType='easeOutCubic'
                        AnimationDuration={500}
                        ContainerClassName='ScrollUpButton__Container'
                        TransitionClassName='ScrollUpButton__Toggled'
                        style={{ backgroundColor: "transparent", fill: "#00377d", outline: "none" }}
                        ToggledStyle={{}} />
                </div>
            </div >
        );
    }

    //Renderiza el page header normal si es dispositivo grande y menu hamburguesa si es pequeño
    renderResponsive() {
        // Si es dispositivo pequeño
        if (document.documentElement.clientWidth <= 414) {
            return this.burgerMenu();
        } else {
            // Si es dispositivo grande
            return (
                <nav className="navbar navbar-expand-lg" id="outer-container">
                    <div className="collapse navbar-collapse list-inline">
                        {/*================================*/}
                        {/* Menu Start */}
                        {/*================================*/}
                        {this.topBarMainOptions()}
                        {/* Export Data Option */}
                        {this.topBarRightOptions()}
                        {/*/ Menu End*/}
                    </div>
                </nav>
            );
        }
    }

    // Renderizar el menú hamburguesa
    burgerMenu() {
        return (
            <div>
                <Menu width={260} isOpen={false} styles={styles}>
                    <div>
                        <div className="logo-img">
                            <img className="small-logo" style={{ width: '100%', height: '45px' }}
                                src={logoBlue} alt="" />
                        </div>
                    </div>
                    <div style={{ padding: '30px', paddingLeft: '0px' }}>
                        <div className="page-sidebar-inner">
                            <div className="page-sidebar-menu">
                                <ul className="accordion-menu">
                                    {this.canSeeDashboardResponsive()}
                                    {this.canSeeDictionaryResponsive()}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar-footer" style={{ width: '100%' }}>
                        <a className="pull-left" href="/login" data-toggle="tooltip"
                            data-placement="top" data-original-title="Sing Out"
                            onClick={e => this.logoutHandler(e)}>
                            <i data-feather="log-out" className="ht-15" style={{ paddingBottom: '5px', height: '30px' }}></i>
                        </a>
                    </div>
                </Menu>
                <div className="download-icon">
                    {this.exportDataOptionResponsive()}
                </div>
            </div>
        );
    }

    // Renderiza el navbar principal
    topBarMainOptions() {
        return (
            <ul className="navbar-nav mr-auto">
                {/* Logo */}
                <img src={logo} alt="" className="logo-top-bar pd-4" />
                {/*Renderiza dashboard por tipo encuesta o journey*/}
                {this.seeDashboard()}
                {/* Renderiza diccionarios y stopwords si es Editor */}
                {this.canSeeDictionary()}
            </ul>
        );
    }

    //Método que permite señalar la pestaña (Dashboard o Diccionario) actual
    highlightView(location, idView, pathOption1, pathOption2, pathOption3) {
        $(document).on("DOMContentLoaded", function (e) {
            if (location.pathname === pathOption1 ||
                location.pathname === pathOption2 ||
                location.pathname === pathOption3) {
                $("#" + idView).addClass("analytic-nav-a-selected");
            }
            else {
                $("#" + idView).removeClass("analytic-nav-a-selected");
            }
        });

        if (location.pathname === pathOption1 ||
            location.pathname === pathOption2 ||
            location.pathname === pathOption3) {
            $("#" + idView).addClass("analytic-nav-a-selected");
        }
        else {
            $("#" + idView).removeClass("analytic-nav-a-selected");
        }
    }

    // Renderiza los elementos de la pestaña diccionario responsive
    canSeeDictionaryResponsive() {
        // Obtener los permisos de acceso a las vistas de Testing (diccionario) por medio del token
        let canAccessDictionary = getAuth().token.user.access.dictionary_screen.survey;
        let canAccessJourney = getAuth().token.user.access.dictionary_screen.journey_voc;
        let canAccessStopwords = getAuth().token.user.access.dictionary_screen.stopwords;
        if (canAccessDictionary || canAccessJourney) {
            return (
                <li className="closed">
                    <a href=""><i data-feather="book"></i>
                        <span>Testing</span>
                        <i className="accordion-icon fa fa-angle-left"></i>
                    </a>
                    <ul className="sub-menu" style={{ display: "block" }}>
                        {canAccessDictionary ? <li><a href={Constants.PATH_DICTIONARY}>Tipo Encuesta</a></li> : ""}
                        {canAccessJourney ? <li><a href={Constants.PATH_DICTIONARY_JOURNEY}>Journey</a></li> : ""}
                        {canAccessStopwords ? <li><a href={Constants.PATH_STOPWORDS}>StopWords</a></li> : ""}
                    </ul>
                </li>
            );
        }
    }

    // Renderiza los elementos de la pestaña dashboard responsive
    canSeeDashboardResponsive() {
        // Obtener los permisos de acceso a las vistas de Dashboard por medio del token
        let canAccessDashboard = getAuth().token.user.access.main_screen.survey;
        let canAccessJourney = getAuth().token.user.access.main_screen.journey_voc;
        //let canAccessJourney = true; 
        if (canAccessDashboard || canAccessJourney) {
            return (
                <li className="closed">
                    <a href=""><i data-feather="bar-chart-2"></i>
                        <span>Dashboard</span>
                        <i className="accordion-icon fa fa-angle-left"></i>
                    </a>
                    <ul className="sub-menu" style={{ display: "block" }}>
                        {canAccessDashboard ? <li class="active"><a href={Constants.PATH_DASHBOARD}>Tipo Encuesta</a></li> : ""}
                        {canAccessJourney ? <li><a href={Constants.PATH_JOURNEY}>Journey</a></li> : ""}
                    </ul>
                </li>
            );
        }
    }

    // Renderiza los elementos de la pestaña diccionario 
    canSeeDictionary() {
        // Obtener los permisos de acceso a las vistas de Diccionario por medio del token
        let canAccessDictionary = getAuth().token.user.access.dictionary_screen.survey;
        let canAccessJourney = getAuth().token.user.access.dictionary_screen.journey_voc;
        let canAccessStopwords = getAuth().token.user.access.dictionary_screen.stopwords;
        if (canAccessDictionary || canAccessJourney) {
            return (
                <div className="header-right pull-right">
                    <ul className="list-inline justify-content-end">
                        <li className="list-inline-item align-middle dropdown hidden-xs mg-t-5 ">
                            <Link id={Constants.PAGE_DICTIONARY} data-toggle="dropdown" className="btn txt-topbar analytic-nav-a"
                                to={location => {
                                    this.highlightView(location, Constants.PAGE_DICTIONARY,
                                        Constants.PATH_DICTIONARY, Constants.PATH_DICTIONARY_JOURNEY,
                                        Constants.PATH_STOPWORDS);
                                }
                                }
                                onClick={refreshToken()}>
                                Testing
                            </Link>
                            <ul className="dropdown-menu languages-dropdown shadow-2 submenu-front">
                                {this.seeDictionarySurvey(canAccessDictionary)}
                                {this.seeDictionaryJourney(canAccessJourney)}
                                {this.stopwordsOption(canAccessStopwords)}
                            </ul>
                        </li>
                    </ul>
                </div>
            );
        }
    }

    // Renderiza el link para acceder a Testing (diccionario) Tipo Encuesta
    seeDictionarySurvey(canAccessDictionary) {
        if (canAccessDictionary) {
            return (
                <li>
                    <NavLink to={Constants.PATH_DICTIONARY} className="txt-topbar analytic-nav-a-toggle"
                        onClick={refreshToken()} activeClassName="analytic-nav-a-selected">
                        Tipo Encuesta
                    </NavLink>
                </li>)
        } else {
            return null
        }
    }

    // Renderiza el link para acceder a Testing (diccionario) Journey
    seeDictionaryJourney(canAccessJourney) {
        if (canAccessJourney) {
            return (<li>
                <NavLink to={Constants.PATH_DICTIONARY_JOURNEY} className="txt-topbar analytic-nav-a-toggle"
                    onClick={refreshToken()} activeClassName="analytic-nav-a-selected">
                    Journey
                </NavLink>
            </li>)
        }
    }

    // Renderiza el link para acceder a Testing (diccionario) Stopwords
    stopwordsOption(canAccessStopwords) {
        if (canAccessStopwords) {
            return (
                <li>
                    <NavLink exact to={Constants.PATH_STOPWORDS} className="txt-topbar analytic-nav-a-toggle"
                        onClick={refreshToken()} activeClassName="analytic-nav-a-selected">
                        StopWords
                    </NavLink>
                </li>
            );
        } else {
            return null;
        }
    }

    // Renderiza el dropdown de Dashboard en el navbar
    seeDashboard() {
        let canAccessDashboard = getAuth().token.user.access.main_screen.survey;
        let canAccessJourney = getAuth().token.user.access.main_screen.journey_voc;
        //let canAccessJourney = true;
        if (canAccessDashboard || canAccessJourney) {
            return (
                <div className="header-right pull-right">
                    <ul className="list-inline justify-content-end">
                        <li className="list-inline-item align-middle dropdown hidden-xs mg-t-5 ">
                            <Link id={Constants.PAGE_DASHBOARD} data-toggle="dropdown" className="btn txt-topbar analytic-nav-a"
                                to={location => {
                                    this.highlightView(location, Constants.PAGE_DASHBOARD,
                                        Constants.PATH_DASHBOARD, Constants.PATH_JOURNEY);
                                }
                                }
                                onClick={refreshToken()}>
                                Dashboard
                            </Link>

                            <ul className="dropdown-menu languages-dropdown shadow-2">
                                {this.seeDashboardSurvey(canAccessDashboard)}
                                {this.seeDashboardJourney(canAccessJourney)}
                            </ul>
                        </li>
                    </ul>
                </div>
            );
        }
    }

    // Renderiza el link para acceder a Dashboard Tipo Encuesta
    seeDashboardSurvey(canAccessDashboard) {
        if (canAccessDashboard) {
            return (
                <li>
                    <NavLink exact to={Constants.PATH_DASHBOARD} className="txt-topbar analytic-nav-a-toggle"
                        onClick={refreshToken()} activeClassName="analytic-nav-a-selected">
                        Tipo Encuesta
                    </NavLink>
                </li>
            );
        } else {
            return null;
        }
    }

    // Renderiza el link para acceder a Dashboard Journey
    seeDashboardJourney(canAccessJourney) {
        if (canAccessJourney) {
            return (
                <li>
                    <NavLink exact to={Constants.PATH_JOURNEY} className="txt-topbar analytic-nav-a-toggle"
                        onClick={refreshToken()} activeClassName="analytic-nav-a-selected">
                        Journey
                    </NavLink>
                </li>
            );
        } else {
            return null;
        }
    }

    //Renderiza las opciones de la parte derecha del navbar
    topBarRightOptions() {
        return (
            <div className="header-right pull-right">
                <ul className="navbar-nav mr-auto mg-t-5">
                    {this.exportDataOption()}
                    {this.dropdownMenu()}
                </ul>
            </div>
        );
    }

    //Renderiza la opción de descarga del navbar
    exportDataOption() {
        if (this.props.location.pathname === Constants.PATH_STOPWORDS) {
            return null;
        } else {
            if (this.state.isLoading) {
                return (
                    <a className="text-white mg-l-100 mg-t-5" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="text-white mg-r-15" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        }
    }

    //Renderiza la opción de descarga del navbar en el menu hamburguesa
    exportDataOptionResponsive() {
        if (this.props.location.pathname === Constants.PATH_STOPWORDS) {
            return null;
        } else {
            if (this.state.isLoading) {
                return (
                    <a className="download-icon" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadingGif} className="wd-30 ht-30 center" alt="" />
                    </a>
                );
            } else {
                return (
                    <a className="download-icon" target="_blank" style={{ cursor: "pointer" }}>
                        <img src={downloadIcon} onClick={e => this.downloadFile(e)} className="img-fluid wd-35 ht-35" alt="" />
                    </a>
                );
            }
        }
    }

    // Método que permite la descarga de los datos
    downloadFile = (e) => {
        //Se construye la URL
        let BASE_URL_FILE = Constants.BASE_API_URL;
        const authToken = getAuth().token_id;
        let storage = JSON.parse(localStorage.getItem("Filter"));
        let dynamicURL = "";
        let fileName = "";
        let fileType = "";

        // Se capturan los filtros desde Local Storage
        //storage = JSON.parse(localStorage.getItem("Filter"));
        //dynamicURL = "";
        //BASE_URL_FILE = Constants.BASE_API_URL;

        if (this.props.location.pathname === Constants.PATH_DASHBOARD ||
            this.props.location.pathname === Constants.PATH_DICTIONARY) {
            dynamicURL = Constants.DOWNLOAD_DATA +
                Constants.PARAM_COUNTRY + encodeURIComponent(storage.countrySelected.label);
            for(let i = 0; i < storage.typeSurveySelected.length; i++) {
                dynamicURL += Constants.PARAM_SURVEY + encodeURIComponent(storage.typeSurveySelected[i].label);
            }
            for(let i = 0; i < storage.segmentSelected.length; i++) {
                dynamicURL += Constants.PARAM_SEGMENT + encodeURIComponent(storage.segmentSelected[i].label);
            }
            for(let i = 0; i < storage.productTypeSelected.length; i++) {
                dynamicURL += Constants.PARAM_PRODUCT_TYPE + encodeURIComponent(storage.productTypeSelected[i].label);
            }
            for(let i = 0; i < storage.regionSelected.length; i++) {
                dynamicURL += Constants.PARAM_REGION + encodeURIComponent(storage.regionSelected[i].label);
            }
            for(let i = 0; i < storage.contractSelected.length; i++) {
                dynamicURL += Constants.PARAM_CONTRACT + encodeURIComponent(storage.contractSelected[i].label);
            }
            for(let i = 0; i < storage.municipioSelected.length; i++) {
                dynamicURL += Constants.PARAM_MUNICIPIO + encodeURIComponent(storage.municipioSelected[i].label);
            }
            for(let i = 0; i < storage.segmentB2BSelected.length; i++) {
                dynamicURL += Constants.PARAM_SEGMENTB2B + encodeURIComponent(storage.segmentB2BSelected[i].label);;
            }
            fileType = storage.channelSelected.label + "_survey";
        } else {
            dynamicURL = Constants.DOWNLOAD_DATA_JOURNEY +
                Constants.PARAM_COUNTRY + encodeURIComponent(storage.countrySelected.label);
                for(let i = 0; i < storage.journeySelected.length; i++) {
                    dynamicURL += Constants.PARAM_JOURNEY + encodeURIComponent(storage.journeySelected[i].label);
                }
                for(let i = 0; i < storage.segmentSelected.length; i++) {
                    dynamicURL += Constants.PARAM_SEGMENT + encodeURIComponent(storage.segmentSelected[i].label);
                }
                for(let i = 0; i < storage.productTypeSelected.length; i++) {
                    dynamicURL += Constants.PARAM_PRODUCT_TYPE + encodeURIComponent(storage.productTypeSelected[i].label);
                }
                for(let i = 0; i < storage.regionSelected.length; i++) {
                    dynamicURL += Constants.PARAM_REGION + encodeURIComponent(storage.regionSelected[i].label);
                }
                for(let i = 0; i < storage.contractSelected.length; i++) {
                    dynamicURL += Constants.PARAM_CONTRACT + encodeURIComponent(storage.contractSelected[i].label);
                }
                for(let i = 0; i < storage.municipioSelected.length; i++) {
                    dynamicURL += Constants.PARAM_MUNICIPIO + encodeURIComponent(storage.municipioSelected[i].label);
                }
                for(let i = 0; i < storage.segmentB2BSelected.length; i++) {
                    dynamicURL += Constants.PARAM_SEGMENTB2B + encodeURIComponent(storage.segmentB2BSelected[i].label);;
                }
            fileType = "journey";
        }

        BASE_URL_FILE = BASE_URL_FILE + dynamicURL +
            Constants.PARAM_PERIOD + encodeURIComponent(storage.periodTimeSelected.value) +
            Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DASHBOARD ||
            this.props.location.pathname === Constants.PATH_JOURNEY) {
            BASE_URL_FILE = BASE_URL_FILE + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
            fileName = "Data_Production_";
        } else {
            BASE_URL_FILE = BASE_URL_FILE + Constants.PARAM_DICTIONARY_TYPE_TEST;
            fileName = "Data_Test_";
        }

        // Se completa nombre de archivo
        fileName = fileName + (storage.countrySelected.label) + "_" + fileType
            + Constants.EXT_EXCEL;

        this.setState({
            isLoading: true
        });

        // Se crea petición GET con axios
        axios({
            url: BASE_URL_FILE,
            method: 'GET',
            responseType: 'blob',
            headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' }
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
            this.setState({
                isLoading: false
            });
        }).catch(e => {
            this.setState({
                isLoading: false
            });
            //Mostrar mensaje de fallo en descarga de datos
            swal({
                text: Constants.ALERT_ERROR_TEXT,
                icon: Constants.ALERT_DEFAULT_ERROR_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        });
    }

    //Opción de usuario y cerrar sesión
    dropdownMenu() {
        return (
            <div>
                <ul className="list-inline justify-content-end">
                    <li className="list-inline-item dropdown">
                        <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="text-white">
                            <img src={userIcon} className="img-fluid wd-35 ht-35" alt="" />
                            <span className="select-profile"> {getAuth().token.user.user_name}</span>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right dropdown-profile shadow-2 hide">
                            <div className="user-profile-area">
                                <div className="user-profile-heading">
                                    <div className="profile-text">
                                        <h6>{getAuth().token.user.full_name}</h6>
                                        <span>{getAuth().token.user.email}</span>
                                    </div>
                                </div>
                                <button type="button" onClick={e => this.logoutHandler(e)} className="dropdown-item btn btn-link">
                                    <i className="icon-power" aria-hidden="true" /> Cerrar Sesión</button>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
    logoutHandler = (e) => {
        clearLocalStorage();
    }
}
export default withRouter(PageHeader)
