import React, { Component } from 'react';

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: props.columns,
            data: props.data,
            idTable: props.idTable
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            columns: nextProps.columns,
            data: nextProps.data,
            idTable: nextProps.idTable
        });
    }

    checkDataNotNull() {
        if (this.state.data != null) {
            return (
                <table className="table hover thead-light responsive nowrap text-dark" id={this.state.idTable}>
                    <thead>
                        <tr>
                            {this.state.columns.map((value, index) => {
                                return <th key={index}>{value}</th>
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((value, index) => {
                            return <tr key={index}>
                                {value.map((row, id) => {
                                    return <td key={id}>{row}</td>
                                })}
                            </tr>
                        })}
                    </tbody>
                </table>
            );
        } else {
            return(<h3><center>No fue posible cargar el Diccionario</center></h3>);
        }
    }

    render() {
        return (
            <div className="mg-5" style={{overflowX: 'auto'}}>
                {this.checkDataNotNull()}
            </div>
        );
    }
}

export default Table