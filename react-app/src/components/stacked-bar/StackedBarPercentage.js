import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts'

class StackedBarPercentage extends Component {
    constructor(props) {
        super(props);

        let initialStateChartOptions = {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                // En <categories> se pasan los Nombres de Tópicos
                categories: [],
                crosshair: true
            }],
            // Se cargan 2 objetos para gráfica doble eje
            yAxis: [
                {
                    // Eje principal: Cantidad
                    title: {
                        text: 'Cantidad',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, {
                    // Eje secundario: % Tópico
                    title: {
                        text: '% Tópico',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}%',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
            tooltip: {
                shared: true
            },
            legend: {
                align: 'center'
            },
            plotOptions: {
                column: {
                    stacking: 'number'
                }
            },
            // En cada arreglo <data> de <series> se pasan los valores
            series: [{
                name: 'Promotores(9-10)',
                color: '#4180DC',
                type: 'column',
                yAxis: 0,
                data: []
            }, {
                name: 'Neutros(7-8)',
                color: '#F7C913',
                type: 'column',
                yAxis: 0,
                data: []
            }, {
                name: 'Detractores(0-6)',
                color: '#CA2400',
                type: 'column',
                yAxis: 0,
                data: []
            },
            {
                name: '% Tópico',
                color: '#A140D8',
                type: 'spline',
                data: [],
                tooltip: {
                    valueSuffix: '%'
                },
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[10],
                    fillColor: 'white'
                },
                yAxis: 1
            }]
        };

        this.state = {
            chartOptions: initialStateChartOptions
        };
    }

    // Prueba de petición para usar JSON
    getData() {
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => {
                this.setState({ data: json });
                // Se pasa JSONdata estático para pruebas
                let JSONdata = [
                    {
                        'topicName': 'Estabilidad general',
                        'totalPromoters': 468,
                        'totalNeutrals': 281,
                        'totalDetractors': 582,
                        'percentTopic': 31.9
                    },
                    {
                        'topicName': 'Servicio',
                        'totalPromoters': 257,
                        'totalNeutrals': 161,
                        'totalDetractors': 474,
                        'percentTopic': 21.4
                    },
                    {
                        'topicName': 'Tarifa',
                        'totalPromoters': 38,
                        'totalNeutrals': 72,
                        'totalDetractors': 274,
                        'percentTopic': 9.2
                    },
                    {
                        'topicName': 'Calidad Internet',
                        'totalPromoters': 18,
                        'totalNeutrals': 27,
                        'totalDetractors': 109,
                        'percentTopic': 3.7
                    },
                    {
                        'topicName': 'Velocidad',
                        'totalPromoters': 37,
                        'totalNeutrals': 80,
                        'totalDetractors': 228,
                        'percentTopic': 8.3
                    },
                    {
                        'topicName': 'Tiempo de solución',
                        'totalPromoters': 57,
                        'totalNeutrals': 27,
                        'totalDetractors': 199,
                        'percentTopic': 6.8
                    },
                    {
                        'topicName': 'Modelo de actuación',
                        'totalPromoters': 89,
                        'totalNeutrals': 25,
                        'totalDetractors': 90,
                        'percentTopic': 4.9
                    }
                ];
                this.loadJsonDataInStat(JSONdata);
                //this.printJSONdata(json);
            })
            .catch(error => {
                console.error(error);
            })
    }

    loadJsonDataInStat(json) {
        // Se declaran arreglos para iteración de JSON
        var arrTopicName = [];
        var arrTotalPromoters = [];
        var arrTotalNeutrals = [];
        var arrTotalDetractors = [];
        var arrPercentTopic = [];

        for (let index = 0; index < json.length; index++) {
            var obj = json[index];

            // Se cargan los valores de cada objeto del JSON
            arrTopicName.push(obj.topicName);
            arrTotalPromoters.push(obj.totalPromoters);
            arrTotalNeutrals.push(obj.totalNeutrals);
            arrTotalDetractors.push(obj.totalDetractors);
            arrPercentTopic.push(obj.percentTopic);
        }

        // Se pasan los arreglos a las opciones de gráfica <charOptions>
        this.setState({
            chartOptions: {
                xAxis: {
                    categories: arrTopicName
                },
                series: [{
                    // Promotores
                    data: arrTotalPromoters
                }, {
                    // Neutros
                    data: arrTotalNeutrals
                }, {
                    // Detractores
                    data: arrTotalDetractors
                },
                {
                    // % Tópico
                    data: arrPercentTopic
                }]
            }
        });
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getData();
    }

    render() {
        const { chartOptions } = this.state;

        return (
            <div>
                <HighchartsReact
                    Highcharts={Highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default StackedBarPercentage;