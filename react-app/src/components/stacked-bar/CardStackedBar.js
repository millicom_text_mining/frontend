import React, { Component } from 'react';
import '../card-stats/CardStats.css';
import * as Constants from '../../constants/constants';
import StackedBarNPS from './StackedBarNPS';
import axios from 'axios';
import { getAuth } from "../../context/auth";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';
import MessageError from '../message-error-chart/MessageErrorChart';

class CardStackedBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: props.countrySelected,
            dynamicFilterSelected: props.dynamicFilterSelected,
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            arrTopTopics: [],
            arrUnitPeriodTime: [],
            isLoading: true
        };
        this.updateStackedBarsData = this.updateStackedBarsData.bind(this);
        this.getStackedBarsData = this.getStackedBarsData.bind(this);
    }

    // Petición GET para obtener datos de las 7 gráficas de barras
    getStackedBarsData() {
        //Se construye la URL
        var dynamicFilterURL = "";
        var isJourneyVoc = "";

        if (this.props.location.pathname === Constants.PATH_DASHBOARD ||
            this.props.location.pathname === Constants.PATH_DICTIONARY) {
            for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                dynamicFilterURL = Constants.PARAM_SURVEY + encodeURIComponent(this.props.dynamicFilterSelected[i].label);
            }
        } else {
            if (this.props.dynamicFilterSelected !== null) {
                for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                    dynamicFilterURL = Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.props.dynamicFilterSelected[i].value);
                }
            }
            isJourneyVoc = Constants.JOURNEYS_ENDPOINT;
        }

        let URL_FORMATTED = Constants.BASE_API_URL + Constants.TOPICS_BAR_ENDPOINT +
            isJourneyVoc +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            dynamicFilterURL +
            Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected.value);

        if (this.props.segmentSelected !== null) {
            for (let i = 0; i < this.props.segmentSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENT + this.props.segmentSelected[i].value;
            }
        }
        if (this.props.productTypeSelected !== null) {
            for (let i = 0; i < this.props.productTypeSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_PRODUCT_TYPE + this.props.productTypeSelected[i].value;
            }
        }
        if (this.props.regionSelected !== null) {
            for (let i = 0; i < this.props.regionSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.regionSelected[i].value;
            }
        }
        if (this.props.contractSelected !== null) {
            for (let i = 0; i < this.props.contractSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.contractSelected[i].value;
            }
        }
        if (this.props.municipioSelected !== null) {
            for (let i = 0; i < this.props.municipioSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_MUNICIPIO + this.props.municipioSelected[i].value;
            }
        }
        if (this.props.segmentB2BSelected !== null) {
            for (let i = 0; i < this.props.segmentB2BSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENTB2B + this.props.segmentB2BSelected[i].value;
            }
        }

        URL_FORMATTED = URL_FORMATTED + Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY ||
            this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        //Se captura el token actual
        const authToken = getAuth().token_id;
        //Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        //Petición GET con librería Axios
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.updateStackedBarsData(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    // Método para cambiar estados del Gráfico y del Spinner
    updateStackedBarsData(graphic) {
        this.setState({
            arrTopTopics: graphic.arrTopTopics,
            arrUnitPeriodTime: graphic.arrUnitPeriodTime,
            isLoading: false,
            graphicContainsData: true
        });
    }

    // Retorna título para Card según la página (Resumen o Diccionario)
    titleIsDictionaryPage() {
        if (this.props.location.pathname === Constants.PATH_DICTIONARY
            || this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            return <font color="red">(Ambiente de pruebas)</font>;
        } else {
            return null;
        }
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.dynamicFilterSelected !== this.props.dynamicFilterSelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected !== this.props.segmentSelected
            || nextProps.productTypeSelected !== this.props.productTypeSelected
            || nextProps.regionSelected !== this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                dynamicFilterSelected: nextProps.dynamicFilterSelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                municipioSelected: nextProps.municipioSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoading: true
            }, function () { this.getStackedBarsData() });
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getStackedBarsData();
    }

    // Método para decidir renderizado de Loader o Componentes
    renderStackedBarComponent() {
        if (this.state.isLoading) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            );
        } else {
            if (this.state.graphicContainsData) {
                let flexVar = "";
                if (document.documentElement.clientWidth <= 500) {
                    flexVar = "column";
                } else {
                    flexVar = "";
                }
                return (
                    <div style={{ display: "flex", flexFlow: "row wrap", flexDirection: flexVar }}>
                        {this.state.arrTopTopics.map((object, index) => (
                            <div style={{ flex: 1, flexBasis: "50%" }} key={index} className="body">
                                <StackedBarNPS objectData={object} arrXAxis={this.state.arrUnitPeriodTime} stacking={'percent'} percent={'%'} />
                            </div>
                        ))
                        }
                    </div>
                );
            } else {
                return (
                    <MessageError />
                );
            }
        }
    }

    // Método del ciclo de vida del componente
    render() {
        return (
            <div className={this.props.size}>
                <div className="card h-100 mg-b-20">
                    <div className="card-header">
                        <h4 className="card-header-title">
                            {this.props.cardTitle} {this.titleIsDictionaryPage()}
                        </h4>
                    </div>
                    <div className="card-body pd-0" id={this.props.id}>
                        <div id="chart">
                            {this.renderStackedBarComponent()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
export default withRouter(CardStackedBar)