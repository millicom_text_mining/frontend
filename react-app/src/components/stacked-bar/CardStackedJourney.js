import React, { Component } from 'react';
import '../card-stats/CardStats.css';
import * as Constants from '../../constants/constants';
import StackedBarNPS from './StackedBarNPS';
import axios from 'axios';
import { getAuth } from "../../context/auth";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';
import MessageError from '../message-error-chart/MessageErrorChart';

class CardStackedBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: props.countrySelected,
            dynamicFilterSelected: props.dynamicFilterSelected,
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            filterSelected: props.filterSelected,
            arrTopJourneys: [],
            arrJourneysName: [],
            isLoading: true
        };
        this.updateStackedBarsData = this.updateStackedBarsData.bind(this);
        this.getStackedBarsData = this.getStackedBarsData.bind(this);
    }

    // Petición GET para obtener datos de la gráfica de barras
    getStackedBarsData() {
        //Se construye la URL

        let URL_FORMATTED = Constants.BASE_API_URL + Constants.JOURNEYS_ENDPOINT +
            '/nps' +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected) +
            Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected) + 
            Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        //Se captura el token actual
        const authToken = getAuth().token_id;
        //Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken } }
        //Petición GET con librería Axios
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.updateStackedBarsData(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    // Método para cambiar estados del Gráfico y del Spinner
    updateStackedBarsData(graphic) {
        this.setState({
            arrTopJourneys: graphic.arrJourneys,
            arrJourneysName: graphic.arrJourneysName,
            isLoading: false,
            graphicContainsData: true
        });
    }

    // Retorna título para Card según la página (Journey o Diccionario Joureny)
    titleIsDictionaryPage() {
        if (this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            return <font color="red">(Ambiente de pruebas)</font>;
        } else {
            return null;
        }
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.dynamicFilterSelected !== this.props.dynamicFilterSelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected != this.props.segmentSelected
            || nextProps.productTypeSelected != this.props.productTypeSelected
            || nextProps.regionSelected != this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                dynamicFilterSelected: nextProps.dynamicFilterSelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                municipioSelected: nextProps.municipioSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoading: true
            }, function () {
                this.getStackedBarsData()
            });
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getStackedBarsData();
    }

    // Método para decidir renderizado de Loader o Componentes
    renderStackedBarComponent() {
        if (this.state.isLoading) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            );
        } else {

            if (this.state.graphicContainsData) {
                return (
                    <div>
                        <StackedBarNPS objectData={this.state.arrTopJourneys[0]} arrXAxis={this.state.arrJourneysName} stacking={'normal'} percent={''} />
                    </div>
                );
            } else {
                return (
                    <MessageError />
                );
            }

        }
    }

    // Método del ciclo de vida del componente
    render() {
        return (
            <div className={this.props.size}>
                <div className="card h-100 mg-b-20">
                    <div className="card-header">
                        <h4 className="card-header-title">
                            {this.props.cardTitle} {this.titleIsDictionaryPage()}
                        </h4>
                    </div>
                    <div className="card-body pd-0" id={this.props.id}>
                        <div id="chart">
                            {this.renderStackedBarComponent()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

} 

export default withRouter(CardStackedBar)
