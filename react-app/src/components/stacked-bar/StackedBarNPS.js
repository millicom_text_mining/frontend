import React from "react";
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts'

export default function StackedBarNPS(props) {
    
    const { chartOptions } = {
        chartOptions: {
            chart: {
                zoomType: 'xy'
            },
            // Opción para exportar gráfica
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadXLS']
                    }
                }
            },
            title: {
                text: props.objectData === undefined ? "" : props.objectData.topicName
            },
            credits: {
                enabled: false
            },
            xAxis: [{
                // En <categories> se pasan las fechas/journeys
                categories: props.arrXAxis,
                crosshair: true
            }],
            // Se cargan 2 objetos para gráfica doble eje
            yAxis: [
                {
                    // Eje principal: % de Promotores, Neutros y Detractores
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    gridLineColor: 'rgba(0, 0, 0, 0)',
                    labels: {
                        format: '{value}' + props.percent,
                        style: {
                            color: 'rgba(0, 0, 0, 0)'
                            //color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, {
                    startOnTick: true,
                    max: 100,
                    min: -100,
                    // Eje secundario: NPS
                    title: {
                        //text: 'NPS',
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    gridLineColor: 'rgba(0, 0, 0, 0)',
                    labels: {
                        format: '{value}',
                        style: {
                            //color: Highcharts.getOptions().colors[0]
                            color: 'rgba(0, 0, 0, 0)'
                        }
                    },
                    opposite: true
                }],
            tooltip: {
                footerFormat: 'Total: <b>{point.total}</b>',
                shared: true
            },
            legend: {
                align: 'center'
            },
            plotOptions: {
                column: {
                    stacking: props.stacking
                }                
            },
            // En cada arreglo <data> de <series> se pasan los valores
            series: [{
                name: 'Promotores(9-10)',
                color: '#396BB4',
                type: 'column',
                yAxis: 0,
                data: props.objectData === undefined ? null : props.objectData.arrTotalPromoters,
                tooltip: {
                    valueSuffix: props.percent
                }
            }, {
                name: 'Neutros(7-8)',
                color: '#F7C913',
                type: 'column',
                yAxis: 0,
                data: props.objectData === undefined ? null : props.objectData.arrTotalNeutrals,
                tooltip: {
                    valueSuffix: props.percent
                }
            }, {
                name: 'Detractores(0-6)',
                color: '#CA2400',
                type: 'column',
                yAxis: 0,
                data: props.objectData === undefined ? null : props.objectData.arrTotalDetractors,
                tooltip: {
                    valueSuffix: props.percent
                }
            },
            {
                name: 'NPS',
                color: '#A140D8',
                data: props.objectData === undefined ? null : props.objectData.arrNps,
                //data: [64.84, 67.43, 60.63, 63.02],
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[10],
                    fillColor: 'white'
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                },
                yAxis: 1
            }]
        }
    };
    return (
        <div>
            <HighchartsReact
                Highcharts={Highcharts}
                options={chartOptions}
            />
        </div >
    );
}
