import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import highcharts from 'highcharts'
import './LineChart.css';

class LineChart extends Component {
    constructor(props) {
        super(props);
        var keyTest = true;
        let topicOptions = ['Modelo de actuación', 'Velocidad'];
        this.state = {
            chartOptions: {
                xAxis: {
                    title: {
                        text: ''
                    },
                    // Depende del filtro de PeriodTime en CardFilter
                    categories: highcharts.getOptions().lang.months
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    },
                    tickInterval: 5,
                    type: 'linear',
                    minorTickInterval: 'auto'
                },
                title: '',
                series: [
                    {
                        name: 'Acumulado',
                        data: [4, 4, 8, 11, 11, 15, 15]
                    }
                ]
            }, filterTopics: 'Tópico',
            legend: {
                align: 'center'
            },
            topicOptions: topicOptions,
            nameItemSelected: 'TopicLineChart',
            topicSelected: topicOptions[0]

        };
    }

    updateTopicsLineChart(value) {
        let baseUrl = 'https://jsonplaceholder.typicode.com/posts';
        if (this.keyTest) {
            this.setState(
                {
                    chartOptions: {
                        series: [
                            // Aquí se pasaría el arreglo obtenido del JSON
                            {
                                data: [4, 4, 8, 11, 11, 15, 15]
                            }
                        ]
                    },
                    topicSelected: value
                });            
        } else {
            this.setState(
                {
                    chartOptions: {
                        series: [
                            // Aquí se pasaría el arreglo obtenido del JSON
                            {
                                data: [3, 5, 5, 7, 17, 17, 20]
                            }
                        ]
                    },
                    topicSelected: value
                });
        }        
        this.keyTest = !this.keyTest
    }

    render() {
        const { chartOptions } = this.state;

        return (
            <div className="form-layout form-layout-4">
                <div className="dropdown-topic">
                </div>
                <HighchartsReact
                    highcharts={highcharts}
                    options={chartOptions}
                />
            </div>
        )
    }
}

export default LineChart