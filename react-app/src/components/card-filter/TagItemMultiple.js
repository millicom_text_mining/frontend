import React from "react";

// Mapea cada elemento seleccionado en los filtros multiples y lo renderiza en un tag
export default function TagItemMultiple(props) {
    if (props.filterItemObj != null) {
        return (
            <span>
                {props.filterItemObj.map((object) => (
                    <p className="badge badge-pill mg-t-10 badge-dark">{object.label}</p>
                ))
                }
            </span>
        )
    } else {
        return null;
    }
}
