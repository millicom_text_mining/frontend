import React, { Component } from 'react';
import './CardFilter.css';
import Select from 'react-select';
import * as Constants from '../../constants/constants';
import TagItem from './TagItem';
import axios from 'axios';
import $ from "jquery";
import { getAuth, refreshToken } from "../../context/auth";
import swal from 'sweetalert';
import DynamicFilter from './DynamicFilter';
import TagItemMultiple from './TagItemMultiple';
import ReactDOM from 'react-dom';
import App from '../../App';


// Texto
const textShowFilter = "Mostrar filtros";
const placeholderFilters = "Seleccione una opción...";

// Estilo aplicado a las opciones seleccionadas de los filtros múltiples
const colorStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),

    multiValue: (styles) => {
        return {
            ...styles,
            backgroundColor: '#EEEDEC ',
        };
    },
    multiValueLabel: (styles) => ({
        ...styles,
        color: 'black',
    }),
    multiValueRemove: (styles) => ({
        ...styles,
        color: '#00377D',
        backgroundColor: '#EEEDEC ',
        ':hover': {
            backgroundColor: '#d4f2fa',
        },
    }),
};

class CardFilter extends Component {
    constructor(props) {
        super(props);
        this.filtersToDisplay = [];
        this.state = {
            showFilter: false,
            textFilterButton: textShowFilter,
            countryOptions: [],
            channelOptions: [],
            typeSurveyOptions: [],
            periodTimeOptions: [
                {
                    "value": 'six_months',
                    "label": "Últimos 6 meses"
                },
                {
                    "value": 'five_months',
                    "label": "Últimos 5 meses"
                },
                {
                    "value": 'four_months',
                    "label": "Últimos 4 meses"
                },
                {
                    "value": 'thirty_days',
                    "label": "Últimos 30 días"
                },
                {
                    "value": 'current_month',
                    "label": "Mes actual"
                },
                {
                    "value": 'last_month',
                    "label": "Mes anterior"
                }
            ],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: [],
            filterOptions: [],
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            channelSelected: {
                value: props.channelSelected.value,
                label: props.channelSelected.label
            },
            typeSurveySelected: props.typeSurveySelected,
            periodTimeSelected: {
                value: props.periodTimeSelected.value,
                label: props.periodTimeSelected.label
            },
            filterSelected: {
                value: props.filterSelected.value,
                label: props.filterSelected.label
            },
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected
        };

        this.handleOnProcessBtn = this.handleOnProcessBtn.bind(this);
        this.handleOnCleanFilterBtn = this.handleOnCleanFilterBtn.bind(this);
    }

    render() {
        return (
            <div className="col-xl-12 mg-b-10">
                {/* Etiqueta con filtro seleccionados */}
                <div className="sticky">
                    <TagItem filterItemObj={this.state.countrySelected} />
                    <TagItem filterItemObj={this.state.channelSelected} />
                    <TagItemMultiple filterItemObj={this.state.typeSurveySelected} />
                    <TagItem filterItemObj={this.state.periodTimeSelected} />
                    <TagItemMultiple filterItemObj={this.state.segmentSelected} />
                    <TagItemMultiple filterItemObj={this.state.productTypeSelected} />
                    <TagItemMultiple filterItemObj={this.state.regionSelected} />
                    <TagItemMultiple filterItemObj={this.state.contractSelected} />
                    <TagItemMultiple filterItemObj={this.state.municipioSelected} />
                    <TagItemMultiple filterItemObj={this.state.segmentB2BSelected} />
                </div>
                {/* Opción/Botón de Mostrar filtros */}
                <button data-target="#collapseForm" className="btn btn-primary mg-t-10 float-right"
                    data-toggle="collapse" type="button" onClick={this.showFilter.bind(this)} title="Mostrar/Ocultar">
                    {this.state.textFilterButton}</button>
                {/* Filtros */}
                <div className="collapse mg-t-50" id="collapseForm">
                    <div className="card card-body">
                        <div className="row">
                            <div className="col-xl-6">
                                <div className="form-layout form-layout-4">
                                    <div className="row">
                                        {/* Nombre filtro País */}
                                        <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                            País:</label>

                                        {/* Selector País */}
                                        <div className="col-sm-8" id="countrySelector">
                                            <Select
                                                value={this.state.countrySelected}
                                                isDisabled={false}
                                                placeholder={placeholderFilters}
                                                onChange={this.handleCountryChange()}
                                                isLoading={false}
                                                isSearchable={false}
                                                options={this.state.countryOptions}
                                                filterOption={false}
                                            />
                                        </div>
                                    </div>

                                    <div className="row mg-t-20">
                                        {/* Nombre filtro Canal */}
                                        <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                            Canal:</label>

                                        {/* Selector Canal */}
                                        <div className="col-sm-8" id="channelSelector">
                                            <Select
                                                value={this.state.channelSelected}
                                                isDisabled={false}
                                                placeholder={placeholderFilters}
                                                onChange={this.handleChannelChange()}
                                                isLoading={false}
                                                isSearchable={false}
                                                options={this.state.channelOptions}
                                                filterOption={false}
                                            />
                                        </div>
                                    </div>

                                    <div className="row mg-t-20">
                                        {/* Nombre filtro Tipo Encuesta */}
                                        <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                            Tipo de encuesta:</label>

                                        {/* Selector Tipo Encuesta */}
                                        <div className="col-sm-8" id="typeSurveySelectorSelector">
                                            <Select
                                                value={this.state.typeSurveySelected}
                                                isDisabled={false}
                                                placeholder={placeholderFilters}
                                                onChange={this.handleTypeSurveyChange()}
                                                isLoading={false}
                                                isSearchable={true}
                                                options={this.state.typeSurveyOptions}
                                                isMulti={true}
                                                styles={colorStyles}
                                            />
                                        </div>
                                    </div>

                                    <div className="row mg-t-20">
                                        {/* Nombre filtro Período de tiempo */}
                                        <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                            Período de tiempo:</label>

                                        {/* Selector Período de tiempo */}
                                        <div className="col-sm-8" id="periodTimeSelector">
                                            <Select
                                                value={this.state.periodTimeSelected}
                                                isDisabled={false}
                                                placeholder={placeholderFilters}
                                                onChange={this.handlePeriodTimeChange()}
                                                isLoading={false}
                                                isSearchable={false}
                                                options={this.state.periodTimeOptions}
                                                filterOption={false}
                                            />
                                        </div>
                                    </div>
                                    {/*Filtros dinámicos */}
                                    <div id="container">
                                        <div id="containerFiltros">
                                            <DynamicFilter 
                                                filterItemObj={this.filtersToDisplay}
                                                segmentSelected={this.state.segmentSelected}
                                                productTypeSelected={this.state.productTypeSelected}
                                                regionSelected={this.state.regionSelected}
                                                contractSelected={this.state.contractSelected}
                                                municipioSelected={this.state.municipioSelected}
                                                segmentB2BSelected={this.state.segmentB2BSelected}
                                                filterOptions={this.state.filterOptions}
                                                onChangeMethod={this.onChangeMethod}
                                                onClick={this.handleDynamicFilterData.bind(this)} />
                                        </div>
                                    </div>
                                    {/*Selector de filtros dinámicos */}
                                    <div className="row mg-t-20">
                                        <button title="Añadir filtros como Región, Tipo Producto, etc" id="addFilter" data-target="#filterForm" className="circleStyle btn"
                                            data-toggle="collapse" type="button" >
                                            Añadir Filtros
                                            </button>
                                        <div className="collapse col-sm-7" id="filterForm">
                                            <div className="col-sm-9 filter" id="filterSelector">
                                                <Select
                                                    value={""}
                                                    isDisabled={false}
                                                    placeholder={""}
                                                    onChange={this.handleFilterChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.filterOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    {/* Botón para procesar filtros */}
                                    <button type="button" className="btn btn-primary mg-b-15" onClick={this.handleOnProcessBtn}
                                        data-target="#collapseForm" data-toggle="collapse" title="Actualización de Gráficos">Visualizar</button>

                                    {/* Botón para limpiar filtros 
                                <button type="button" className="btn btn-secondary" onClick={this.handleOnCleanFilterBtn} >Limpiar Filtros</button>
                                */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    //Método que obtiene los filtros seleccionados en el hijo DynamicFilter
    handleDynamicFilterData(props) {
        this.setState({
            segmentSelected: props[0],
            productTypeSelected: props[1],
            regionSelected: props[2],
            contractSelected: props[3],
            municipioSelected: props[4],
            segmentB2BSelected: props[5]
        },
            function () {
                //Mostrar el boton de añadir filtro y esconderlo cuando no se tengan opciones para añadir
                if (this.state.filterOptions.length === 0) {
                    document.getElementById('addFilter').style.visibility = 'hidden';
                } else {
                    document.getElementById('addFilter').style.visibility = 'visible';
                }
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
            });
    }

    // Función llamada cuando se cambia el período de tiempo seleccionado
    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando las variables no cambian
        if (nextProps.countrySelected !== this.state.countrySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                }
            });
        }
        if (nextProps.channelSelected !== this.state.channelSelected) {
            this.setState({
                channelSelected: {
                    value: nextProps.channelSelected.value,
                    label: nextProps.channelSelected.label
                }
            });
        }
        if (nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                typeSurveySelected: [{
                    value: nextProps.typeSurveySelected.value,
                    label: nextProps.typeSurveySelected.label
                }]
            });
        }
        if (nextProps.periodTimeSelected !== this.state.periodTimeSelected) {
            this.setState({
                periodTimeSelected: {
                    value: nextProps.periodTimeSelected.value,
                    label: nextProps.periodTimeSelected.label
                }
            });
        }
        if (nextProps.filterSelected !== this.state.filterSelected) {
            this.setState({
                filterSelected: {
                    value: nextProps.filterSelected.value,
                    label: nextProps.filterSelected.label
                }
            });
        }
        if (nextProps.segmentSelected !== this.state.segmentSelected) {
            this.setState({
                segmentSelected: [{
                    value: nextProps.segmentSelected.value,
                    label: nextProps.segmentSelected.label
                }]
            });
        }
        if (nextProps.productTypeSelected !== this.state.productTypeSelected) {
            this.setState({
                productTypeSelected: [{
                    value: nextProps.productTypeSelected.value,
                    label: nextProps.productTypeSelected.label
                }]
            });
        }
        if (nextProps.regionSelected !== this.state.regionSelected) {
            this.setState({
                regionSelected: [{
                    value: nextProps.regionSelected.value,
                    label: nextProps.regionSelected.label
                }]
            });
        }
        if (nextProps.contractSelected !== this.state.contractSelected) {
            this.setState({
                contractSelected: [{
                    value: nextProps.contractSelected.value,
                    label: nextProps.contractSelected.label
                }]
            });
        }
        if (nextProps.municipioSelected !== this.state.municipioSelected) {
            this.setState({
                municipioSelected: [{
                    value: nextProps.municipioSelected.value,
                    label: nextProps.municipioSelected.label
                }]
            });
        }
        if (nextProps.segmentB2BSelected !== this.state.segmentB2BSelected) {
            this.setState({
                segmentB2BSelected: [{
                    value: nextProps.segmentB2BSelected.value,
                    label: nextProps.segmentB2BSelected.label
                }]
            });
        }
    }

    //Método que se ejecuta con el botón Visualizar
    handleOnProcessBtn(e) {
        if (this.state.countrySelected != null && this.state.channelSelected != null &&
            this.state.typeSurveySelected[0] != null && this.state.periodTimeSelected != null) {
            this.setState({
                periodTimeLabel: this.state.periodTimeSelected.label,
                textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros"
            },
                //Retorna al padre los filtros
                this.props.onClick([this.state.countrySelected, this.state.channelSelected,
                this.state.typeSurveySelected, this.state.periodTimeSelected,
                this.state.segmentSelected, this.state.productTypeSelected, this.state.regionSelected,
                this.state.contractSelected, this.state.municipioSelected, this.state.segmentB2BSelected]));
        } else {
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        }

        this.showFilter();
        // Se refresca token cuando el usuario procesa filtros
        refreshToken();
    }

    handleOnCleanFilterBtn() {
        this.setState({
            countrySelected: {
                value: null,
                label: null
            },
            channelSelected: {
                value: null,
                label: null
            },
            typeSurveySelected: [{
                value: null,
                label: null
            }],
            segmentSelected: [{
                value: null,
                label: null
            }],
            productTypeSelected: [{
                value: null,
                label: null
            }],
            regionSelected: [{
                value: null,
                label: null
            }],
            contractSelected: [{
                value: null,
                label: null
            }],
            municipioSelected: [{
                value: null,
                label: null
            }],
            segmentB2BSelected: [{
                value: null,
                label: null
            }],
            channelOptions: [],
            typeSurveyOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        });
    }

    // Método para cambiar estados de: *Mostrar filtros *Texto de botón de filtros
    showFilter() {
        this.setState({
            showFilter: !this.state.showFilter,
            textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros"
        });
    }

    // Método que se ejecuta cuando hay un cambio de pais en el filtro 
    handleCountryChange = name => countryObject => {
        this.setState({
            //Cambia el valor al nuevo país seleccionado
            countrySelected: {
                value: countryObject.value,
                label: countryObject.label
            },
            //Resetea todos los demás filtros 
            channelSelected: null,
            typeSurveySelected: null,
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            channelOptions: [],
            typeSurveyOptions: [],
            filterOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                //Obtiene los canales para ese país
                this.getChannelsByCountry();
                //Obtiene las opciones de filtros dinámicos
                this.getFilterOptions();
                //Obtiene las opciones de cada filtro dinámico
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            });
    };

    // Método que se ejecuta cuando hay un cambio de canal en el filtro 
    handleChannelChange = name => channelObject => {
        this.setState({
            //Cambia el valor del canal al nuevo seleccionado
            channelSelected: {
                value: channelObject.value,
                label: channelObject.label
            },
            //Resetea todos los demás filtros 
            typeSurveySelected: null,
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            typeSurveyOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                //Obtiene las opciones de los tipos de encuesta para ese canal
                this.getTypeSurveysByChannel();
                //Obtiene las opciones de cada filtro dinámico
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            }
        );

    };

    handleTypeSurveyChange = name => typeSurveyObject => {
        this.setState({
            //Cambia el/los valor/valores del journey al nuevo seleccionado
            typeSurveySelected: typeSurveyObject,
            //Resetea todos los demás filtros 
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                //Obtiene las opciones de cada filtro dinámico
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            }
        );
    };

    handlePeriodTimeChange = name => periodTimeObject => {
        this.setState({
            //Cambia el valor del periodo de tiempo al nuevo seleccionado
            periodTimeSelected: {
                value: periodTimeObject.value,
                label: periodTimeObject.label
            },
            //Resetea todos los demás filtros 
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
        },
            function () {
                //Obtiene las opciones de cada filtro dinámico
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            });
    }

    //Funcion que se ejecuta cuando se cambia el valor del selector añadir filtro
    handleFilterChange = name => filterObject => {
        this.setState({
            filterSelected: {
                value: filterObject.value,
                label: filterObject.label
            }
        },
            function () {
                if (this.state.filterOptions.length - 1 === 0) {
                    document.getElementById('addFilter').style.visibility = 'hidden';
                } else {
                    document.getElementById('addFilter').style.visibility = 'visible';
                }
                $('#filterForm').removeClass('show');
                this.checkWhichFilter();
            }
        );
    }

    // Eliminar la opción del filtro seleccionado del selector añadir filtro
    removeFilterList(e) {
        var array = [...this.state.filterOptions]; // make a separate copy of the array
        var index = array.findIndex(x => x.value === e.value);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ filterOptions: array });
        }
    }

    //Obtener los valores del filtro seleccionado y agregarlo a los filtros que se deben mostrar
    checkWhichFilter() {
        let filterName = this.state.filterSelected.label;
        //Si el filtro seleccionado es Segmento
        if (this.state.filterSelected.value === "segmento") {
            this.getDynamicFilters("segmento");
            // Crea un objeto que se le envia por props a DynamicFilter
            let segmentElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "segmentSelector",
                options: this.state.segmentOptions,
                idButton: "closeSegment",
                containerId: "segmentContainer",
                selectedValue: this.state.filterSelected,
                selectedItem: this.state.segmentSelected
            }
            if (!(this.filtersToDisplay.includes(segmentElement))) {
                this.filtersToDisplay.push(segmentElement);
            }
        }
        //Si el filtro seleccionado es Tipo Producto
        if (this.state.filterSelected.value === "tipo_producto") {
            this.getDynamicFilters("tipo_producto");
            // Crea un objeto que se le envia por props a DynamicFilter
            let productTypeElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "productTypeSelector",
                options: this.state.productTypeOptions,
                idButton: "closeProductType",
                containerId: "productTypeContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(productTypeElement))) {
                this.filtersToDisplay.push(productTypeElement);
            }
        }
        //Si el filtro seleccionado es Región
        if (this.state.filterSelected.value === "region") {
            this.getDynamicFilters("region");
            // Crea un objeto que se le envia por props a DynamicFilter
            let regionElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "regionSelector",
                options: this.state.regionOptions,
                idButton: "closeRegion",
                containerId: "regionContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(regionElement))) {
                this.filtersToDisplay.push(regionElement);
            }
        }
        //Si el filtro seleccionado es Tipo Contrato
        if (this.state.filterSelected.value === "tipo_contrato") {
            this.getDynamicFilters("tipo_contrato");
            // Crea un objeto que se le envia por props a DynamicFilter
            let contractElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "contractSelector",
                options: this.state.contractOptions,
                idButton: "closeContract",
                containerId: "contractContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(contractElement))) {
                this.filtersToDisplay.push(contractElement);
            }
        }
        //Si el filtro seleccionado es Municipio
        if (this.state.filterSelected.value === "municipio") {
            this.getDynamicFilters("municipio");
            // Crea un objeto que se le envia por props a DynamicFilter
            let municipioElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "municipioSelector",
                options: this.state.municipioOptions,
                idButton: "closeMunicipio",
                containerId: "municipioContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(municipioElement))) {
                this.filtersToDisplay.push(municipioElement);
            }
        }
        //Si el filtro seleccionado es Segmento B2B
        if (this.state.filterSelected.value === "segmento_b2b") {
            this.getDynamicFilters("segmento_b2b");
            // Crea un objeto que se le envia por props a DynamicFilter
            let segmentB2BElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "segmentB2BSelector",
                options: this.state.segmentB2BOptions,
                idButton: "segmentB2BMunicipio",
                containerId: "segmentB2BContainer",
                selectedValue: this.state.filterSelected
            }

            if (!(this.filtersToDisplay.includes(segmentB2BElement))) {
                this.filtersToDisplay.push(segmentB2BElement);
            }
        }
        this.removeFilterList(this.state.filterSelected);
    }

    // Petición GET para obtener lista de Países
    getCountries() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.COUNTRIES_ENDPOINT;
        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }

        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadCountries(json.filter.arrCountries);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de canales dado un País
    getChannelsByCountry() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.CHANNELS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value;

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadChannels(json.filter.arrChannels);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de Tipos de Encuesta dados País y Canal
    getTypeSurveysByChannel() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.SURVEYS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value +
            Constants.PARAM_CHANNEL_ID + this.state.channelSelected.value;

        const authToken = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadTypeSurveys(json.filter.arrTypeSurveys);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener las opciones para el selector de añadir filtro
    getFilterOptions() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.FILTER_OPTIONS_ENDPOINT;

        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadFilterOptions(json.filter.arrFilter);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener los valores del filtro seleccionado
    getDynamicFilters(filterType) {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DYNAMIC_FILTERS_ENDPOINT
            + Constants.PARAM_COUNTRY + this.state.countrySelected.label
            + Constants.PARAM_PERIOD + this.state.periodTimeSelected.value
            + Constants.PARAM_FIELD_CONCAT + filterType; // Se envía el tipo de filtro dinámico

        // Se envian todos los tipos de encuesta seleccionados
        for (let i = 0; i < this.state.typeSurveySelected.label; i++) {
            URL_FORMATTED += Constants.PARAM_SURVEY + encodeURIComponent(this.state.typeSurveySelected[i].label);
        }

        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadDynamicFilters(json.filter.arrDynamic, filterType);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Asignar estado a countryOptions
    loadCountries(arrCountries) {
        const arrCountryOptions = arrCountries.map(country => ({
            value: country.key,
            label: country.value
        }));
        this.setState({
            countryOptions: arrCountryOptions
        });
    }

    // Asignar estado a channelOptions
    loadChannels(arrChannels) {
        const arrChannelOptions = arrChannels.map(channel => ({
            value: channel.key,
            label: channel.value
        }));
        this.setState({
            channelOptions: arrChannelOptions
        });
    }

    // Asignar estado a typeSurveyOptions
    loadTypeSurveys(arrTypeSurveys) {
        const arrTypeSurveyOptions = arrTypeSurveys.map(typeSurvey => ({
            value: typeSurvey.key,
            label: typeSurvey.value
        }));
        this.setState({
            typeSurveyOptions: arrTypeSurveyOptions
        });
    }

    // Cargar las opciones al selector de añadir filtro
    loadFilterOptions(arrFilters) {
        const arrFilterOptions = arrFilters.map(filter => ({
            value: filter.key,
            label: filter.value
        }));
        this.setState({
            filterOptions: arrFilterOptions
        });
    }

    // Cargar las opciones al selector del filtro seleccionado
    loadDynamicFilters(arrDynamic, filterType) {
        const arrFilterOptions = arrDynamic.map(filter => ({
            value: filter.key,
            label: filter.value
        }));
        if (filterType === "segmento") {
            this.setState({
                segmentOptions: arrFilterOptions
            });
        }
        if (filterType === "tipo_producto") {
            this.setState({
                productTypeOptions: arrFilterOptions
            });
        }
        if (filterType === "region") {
            this.setState({
                regionOptions: arrFilterOptions
            });
        }
        if (filterType === "tipo_contrato") {
            this.setState({
                contractOptions: arrFilterOptions
            });
        }
        if (filterType === "municipio") {
            this.setState({
                municipioOptions: arrFilterOptions
            });
        }
        if (filterType === "segmento_b2b") {
            this.setState({
                segmentB2BOptions: arrFilterOptions
            });
        }
    }

    //Refrescar valores de los filtros dinámicos 
    refreshPage(country, channel, survey, period) {
        if (country !== null && channel !== null && survey !== null && period !== null) {
            this.getDynamicFilters("segmento");
            this.getDynamicFilters("tipo_producto");
            this.getDynamicFilters("region");
            this.getDynamicFilters("tipo_contrato");
            this.getDynamicFilters("municipio");
            this.getDynamicFilters("segmento_b2b");
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getCountries();
        this.getChannelsByCountry();
        this.getTypeSurveysByChannel();
        this.getFilterOptions();
        this.getDynamicFilters("segmento");
        this.getDynamicFilters("tipo_producto");
        this.getDynamicFilters("region");
        this.getDynamicFilters("tipo_contrato");
        this.getDynamicFilters("municipio");
        this.getDynamicFilters("segmento_b2b");
    }
}
export default CardFilter