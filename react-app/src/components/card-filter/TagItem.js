import React from "react";

// Mapea cada elemento seleccionado en los filtros y lo renderiza en un tag
export default function TagItem(props) {
    if (props.filterItemObj != null) {
        return (
            <p className="badge badge-pill mg-t-10 badge-dark">{props.filterItemObj.label}</p>
        );
    } else{
        return null;
    }
}