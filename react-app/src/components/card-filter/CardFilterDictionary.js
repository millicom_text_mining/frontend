import React, { Component } from 'react';
import Select from 'react-select';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import { getAuth, refreshToken } from "../../context/auth";
import swal from 'sweetalert';
import $ from "jquery";
import dropify from "dropify";
import TagItem from './TagItem';
import ButtonLoadDictionary from '../../pages/dictionary/ButtonLoadDictionary';
import ButtonProcessDictionary from '../../pages/dictionary/ButtonProcessDictionary';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import Loader from 'react-loader-spinner';
import './CardFilterDictionary.css';
import { withRouter } from 'react-router-dom';
import TagItemMultiple from './TagItemMultiple';
import DynamicFilter from './DynamicFilter';

// let arrPeriodTimeOptions = ["Hoy", "Ayer", "Semana actual", "Semana pasada", "Mes pasado", "Últimos 4 meses"];

// Texto
const textShowFilter = "Mostrar filtros";
const placeholderFilters = "Seleccione una opción...";

const colorStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),

    multiValue: (styles) => {
        return {
            ...styles,
            backgroundColor: '#EEEDEC ',
        };
    },
    multiValueLabel: (styles) => ({
        ...styles,
        color: 'black',
    }),
    multiValueRemove: (styles) => ({
        ...styles,
        color: '#00377D',
        backgroundColor: '#EEEDEC ',
        ':hover': {
            backgroundColor: '#d4f2fa',
        },
    }),
};

class CardFilter extends Component {
    constructor(props) {
        super(props);
        this.filtersToDisplay = [];
        this.state = {
            showFilter: false,
            textFilterButton: textShowFilter,
            selectedFile: null,
            isProcessingDictionary: false,
            loadingMessage: null,
            countryOptions: [],
            channelOptions: [],
            typeSurveyOptions: [],
            periodTimeOptions: [
                {
                    "value": 'six_months',
                    "label": "Últimos 6 meses"
                },
                {
                    "value": 'five_months',
                    "label": "Últimos 5 meses"
                },
                {
                    "value": 'four_months',
                    "label": "Últimos 4 meses"
                },
                {
                    "value": 'thirty_days',
                    "label": "Últimos 30 días"
                },
                {
                    "value": 'current_month',
                    "label": "Mes actual"
                },
                {
                    "value": 'last_month',
                    "label": "Mes anterior"
                }
            ],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: [],
            filterOptions: [],
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            channelSelected: {
                value: props.channelSelected.value,
                label: props.channelSelected.label
            },
            /*typeSurveySelected: {
                value: props.typeSurveySelected.value,
                label: props.typeSurveySelected.label
            },*/
            typeSurveySelected: props.typeSurveySelected,
            periodTimeSelected: {
                value: props.periodTimeSelected.value,
                label: props.periodTimeSelected.label
            },
            filterSelected: {
                value: props.filterSelected.value,
                label: props.filterSelected.label
            },
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected
        };

        this.handleOnProcessBtn = this.handleOnProcessBtn.bind(this);
        this.handleOnCleanFilterBtn = this.handleOnCleanFilterBtn.bind(this);
        this.renderDragAndDrop = this.renderDragAndDrop.bind(this);
    }

    render() {
        return (
            <div className="col-xl-12">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="sticky">
                            <TagItem filterItemObj={this.state.countrySelected} />
                            <TagItem filterItemObj={this.state.channelSelected} />
                            {/*<TagItem filterItemObj={this.state.typeSurveySelected} />*/}
                            <TagItemMultiple filterItemObj={this.state.typeSurveySelected} />
                            <TagItem filterItemObj={this.state.periodTimeSelected} />
                            <TagItemMultiple filterItemObj={this.state.segmentSelected} />
                            <TagItemMultiple filterItemObj={this.state.productTypeSelected} />
                            <TagItemMultiple filterItemObj={this.state.regionSelected} />
                            <TagItemMultiple filterItemObj={this.state.contractSelected} />
                            <TagItemMultiple filterItemObj={this.state.municipioSelected} />
                            <TagItemMultiple filterItemObj={this.state.segmentB2BSelected} />
                        </div>

                        {/* Opción/Botón de Mostrar filtros */}
                        <button data-target="#collapseForm" className="btn btn-primary mg-b-10 float-right"
                            data-toggle="collapse" type="button" onClick={this.showFilter.bind(this)} title="Mostrar/Ocultar">
                            {this.state.textFilterButton}</button>
                    </div>
                </div>
                {/* Card */}
                <BlockUi tag="div" blocking={this.state.isProcessingDictionary} message={this.state.loadingMessage} loader={<Loader active type={Constants.DEFAULT_SPINNER_TYPE} color="#000640" />}>
                    <div className="collapse" id="collapseForm">
                        <div className="card card-body">{/* 
                            <div className="row">
                                <div class="webnots-notice webnots-notification-box">
                                    Actualmente se están procesando los siguientes diccionarios: <b>Uso - Home</b>, <b>Uso - Móvil</b>, <b>CC</b>
                                </div>
                            </div> */}
                            <div className="row">
                                <div className="col-xl-6">
                                    <div className="form-layout form-layout-4">
                                        {/* Filtros */}
                                        <div className="row">
                                            {/* Nombre filtro País */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                País:</label>

                                            {/* Selector País */}
                                            <div className="col-sm-8" id="countrySelector">
                                                <Select
                                                    value={this.state.countrySelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleCountryChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.countryOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>

                                        <div className="row mg-t-20">
                                            {/* Nombre filtro Canal */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                Canal:</label>

                                            {/* Selector Canal */}
                                            <div className="col-sm-8" id="channelSelector">
                                                <Select
                                                    value={this.state.channelSelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleChannelChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.channelOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>

                                        <div className="row mg-t-20">
                                            {/* Nombre filtro Tipo Encuesta */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                Tipo de encuesta:</label>

                                            {/* Selector Tipo Encuesta */}
                                            <div className="col-sm-8" id="typeSurveySelectorSelector">
                                                <Select
                                                    value={this.state.typeSurveySelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleTypeSurveyChange()}
                                                    isLoading={false}
                                                    isSearchable={true}
                                                    options={this.state.typeSurveyOptions}
                                                    isMulti={true}
                                                    styles={colorStyles}
                                                />
                                            </div>
                                        </div>

                                        <div className="row mg-t-20">
                                            {/* Nombre filtro Período de tiempo */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                Período de tiempo:</label>

                                            {/* Selector Período de tiempo */}
                                            <div className="col-sm-8" id="periodTimeSelector">
                                                <Select
                                                    value={this.state.periodTimeSelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handlePeriodTimeChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.periodTimeOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>
                                        {/*Filtros dinámicos */}
                                        <div id="container">
                                            <DynamicFilter filterItemObj={this.filtersToDisplay}
                                                segmentSelected={this.state.segmentSelected}
                                                productTypeSelected={this.state.productTypeSelected}
                                                regionSelected={this.state.regionSelected}
                                                contractSelected={this.state.contractSelected}
                                                municipioSelected={this.state.municipioSelected}
                                                segmentB2BSelected={this.state.segmentB2BSelected}
                                                filterOptions={this.state.filterOptions}
                                                onChangeMethod={this.onChangeMethod}
                                                onClick={this.handleDynamicFilterData.bind(this)} />
                                        </div>
                                        {/*Selector de filtros dinámicos */}
                                        <div className="row mg-t-20">
                                            <button title="Añadir filtros como Región, Tipo Producto, etc" id="addFilter" data-target="#filterForm" className="circleStyle btn"
                                                data-toggle="collapse" type="button" >
                                                {/* <img src={plusIcon} className="addFilter"></img> */}
                                                Añadir Filtros
                                            </button>
                                            <div className="collapse col-sm-7" id="filterForm">
                                                <div className="col-sm-9 filter" id="filterSelector">
                                                    <Select
                                                        value={""}
                                                        isDisabled={false}
                                                        placeholder={""}
                                                        onChange={this.handleFilterChange()}
                                                        isLoading={false}
                                                        isSearchable={false}
                                                        options={this.state.filterOptions}
                                                        filterOption={false}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>

                                        {/* ************* BOTONES INFERIORES *************** */}
                                        {/* Botón para Actualizar gráficos */}
                                        <button type="button" className="btn btn-primary mg-b-15" onClick={this.handleOnProcessBtn}
                                            data-target="#collapseForm" data-toggle="collapse" title="Actualización de Gráficos">Visualizar</button>

                                        {/* Botón para limpiar filtros 
                                <button type="button" className="btn btn-secondary" onClick={this.handleOnCleanFilterBtn} >Limpiar Filtros</button>
                                */}
                                    </div>
                                </div>
                                <div className="col-xl-6">
                                    {/* Campo para insertar diccionario */}
                                    {this.renderDragAndDrop()}
                                    <div>
                                        {/* Botón para Procesar Diccionario */}
                                        {/*                                     <button type="button" className="btn btn-primary-process float-right" onClick={this.checkValues}
                                        data-target="#collapseForm" data-toggle="collapse" title="Procesamiento de Diccionario">
                                        Calificar
                                    </button> */}
                                        <ButtonProcessDictionary
                                            user_id={getAuth().token.user.user_id}
                                            token={getAuth().token_id}
                                            countrySelected={this.state.countrySelected}
                                            channelSelected={this.state.channelSelected}
                                            typeSurveySelected={this.state.typeSurveySelected}
                                            periodTimeSelected={this.state.periodTimeSelected}
                                            onLoadingStatus={this.handleProcessDictionary.bind(this)}
                                            onClick={refreshToken()} />

                                        <ButtonLoadDictionary
                                            selectedFile={this.state.selectedFile}
                                            user_id={getAuth().token.user.user_id}
                                            token={getAuth().token_id}
                                            countrySelected={this.state.countrySelected}
                                            channelSelected={this.state.channelSelected}
                                            typeSurveySelected={this.state.typeSurveySelected}
                                            periodTimeSelected={this.state.periodTimeSelected}
                                            onLoadingStatus={this.handleProcessDictionary.bind(this)}
                                            onClick={refreshToken()} />

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </BlockUi>
            </div>
        );
    }

    handleDynamicFilterData(props) {
        this.setState({
            segmentSelected: props[0],
            productTypeSelected: props[1],
            regionSelected: props[2],
            contractSelected: props[3],
            municipioSelected: props[4],
            segmentB2BSelected: props[5]
        },
            function () {
                //Mostrar el boton de añadir filtro y esconderlo cuando no se tengan opciones para añadir
                if (this.state.filterOptions.length === 0) {
                    document.getElementById('addFilter').style.visibility = 'hidden';
                } else {
                    document.getElementById('addFilter').style.visibility = 'visible';
                }
                localStorage.removeItem("Filter");
                localStorage.setItem("Filter", JSON.stringify(this.state));
            });
    }

    // Props es un arreglo: [0] isLoadingDictionary
    handleProcessDictionary(props) {
        this.setState({
            isProcessingDictionary: props[0],
            loadingMessage: props[1]
        });
    }

    // Función que retorna la sección para carga de diccionario según página (Resumen o Diccionario)
    renderDragAndDrop() {
        if (this.props.location.pathname === Constants.PATH_DICTIONARY) {
            return (
                <input type="file" className="dropify drag-drop" onChange={this.fileChangedHandler}
                    data-allowed-file-extensions="csv" data-height="260" />
            );
        } else {
            return null;
        }
    }

    // Función que se invoca cuando se cambia el Diccionario insertado
    fileChangedHandler = event => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    // Función llamada cuando se cambia el período de tiempo seleccionado
    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando las variables no cambian
        if (nextProps.countrySelected !== this.state.countrySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                }
            });
        }
        if (nextProps.channelSelected !== this.state.channelSelected) {
            this.setState({
                channelSelected: {
                    value: nextProps.channelSelected.value,
                    label: nextProps.channelSelected.label
                }
            });
        }
        if (nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                typeSurveySelected: [{
                    value: nextProps.typeSurveySelected.value,
                    label: nextProps.typeSurveySelected.label
                }]
            });
        }
        if (nextProps.periodTimeSelected !== this.state.periodTimeSelected) {
            this.setState({
                periodTimeSelected: {
                    value: nextProps.periodTimeSelected.value,
                    label: nextProps.periodTimeSelected.label
                }
            });
        }
        if (nextProps.filterSelected !== this.state.filterSelected) {
            this.setState({
                filterSelected: {
                    value: nextProps.filterSelected.value,
                    label: nextProps.filterSelected.label
                }
            });
        }
        if (nextProps.segmentSelected !== this.state.segmentSelected) {
            this.setState({
                segmentSelected: [{
                    value: nextProps.segmentSelected.value,
                    label: nextProps.segmentSelected.label
                }]
            });
        }
        if (nextProps.productTypeSelected !== this.state.productTypeSelected) {
            this.setState({
                productTypeSelected: [{
                    value: nextProps.productTypeSelected.value,
                    label: nextProps.productTypeSelected.label
                }]
            });
        }
        if (nextProps.regionSelected !== this.state.regionSelected) {
            this.setState({
                regionSelected: [{
                    value: nextProps.regionSelected.value,
                    label: nextProps.regionSelected.label
                }]
            });
        }
        if (nextProps.contractSelected !== this.state.contractSelected) {
            this.setState({
                contractSelected: [{
                    value: nextProps.contractSelected.value,
                    label: nextProps.contractSelected.label
                }]
            });
        }
        if (nextProps.municipioSelected !== this.state.municipioSelected) {
            this.setState({
                municipioSelected: [{
                    value: nextProps.municipioSelected.value,
                    label: nextProps.municipioSelected.label
                }]
            });
        }
        if (nextProps.segmentB2BSelected !== this.state.segmentB2BSelected) {
            this.setState({
                segmentB2BSelected: [{
                    value: nextProps.segmentB2BSelected.value,
                    label: nextProps.segmentB2BSelected.label
                }]
            });
        }
    }

    handleOnProcessBtn(e) {
        if (this.state.countrySelected != null && this.state.channelSelected != null &&
            this.state.typeSurveySelected[0] != null && this.state.periodTimeSelected != null) {
                this.setState({
                    periodTimeLabel: this.state.periodTimeSelected.label,
                    textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros"
                },
                    //Retorna al padre los filtros
                    this.props.onClick([this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected,
                    this.state.segmentSelected, this.state.productTypeSelected, this.state.regionSelected,
                    this.state.contractSelected, this.state.municipioSelected, this.state.segmentB2BSelected]));
        } else {
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        }
        this.showFilter();

        // Se refresca token cuando el usuario procesa filtros
        refreshToken();
    }

    handleOnCleanFilterBtn() {
        this.setState({
            countrySelected: {
                value: null,
                label: null
            },
            channelSelected: {
                value: null,
                label: null
            },
            typeSurveySelected: [{
                value: null,
                label: null
            }],
            segmentSelected: [{
                value: null,
                label: null
            }],
            productTypeSelected: [{
                value: null,
                label: null
            }],
            regionSelected: [{
                value: null,
                label: null
            }],
            contractSelected: [{
                value: null,
                label: null
            }],
            municipioSelected: [{
                value: null,
                label: null
            }],
            segmentB2BSelected: [{
                value: null,
                label: null
            }],
            channelOptions: [],
            typeSurveyOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        });
    }
    // Método para cambiar estados de: *Mostrar filtros *Texto de botón de filtros
    showFilter() {
        this.setState({
            showFilter: !this.state.showFilter,
            textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros"
        });
    }

    resetSelectors = value => {
        this.setState(prevState => ({
            countrySelected: {
                value: null,
                label: null
            },
            channelSelected: null,
            typeSurveySelected: null,
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            channelOptions: [],
            typeSurveyOptions: [],
            filterOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        }));
    };

    handleCountryChange = name => countryObject => {
        this.setState({
            countrySelected: {
                value: countryObject.value,
                label: countryObject.label
            },
            channelSelected: null,
            typeSurveySelected: null,
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            channelOptions: [],
            typeSurveyOptions: [],
            filterOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                this.getChannelsByCountry(countryObject.value);
                this.getFilterOptions();
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            });
    };

    handleChannelChange = name => channelObject => {
        this.setState({
            channelSelected: {
                value: channelObject.value,
                label: channelObject.label
            },
            typeSurveySelected: null,
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            typeSurveyOptions: [],
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                this.getTypeSurveysByChannel();
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            }
        );

    };

    handleTypeSurveyChange = name => typeSurveyObject => {
        this.setState({
            //Cambia el/los valor/valores del journey al nuevo seleccionado
            typeSurveySelected: typeSurveyObject,
            //Resetea todos los demás filtros 
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        },
            function () {
                this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                    this.state.typeSurveySelected, this.state.periodTimeSelected);
            }
        );
    };

    handlePeriodTimeChange = name => periodTimeObject => {
        this.setState({
            periodTimeSelected: {
                value: periodTimeObject.value,
                label: periodTimeObject.label
            },
            segmentSelected: null,
            productTypeSelected: null,
            regionSelected: null,
            contractSelected: null,
            municipioSelected: null,
            segmentB2BSelected: null,
            segmentOptions: [],
            productTypeOptions: [],
            regionOptions: [],
            contractOptions: [],
            municipioOptions: [],
            segmentB2BOptions: []
        }, function () {
            this.refreshPage(this.state.countrySelected, this.state.channelSelected,
                this.state.typeSurveySelected[0], this.state.periodTimeSelected);
        });
    }

    //Funcion que se ejecuta cuando se cambia el valor del selector añadir filtro
    handleFilterChange = name => filterObject => {
        this.setState({
            filterSelected: {
                value: filterObject.value,
                label: filterObject.label
            }
        },
            function () {
                if (this.state.filterOptions.length - 1 === 0) {
                    document.getElementById('addFilter').style.visibility = 'hidden';
                } else {
                    document.getElementById('addFilter').style.visibility = 'visible';
                }
                $('#filterForm').removeClass('show');
                this.checkWhichFilter();
            }
        );
    }

    // Eliminar la opción del filtro seleccionado del selector añadir filtro
    removeFilterList(e) {
        var array = [...this.state.filterOptions]; // make a separate copy of the array
        var index = array.findIndex(x => x.value === e.value);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ filterOptions: array });
        }
    }

    //Obtener los valores del filtro seleccionado y agregarlo a los filtros que se deben mostrar
    checkWhichFilter() {
        let filterName = this.state.filterSelected.label;
        //Si el filtro seleccionado es Segmento
        if (this.state.filterSelected.value === "segmento") {
            this.getDynamicFilters("segmento");
            let segmentElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "segmentSelector",
                options: this.state.segmentOptions,
                idButton: "closeSegment",
                containerId: "segmentContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(segmentElement))) {
                this.filtersToDisplay.push(segmentElement);
            }
        }
        //Si el filtro seleccionado es Tipo Producto
        if (this.state.filterSelected.value === "tipo_producto") {
            this.getDynamicFilters("tipo_producto");
            let productTypeElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "productTypeSelector",
                options: this.state.productTypeOptions,
                idButton: "closeProductType",
                containerId: "productTypeContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(productTypeElement))) {
                this.filtersToDisplay.push(productTypeElement);
            }
        }
        //Si el filtro seleccionado es Región
        if (this.state.filterSelected.value === "region") {
            this.getDynamicFilters("region");
            let regionElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "regionSelector",
                options: this.state.regionOptions,
                idButton: "closeRegion",
                containerId: "regionContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(regionElement))) {
                this.filtersToDisplay.push(regionElement);
            }
        }
        //Si el filtro seleccionado es Tipo Contrato
        if (this.state.filterSelected.value === "tipo_contrato") {
            this.getDynamicFilters("tipo_contrato");
            let contractElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "contractSelector",
                options: this.state.contractOptions,
                idButton: "closeContract",
                containerId: "contractContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(contractElement))) {
                this.filtersToDisplay.push(contractElement);
            }
        }
        //Si el filtro seleccionado es Municipio
        if (this.state.filterSelected.value === "municipio") {
            this.getDynamicFilters("municipio");
            let municipioElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "municipioSelector",
                options: this.state.municipioOptions,
                idButton: "closeMunicipio",
                containerId: "municipioContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(municipioElement))) {
                this.filtersToDisplay.push(municipioElement);
            }
        }
        // Si el filtro seleccionado es Segmento B2B
        if (this.state.filterSelected.value === "segmento_b2b") {
            this.getDynamicFilters("segmento_b2b");
            let segmentB2BElement =
            {
                id: this.state.filterSelected.value,
                title: filterName,
                idFilter: "segmentB2BSelector",
                options: this.state.municipioOptions,
                idButton: "closeSegmentB2B",
                containerId: "segmentB2BContainer",
                selectedValue: this.state.filterSelected
            }
            if (!(this.filtersToDisplay.includes(segmentB2BElement))) {
                this.filtersToDisplay.push(segmentB2BElement);
            }
        }
        this.removeFilterList(this.state.filterSelected);
    }

    // Petición GET para obtener lista de Países
    getCountries() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.COUNTRIES_ENDPOINT + "?type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadCountries(json.filter.arrCountries);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de canales dado un País
    getChannelsByCountry() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.CHANNELS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value + "&type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadChannels(json.filter.arrChannels);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de Tipos de Encuesta dados País y Canal
    getTypeSurveysByChannel() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.SURVEYS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value +
            Constants.PARAM_CHANNEL_ID + this.state.channelSelected.value + "&type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadTypeSurveys(json.filter.arrTypeSurveys);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener las opciones para el selector de añadir filtro
    getFilterOptions() {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.FILTER_OPTIONS_ENDPOINT;

        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadFilterOptions(json.filter.arrFilter);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener los valores del filtro seleccionado
    getDynamicFilters(filterType) {
        let URL_FORMATTED = Constants.BASE_API_URL + Constants.DYNAMIC_FILTERS_ENDPOINT
            + Constants.PARAM_COUNTRY + this.state.countrySelected.label
            + Constants.PARAM_PERIOD + this.state.periodTimeSelected.value
            + Constants.PARAM_FIELD_CONCAT + filterType;
        for (let i = 0; i < this.state.typeSurveySelected.length; i++) {
            URL_FORMATTED += Constants.PARAM_SURVEY + encodeURIComponent(this.state.typeSurveySelected[i].label);
        }

        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadDynamicFilters(json.filter.arrDynamic, filterType);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Asignar estado a countryOptions
    loadCountries(arrCountries) {
        const arrCountryOptions = arrCountries.map(country => ({
            value: country.key,
            label: country.value
        }));
        this.setState({
            countryOptions: arrCountryOptions
        });
    }

    // Asignar estado a channelOptions
    loadChannels(arrChannels) {
        const arrChannelOptions = arrChannels.map(channel => ({
            value: channel.key,
            label: channel.value
        }));
        this.setState({
            channelOptions: arrChannelOptions
        });
    }

    // Asignar estado a typeSurveyOptions
    loadTypeSurveys(arrTypeSurveys) {
        const arrTypeSurveyOptions = arrTypeSurveys.map(typeSurvey => ({
            value: typeSurvey.key,
            label: typeSurvey.value
        }));
        this.setState({
            typeSurveyOptions: arrTypeSurveyOptions
        });
    }

    // Cargar las opciones al selector de añadir filtro
    loadFilterOptions(arrFilters) {
        const arrFilterOptions = arrFilters.map(filter => ({
            value: filter.key,
            label: filter.value
        }));
        this.setState({
            filterOptions: arrFilterOptions
        });
    }

    // Cargar las opciones al selector del filtro seleccionado
    loadDynamicFilters(arrDynamic, filterType) {
        const arrFilterOptions = arrDynamic.map(filter => ({
            value: filter.key,
            label: filter.value
        }));
        if (filterType === "segmento") {
            this.setState({
                segmentOptions: arrFilterOptions
            });
        }
        if (filterType === "tipo_producto") {
            this.setState({
                productTypeOptions: arrFilterOptions
            });
        }
        if (filterType === "region") {
            this.setState({
                regionOptions: arrFilterOptions
            });
        }
        if (filterType === "tipo_contrato") {
            this.setState({
                contractOptions: arrFilterOptions
            });
        }
        if (filterType === "municipio") {
            this.setState({
                municipioOptions: arrFilterOptions
            });
        }
        if (filterType === "segmento_b2b") {
            this.setState({
                segmentB2BOptions: arrFilterOptions
            });
        }
    }

    //Refrescar valores de los filtros dinámicos 
    refreshPage(country, channel, survey, period) {
        if (country !== null && channel !== null && survey !== null && period !== null) {
            this.getDynamicFilters("segmento");
            this.getDynamicFilters("tipo_producto");
            this.getDynamicFilters("region");
            this.getDynamicFilters("tipo_contrato");
            this.getDynamicFilters("municipio");
            this.getDynamicFilters("segmento_b2b");
        }
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getCountries();
        this.getChannelsByCountry();
        this.getTypeSurveysByChannel();
        this.getFilterOptions();
        this.getDynamicFilters("segmento");
        this.getDynamicFilters("tipo_producto");
        this.getDynamicFilters("region");
        this.getDynamicFilters("tipo_contrato");
        this.getDynamicFilters("municipio");
        this.getDynamicFilters("segmento_b2b");


        $(".dropify").dropify({
            messages: {
                'default': Constants.DRAGANDDROP_DEFAULT,
                'replace': Constants.DRAGANDDROP_REPLACE,
                'remove': Constants.DRAGANDDROP_REMOVE,
                'error': Constants.DRAGANDDROP_ERROR
            },
            error: {
                'fileFormat': Constants.DRAGANDDROP_ERROR_FILE_FORMAT
            }
        });
    }
}
export default withRouter(CardFilter)