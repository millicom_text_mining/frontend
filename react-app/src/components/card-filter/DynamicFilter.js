import React, { Component } from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';

import './CardFilterDictionary.css';

const animatedComponents = makeAnimated();

//Estilos de color para los filtros múltiples
const colorStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),

    multiValue: (styles) => {
        return {
            ...styles,
            backgroundColor: '#EEEDEC ',
        };
    },
    multiValueLabel: (styles) => ({
        ...styles,
        color: 'black',
    }),
    multiValueRemove: (styles) => ({
        ...styles,
        color: '#00377D',
        backgroundColor: '#EEEDEC ',
        ':hover': {
            backgroundColor: '#d4f2fa',
        },
    }),
};

class DynamicFilters extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterItemObj: props.filterItemObj,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            filterOptions: props.filterOptions
        }
    }

    // Eliminar la opcion seleccionada del selector añadir filtro
    removeFilterList(e) {
        var array = [...this.state.filterItemObj]; // make a separate copy of the array
        var index = array.findIndex(x => x.title === e.label);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ filterItemObj: array });
        }
    }

    //Saber que filtro es el seleccionado y ponerlo en el value del Select
    elementSelected(container) {
        if (container == 'segmentContainer') {
            return(this.props.segmentSelected);   
        }
        if (container == 'productTypeContainer') {
            return(this.props.productTypeSelected);
        }
        if (container == 'regionContainer') {
            return(this.props.regionSelected);
        }
        if (container == 'contractContainer') {
            return(this.props.contractSelected);
        }
        if (container == 'municipioContainer') {
            return(this.props.municipioSelected);  
        }
        if (container == 'segmentB2BContainer') {
            return(this.props.segmentB2BSelected);  
        }   
    }

    // Eliminar el filtro dinámico de la vista y los valores seleccionados
    deleteFilter(container, selectedValue) {
        this.props.filterOptions.push(selectedValue);
        if (container == 'segmentContainer') {
            this.setState({
                segmentSelected: []
            },
            function() {
                this.returnProps();
            });       
        }
        if (container == 'productTypeContainer') {
            this.setState({
                productTypeSelected: []
            },
            function() {
                this.returnProps();
            });  
        }
        if (container == 'regionContainer') {
            this.setState({
                regionSelected: []
            },
            function() {
                this.returnProps();
            });  
        }
        if (container == 'contractContainer') {
            this.setState({
                contractSelected: []
            },
            function() {
                this.returnProps();
            });  
        }
        if (container == 'municipioContainer') {
            this.setState({
                municipioSelected: []
            },
            function() {
                this.returnProps();
            });  
        }
        if (container == 'segmentB2BContainer') {
            this.setState({
                segmentB2BSelected: []
            },
            function() {
                this.returnProps();
            });  
        }
        this.removeFilterList(selectedValue);
        document.getElementById(container).remove();
        this.returnProps();
    }

    render() {
        if (this.props.filterItemObj != null) {
            return (
                <div>
                    {this.props.filterItemObj.map((object) => (
                        <div className="row mg-t-20" id={object.containerId}>
                            {/* Nombre filtro Período de tiempo */}
                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                {object.title}</label>
                            {/* Selector Dinámico */}
                            <div className="col-sm-8" id={object.idFilter}>
                                <Select
                                    value={this.elementSelected(object.containerId)}
                                    isDisabled={false}
                                    placeholder={""}
                                    onChange={this.onChangeMethod(object.id)}
                                    isLoading={false}
                                    options={object.options}
                                    styles={colorStyles}
                                    components={animatedComponents}
                                    isMulti={true}
                                    isSearchable={true}
                                />
                            </div>
                            {/* Botón para eliminar el filtro de la vista **/}
                            <IconButton id={object.idButton} aria-label="delete"
                                onClick={() => this.deleteFilter(object.containerId, object.selectedValue)}
                                style={{ backgroundColor: 'transparent' }}>
                                <ClearIcon style={{ fontSize: 20 }} />
                            </IconButton>
                        </div>
                    ), this)
                    }
                </div>
            )
        } else {
            return null;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.filterItemObj !== this.props.filterItemObj) {
            this.setState({
                filterItemObj: [{
                    title: nextProps.filterItemObj.title,
                    idFilter: nextProps.filterItemObj.idFilter,
                    options: nextProps.filterItemObj.options,
                    idButton: nextProps.filterItemObj.idButton,
                    containerId: nextProps.filterItemObj.containerId,
                    selectedValue: nextProps.filterItemObj.selectedValue
                }]
            });
        }
        if (nextProps.filterOptions !== this.props.filterOptions) {
            this.setState({
                filterOptions: nextProps.filterOptions
            });
        }
        if (nextProps.segmentSelected !== this.state.segmentSelected) {
            this.setState({
                /*segmentSelected: [{
                    value: nextProps.segmentSelected.value,
                    label: nextProps.segmentSelected.label
                }]*/
                segmentSelected: nextProps.segmentSelected
            });
        }
        if (nextProps.productTypeSelected !== this.state.productTypeSelected) {
            this.setState({
                /*productTypeSelected: [{
                    value: nextProps.productTypeSelected.value,
                    label: nextProps.productTypeSelected.label
                }]*/
                productTypeSelected: nextProps.productTypeSelected
            });
        }
        if (nextProps.regionSelected !== this.state.regionSelected) {
            this.setState({
                /*regionSelected: [{
                    value: nextProps.regionSelected.value,
                    label: nextProps.regionSelected.label
                }]*/
                regionSelected: nextProps.regionSelected
            });
        }
        if (nextProps.contractSelected !== this.state.contractSelected) {
            this.setState({
                /*contractSelected: [{
                    value: nextProps.contractSelected.value,
                    label: nextProps.contractSelected.label
                }]*/
                contractSelected: nextProps.contractSelected
            });
        }
        if (nextProps.municipioSelected !== this.state.municipioSelected) {
            this.setState({
                /*municipioSelected: [{
                    value: nextProps.municipioSelected.value,
                    label: nextProps.municipioSelected.label
                }]*/
                municipioSelected: nextProps.municipioSelected
            });
        }
        if (nextProps.segmentB2BSelected !== this.state.segmentB2BSelected) {
            this.setState({
                /*segmentB2BSelected: [{
                    value: nextProps.segmentB2BSelected.value,
                    label: nextProps.segmentB2BSelected.label
                }]*/
                segmentB2BSelected: nextProps.segmentB2BSelected
            });
        }
    }

    //Método que se llama cuando se genera cambio en el filtro
    onChangeMethod(id) {
        if(id === 'segmento') {
            return this.handleSegmentChange();
        } 
        if(id === 'tipo_producto') {
            return this.handleProductTypeChange();
        }
        if(id === 'region') {
            return this.handleRegionChange();
        }
        if(id === 'tipo_contrato') {
            return this.handleContractTypeChange();
        }
        if(id === 'municipio') {
            return this.handleMunicipioChange();
        }
        if(id === 'segmento_b2b') {
            return this.handleSegmentB2BChange();
        }
    }

    handleSegmentChange = name => segmentObject => {
        this.setState({
            segmentSelected: segmentObject
        },
        function() {
            this.returnProps();
        });
    }

    handleProductTypeChange = name => productTypeObject => {
        this.setState({
            productTypeSelected: productTypeObject
        },
        function() {
            this.returnProps();
        });
    }

    handleRegionChange = name => regionObject => {
        this.setState({
            regionSelected: regionObject
        },
        function() {
            this.returnProps();
        });
    }

    handleContractTypeChange = name => contractObject => {
        this.setState({
            contractSelected: contractObject
        },
        function() {
            this.returnProps();
        });
    }

    handleMunicipioChange = name => municipioObject => {
        this.setState({
            municipioSelected: municipioObject
        },
        function() {
            this.returnProps();
        });
    }

    handleSegmentB2BChange = name => segmentB2BObject => {
        this.setState({
            segmentB2BSelected: segmentB2BObject
        },
        function() {
            this.returnProps();
        });
    }

    //Retorna al padre los filtros seleccionados
    returnProps() {
        this.props.onClick([this.state.segmentSelected, this.state.productTypeSelected, 
                            this.state.regionSelected, this.state.contractSelected, 
                            this.state.municipioSelected, this.state.segmentB2BSelected]);
    }


    componentDidMount() {

    }
}


export default DynamicFilters;