import React, { Component } from 'react';
import Select from 'react-select';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import { getAuth, refreshToken } from "../../context/auth";
import swal from 'sweetalert';
import $ from "jquery";
import dropify from "dropify";
import TagItem from './TagItem';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import Loader from 'react-loader-spinner';
import './CardFilterDictionary.css';
import { withRouter } from 'react-router-dom';
import ButtonLoadStopwords from '../../pages/stopwords/ButtonLoadStopwords';

// Texto
const textShowFilter = "Mostrar filtros";
const placeholderFilters = "Seleccione una opción...";

class CardFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showFilter: false,
            textFilterButton: textShowFilter,
            selectedFile: null,
            isProcessingStopwords: false,
            loadingMessage: null,
            countryOptions: [],
            channelOptions: [],
            typeSurveyOptions: [],
            countrySelected: {
                value: props.countrySelected.value,
                label: props.countrySelected.label
            },
            channelSelected: {
                value: props.channelSelected.value,
                label: props.channelSelected.label
            },
            typeSurveySelected: {
                value: props.typeSurveySelected.value,
                label: props.typeSurveySelected.label
            }
        };

        this.handleOnProcessBtn = this.handleOnProcessBtn.bind(this);
        this.handleOnCleanFilterBtn = this.handleOnCleanFilterBtn.bind(this);
        this.renderDragAndDrop = this.renderDragAndDrop.bind(this);
    }

    render() {
        return (
            <div className="col-xl-12">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="sticky">
                            <TagItem filterItemObj={this.state.countrySelected} />
                            <TagItem filterItemObj={this.state.channelSelected} />
                            <TagItem filterItemObj={this.state.typeSurveySelected} />
                        </div>
                    </div>
                </div>
                {/* Card */}
                <BlockUi tag="div" blocking={this.state.isProcessingStopwords} message={this.state.loadingMessage} loader={<Loader active type={Constants.DEFAULT_SPINNER_TYPE} color="#000640" />}>
                    <div id="collapseForm" className="mg-t-50 ">
                        <div className="card card-body">
                            <div className="row">
                                <div className="col-xl-6">
                                    <div className="form-layout form-layout-4">
                                        {/* Filtros */}
                                        <div className="row">
                                            {/* Nombre filtro País */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                País:</label>

                                            {/* Selector País */}
                                            <div className="col-sm-8" id="countrySelector">
                                                <Select
                                                    value={this.state.countrySelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleCountryChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.countryOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>

                                        <div className="row mg-t-20">
                                            {/* Nombre filtro Canal */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                Canal:</label>

                                            {/* Selector Canal */}
                                            <div className="col-sm-8" id="channelSelector">
                                                <Select
                                                    value={this.state.channelSelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleChannelChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.channelOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>

                                        <div className="row mg-t-20">
                                            {/* Nombre filtro Tipo Encuesta */}
                                            <label className="col-sm-3 form-control-label" style={{ color: 'black' }}>
                                                Tipo de encuesta:</label>

                                            {/* Selector Tipo Encuesta */}
                                            <div className="col-sm-8" id="typeSurveySelectorSelector">
                                                <Select
                                                    value={this.state.typeSurveySelected}
                                                    isDisabled={false}
                                                    placeholder={placeholderFilters}
                                                    onChange={this.handleTypeSurveyChange()}
                                                    isLoading={false}
                                                    isSearchable={false}
                                                    options={this.state.typeSurveyOptions}
                                                    filterOption={false}
                                                />
                                            </div>
                                        </div>

                                    </div>
                                    <div>

                                        {/* ************* BOTONES INFERIORES *************** */}
                                        {/* Botón para Actualizar gráficos */}
                                        <button type="button" className="btn btn-primary button_visualizar" onClick={this.handleOnProcessBtn}
                                            data-toggle="collapse" title="Actualización de Gráficos">Visualizar</button>
                                    </div>
                                </div>
                                <div className="col-xl-6">
                                    {/* Campo para insertar diccionario */}
                                    {this.renderDragAndDrop()}
                                    <div>
                                        <ButtonLoadStopwords
                                            selectedFile={this.state.selectedFile}
                                            user_id={getAuth().token.user.user_id}
                                            token={getAuth().token_id}
                                            countrySelected={this.state.countrySelected}
                                            channelSelected={this.state.channelSelected}
                                            typeSurveySelected={this.state.typeSurveySelected}
                                            onLoadingStatus={this.handleProcessStopwords.bind(this)}
                                            onClick={refreshToken()} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </BlockUi>
            </div>
        );
    }

    // Props es un arreglo: [0] isLoadingDictionary
    handleProcessStopwords(props) {
        this.setState({
            isProcessingStopwords: props[0],
            loadingMessage: props[1]
        });
    }

    // Función que retorna la sección para carga de Stopwords
    renderDragAndDrop() {
        return (
            <input type="file" className="dropify drag-drop" onChange={this.fileChangedHandler}
                data-allowed-file-extensions="csv" data-height="202" />
        );
    }

    // Función que se invoca cuando se cambia el Diccionario insertado
    fileChangedHandler = event => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    // Función llamada cuando se cambia el período de tiempo seleccionado
    componentWillReceiveProps(nextProps) {
        // Esta condición no es necesaria, pero ayuda a prevenir un nuevo renderizado cuando las variables no cambian
        if (nextProps.countrySelected !== this.state.countrySelected) {
            this.setState({
                countrySelected: {
                    value: nextProps.countrySelected.value,
                    label: nextProps.countrySelected.label
                }
            });
        }
        if (nextProps.channelSelected !== this.state.channelSelected) {
            this.setState({
                channelSelected: {
                    value: nextProps.channelSelected.value,
                    label: nextProps.channelSelected.label
                }
            });
        }
        if (nextProps.typeSurveySelected !== this.state.typeSurveySelected) {
            this.setState({
                typeSurveySelected: {
                    value: nextProps.typeSurveySelected.value,
                    label: nextProps.typeSurveySelected.label
                }
            });
        }
    }

    handleOnProcessBtn(e) {
        if (this.state.countrySelected != null && this.state.channelSelected != null &&
            this.state.typeSurveySelected != null) {
            this.setState({
                textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros",
                /*                 countrySelected: null,
                                channelSelected: null,
                                typeSurveySelected: null,
                                channelOptions: [],
                                typeSurveyOptions: [] */
            },
                this.props.onClick([this.state.countrySelected, this.state.channelSelected,
                this.state.typeSurveySelected]));
        } else {
            swal({
                text: Constants.ALERT_OBLIG_FIELDS_TEXT,
                icon: Constants.ALERT_DEFAULT_INFO_ICON,
                button: Constants.ALERT_DEFAULT_OK_BUTTON,
            });
        }
        this.showFilter();

        // Se refresca token cuando el usuario procesa filtros
        refreshToken();
    }

    handleOnCleanFilterBtn() {
        this.setState({
            countrySelected: {
                value: null,
                label: null
            },
            channelSelected: {
                value: null,
                label: null
            },
            typeSurveySelected: {
                value: null,
                label: null
            },
            channelOptions: [],
            typeSurveyOptions: []
        });
    }

    // Método para cambiar estados de: *Mostrar filtros *Texto de botón de filtros
    showFilter() {
        this.setState({
            showFilter: !this.state.showFilter,
            textFilterButton: this.state.showFilter ? "Mostrar filtros" : "Ocultar filtros"
        });
    }

    resetSelectors = value => {
        this.setState(prevState => ({
            countrySelected: {
                value: null,
                label: null
            },
            channelSelected: null,
            typeSurveySelected: null,
            channelOptions: [],
            typeSurveyOptions: []
        }));
    };

    handleCountryChange = name => countryObject => {
        this.setState({
            countrySelected: {
                value: countryObject.value,
                label: countryObject.label
            },
            channelSelected: null,
            typeSurveySelected: null,
            channelOptions: [],
            typeSurveyOptions: []
        },
            function () {
                this.getChannelsByCountry(countryObject.value);
            });
    };

    handleChannelChange = name => channelObject => {
        this.setState({
            channelSelected: {
                value: channelObject.value,
                label: channelObject.label
            },
            typeSurveySelected: null,
            typeSurveyOptions: []
        },
            function () {
                this.getTypeSurveysByChannel();
            }
        );

    };

    handleTypeSurveyChange = name => typeSurveyObject => {
        this.setState({
            typeSurveySelected: {
                value: typeSurveyObject.value,
                label: typeSurveyObject.label
            }
        },
            function () {
                this.getTypeSurveysByChannel();
            }
        );
    };

    // Petición GET para obtener lista de Países
    getCountries() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.COUNTRIES_ENDPOINT + "?type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadCountries(json.filter.arrCountries);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de canales dado un País
    getChannelsByCountry() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.CHANNELS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value + "&type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadChannels(json.filter.arrChannels);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Petición GET para obtener lista de Tipos de Encuesta dados País y Canal
    getTypeSurveysByChannel() {
        const URL_FORMATTED = Constants.BASE_API_URL + Constants.SURVEYS_ENDPOINT +
            Constants.PARAM_COUNTRY_ID + this.state.countrySelected.value +
            Constants.PARAM_CHANNEL_ID + this.state.channelSelected.value + "&type=code";
        const authTokenID = getAuth().token_id;
        const HEADERS = { headers: { 'authorization-tigo': authTokenID, 'Cache-Control': 'no-cache' } }
        axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.loadTypeSurveys(json.filter.arrTypeSurveys);
                }
            }
            ).catch(error => {
                console.error(error);
            })
    }

    // Asignar estado a countryOptions
    loadCountries(arrCountries) {
        const arrCountryOptions = arrCountries.map(country => ({
            value: country.key,
            label: country.value
        }));
        this.setState({
            countryOptions: arrCountryOptions
        });
    }

    // Asignar estado a channelOptions
    loadChannels(arrChannels) {
        const arrChannelOptions = arrChannels.map(channel => ({
            value: channel.key,
            label: channel.value
        }));
        this.setState({
            channelOptions: arrChannelOptions
        });
    }

    // Asignar estado a typeSurveyOptions
    loadTypeSurveys(arrTypeSurveys) {
        const arrTypeSurveyOptions = arrTypeSurveys.map(typeSurvey => ({
            value: typeSurvey.key,
            label: typeSurvey.value
        }));
        this.setState({
            typeSurveyOptions: arrTypeSurveyOptions
        });
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getCountries();
        this.getChannelsByCountry();
        this.getTypeSurveysByChannel();
        $(".dropify").dropify({
            messages: {
                'default': Constants.DRAGANDDROP_DEFAULT,
                'replace': Constants.DRAGANDDROP_REPLACE,
                'remove': Constants.DRAGANDDROP_REMOVE,
                'error': Constants.DRAGANDDROP_ERROR
            },
            error: {
                'fileFormat': Constants.DRAGANDDROP_ERROR_FILE_FORMAT
            }
        });
    }
}
export default withRouter(CardFilter)