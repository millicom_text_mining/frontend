import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts, { color } from 'highcharts';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import { getAuth } from "../../context/auth";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';
import MessageError from '../message-error-chart/MessageErrorChart';

var topicData = undefined;

class TopicTotalsChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countrySelected: props.countrySelected,
            dynamicFilterSelected: props.dynamicFilterSelected, //Puede tomar el valor tipo encuesta o journey, dependiendo del path
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            isLoading: true
        };
        this.getTopicTotalsData = this.getTopicTotalsData.bind(this);
    }

    // Petición GET para obtener Tópicos con más Tipificación
    getTopicTotalsData() {
        // Se construye la URL
        var dynamicFilterURL = "";
        var isJourneyVoc = "";

        if (this.props.location.pathname === Constants.PATH_DASHBOARD ||
            this.props.location.pathname === Constants.PATH_DICTIONARY) {
                for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                    dynamicFilterURL = Constants.PARAM_SURVEY + encodeURIComponent(this.props.dynamicFilterSelected[i].label);
                }
        } else {
            if (this.props.dynamicFilterSelected !== null) {
                for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                    dynamicFilterURL = Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.props.dynamicFilterSelected[i].value);
                }
            }
            isJourneyVoc = Constants.JOURNEYS_ENDPOINT;
        }

        let URL_FORMATTED = Constants.BASE_API_URL + Constants.TOTAL_TOPICS_ENDPOINT +
            isJourneyVoc +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            dynamicFilterURL +
            Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected.value);

        if (this.props.segmentSelected !== null) {
            for (let i = 0; i < this.props.segmentSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENT + this.props.segmentSelected[i].value;
            }
        }
        if (this.props.productTypeSelected !== null) {
            for (let i = 0; i < this.props.productTypeSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_PRODUCT_TYPE + this.props.productTypeSelected[i].value;
            }
        }
        if (this.props.regionSelected !== null) {
            for (let i = 0; i < this.props.regionSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.regionSelected[i].value;
            }
        }
        if (this.props.contractSelected !== null) {
            for (let i = 0; i < this.props.contractSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_CONTRACT + this.props.contractSelected[i].value;
            }
        }
        if (this.props.municipioSelected !== null) {
            for (let i = 0; i < this.props.municipioSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_MUNICIPIO + this.props.municipioSelected[i].value;
            }
        }
        if (this.props.segmentB2BSelected !== null) {
            for (let i = 0; i < this.props.segmentB2BSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENTB2B + this.props.segmentB2BSelected[i].value;
            }
        }

        URL_FORMATTED = URL_FORMATTED + Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY ||
            this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        // Se captura el token actual
        const authToken = getAuth().token_id;

        // Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }

        // Se implementa la petición GET
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    // Se cargan los datos obtenidos
                    //this.loadJsonDataInStat(json.graphic);
                    this.insertColors(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    //Se asignan colores específicos a los tópicos
    insertColors(graphic) {
        let colors = ['#434348', '#87B4E7', '#A6EA8A', '#EBA668', '#8086E2', '#E1D369', '#E06681', '#4B8E8E'];
        for(let i = 0; i < graphic.topicsData.length; i++) {
            graphic.topicsData[i]['color'] = colors[i];
        }
        topicData = graphic.topicsData;
        this.loadJsonDataInStat(graphic);
    }

    // Método para cambiar estados del Gráfico y del Spinner
    loadJsonDataInStat(graphic) {
        // Se actualizan valores de la gráfica (xAxis y series) dentro de <charOptions> y se cambia estado isLoading
        this.setState({
            chartOptions: {
                chart: {
                    height: 400,
                    marginRight: 80,
                    marginLeft: 80
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG', 'downloadJPEG', 'downloadXLS']
                        }
                    }
                },
                title: {
                    text: ''
                },
                // Se actualiza <categories> de <xAxis> - Corresponde al eje de Período de Tiempo
                xAxis: [{
                    crosshair: true,
                    categories: graphic.periodTime
                }],
                yAxis: [{
                    // Eje principal
                    min: 0,
                    startOnTick: true,
                    endOnTick: true,
                    title: {
                        text: ''
                    },
                    labels: {
                        format: '{value}%'
                    }
                }],
                legend: {
                    align: 'center'
                },
                credits: {
                    enabled: false
                },
                // Se actualiza series - Corresponde al eje de los datos de los Tópicos
                series: graphic.topicsData,
                tooltip: {
                    valueSuffix: '%',
                    shared: true,
                    useHTML: true,
                    pointFormatter: function () {
                        var symbol = '';
                        if (this.graphic && this.graphic.symbolName) {
                            switch (this.graphic.symbolName) {
                                case 'circle':
                                    symbol = '●';
                                    break;
                                case 'diamond':
                                    symbol = '♦';
                                    break;
                                case 'square':
                                    symbol = '■';
                                    break;
                                case 'triangle':
                                    symbol = '▲';
                                    break;
                                case 'triangle-down':
                                    symbol = '▼';
                                    break;
                                default:
                                    break;
                            }
                        }
                        return '<span style="color:' + this.series.color + '">' + symbol + '</span>' + ' ' + this.series.name + ': ' + this.y + ' %' + '<br/>';
                    }
                }
            },
            // Se cambia estado para ocultar Spinner en render
            isLoading: false,
            graphicContainsData: true
        });
    }

    // Función llamada después de renderizado (Pertenece al ciclo de vida del Componente)
    componentDidMount() {
        this.getTopicTotalsData();
    }

    // Función llamada cuando se cambian los filtros
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.dynamicFilterSelected !== this.props.dynamicFilterSelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected !== this.props.segmentSelected
            || nextProps.productTypeSelected !== this.props.productTypeSelected
            || nextProps.regionSelected !== this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                dynamicFilterSelected: nextProps.dynamicFilterSelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoading: true
            }, function () { this.getTopicTotalsData() });
        }
    }

    render() {
        const { chartOptions } = this.state;
        // Decisión de renderizado para Spinner y Gráfico
        if (this.state.isLoading) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            )
        } else {
            if (this.state.graphicContainsData) {
                return (
                    <div>
                        <HighchartsReact
                            Highcharts={Highcharts}
                            options={chartOptions}
                        />
                    </div>
                );
            } else {
                return (
                    <MessageError />
                );
            }
        }
    }
}

export function getTopicsData() {
    return(topicData);
}

export default withRouter(TopicTotalsChart);