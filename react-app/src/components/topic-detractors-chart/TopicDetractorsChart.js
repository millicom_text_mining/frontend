import React, { Component } from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import * as Constants from '../../constants/constants';
import axios from 'axios';
import { getAuth } from "../../context/auth";
import Loader from 'react-loader-spinner';
import { withRouter } from 'react-router-dom';
import MessageError from '../message-error-chart/MessageErrorChart';
import { getTopicsData } from '../topic-totals/TopicTotalsChart';

class TopicDetractorsChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartOptions: null,
            countrySelected: props.countrySelected,
            dynamicFilterSelected: props.dynamicFilterSelected, //Puede tomar el valor tipo encuesta o journey, dependiendo del path
            periodTimeSelected: props.periodTimeSelected,
            segmentSelected: props.segmentSelected,
            productTypeSelected: props.productTypeSelected,
            regionSelected: props.regionSelected,
            contractSelected: props.contractSelected,
            municipioSelected: props.municipioSelected,
            segmentB2BSelected: props.segmentB2BSelected,
            isLoading: true
        };
        this.getTopicDetractorsData = this.getTopicDetractorsData.bind(this);
    }

    // Petición GET para obtener Tópicos con más Detractores
    getTopicDetractorsData() {
        //Se construye la URL
        var dynamicFilterURL = "";
        var isJourneyVoc = "";

        if (this.props.location.pathname === Constants.PATH_DASHBOARD ||
            this.props.location.pathname === Constants.PATH_DICTIONARY) {
                for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                    dynamicFilterURL += Constants.PARAM_SURVEY + encodeURIComponent(this.props.dynamicFilterSelected[i].label);
                }
        } else {
            isJourneyVoc = Constants.JOURNEYS_ENDPOINT;
            if (this.props.dynamicFilterSelected !== null) {
                for (let i = 0; i < this.props.dynamicFilterSelected.length; i++) {
                    dynamicFilterURL += Constants.PARAM_JOURNEY_ID + encodeURIComponent(this.props.dynamicFilterSelected[i].value);
                }
            }
        }

        let URL_FORMATTED = Constants.BASE_API_URL + Constants.TOTAL_TOPICS_DETRACTORS_ENDPOINT +
            isJourneyVoc +
            Constants.PARAM_COUNTRY + encodeURIComponent(this.props.countrySelected.label) +
            dynamicFilterURL +
            Constants.PARAM_PERIOD + encodeURIComponent(this.props.periodTimeSelected.value);

        if (this.props.segmentSelected !== null) {
            for (let i = 0; i < this.props.segmentSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENT + this.props.segmentSelected[i].value;
            }
        }
        if (this.props.productTypeSelected !== null) {
            for (let i = 0; i < this.props.productTypeSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_PRODUCT_TYPE + this.props.productTypeSelected[i].value;
            }
        }
        if (this.props.regionSelected !== null) {
            for (let i = 0; i < this.props.regionSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_REGION + this.props.regionSelected[i].value;
            }
        }
        if (this.props.contractSelected !== null) {
            for (let i = 0; i < this.props.contractSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_CONTRACT + this.props.contractSelected[i].value;
            }
        }
        if (this.props.municipioSelected !== null) {
            for (let i = 0; i < this.props.municipioSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_MUNICIPIO + this.props.municipioSelected[i].value;
            }
        }
        if (this.props.segmentB2BSelected !== null) {
            for (let i = 0; i < this.props.segmentB2BSelected.length; i++) {
                URL_FORMATTED += Constants.PARAM_SEGMENTB2B + this.props.segmentB2BSelected[i].value;
            }
        }

        URL_FORMATTED = URL_FORMATTED + Constants.PARAM_TYPE;

        // Se concatena el Tipo de ambiente (Pruebas o Producción)
        if (this.props.location.pathname === Constants.PATH_DICTIONARY ||
            this.props.location.pathname === Constants.PATH_DICTIONARY_JOURNEY) {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_TEST;
        } else {
            URL_FORMATTED = URL_FORMATTED + Constants.PARAM_DICTIONARY_TYPE_PRODUCTION;
        }

        //Se captura el token actual
        const authToken = getAuth().token_id;

        //Se construye HEADERS
        const HEADERS = { headers: { 'authorization-tigo': authToken, 'Cache-Control': 'no-cache' } }

        // Se implementa la petición GET
        return axios.get(URL_FORMATTED, HEADERS)
            .then(response => response.data)
            .then(json => {
                if (json.result.success) {
                    this.insertColors(json.graphic);
                    //this.loadJsonDataInStat(json.graphic);
                } else {
                    //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                    this.setState(
                        {
                            isLoading: false,
                            graphicContainsData: false
                        }
                    );
                }
            })
            .catch(error => {
                //Mostrar mensaje de fallo en descarga de datos y opción reintentar
                this.setState(
                    {
                        isLoading: false,
                        graphicContainsData: false
                    }
                );
            })
    }

    //Asignar colores a los tópicos de acuerdo a la grafica de Topicos Principales
    insertColors(graphic) {
        let principalTopicsData = getTopicsData();
        let arrColors = [];
        let color = "";
        for(let i = 0; i < graphic.topicsData.length; i++) {
            color = this.findElement(principalTopicsData, graphic.topicsData[i].name);
            arrColors.push(color);
        }
        for(let i = 0; i < graphic.topicsData.length; i++) {
            graphic.topicsData[i]['color'] = arrColors[i];
        }
        this.loadJsonDataInStat(graphic);
    }  

    //Encontrar el color del topico (name)
    findElement(arr, name) {
        let color = "";
        let colors = ['#5858FA', '#31B404', '#D7DF01', '#04B4AE', '#2E64FE', '#F781F3', '#F78181', '#58D3F7'];
        let j = 0;
        for(var i = 0; i < arr.length; i++) {
            if(arr[i].name === name) {
                color = arr[i].color;
                break;
            } else {
                color = colors[j];
                j++;
            }
        }
        return color;
    }

    // Método para cambiar estados del Gráfico y del Spinner
    loadJsonDataInStat(graphic) {
        // Se declaran arreglos para iteración de JSON
        let arrTopicsData = [];
        for (let index = 0; index < graphic.topicsData.length; index++) {
            const topic = graphic.topicsData[index];
            arrTopicsData.push({
                name: topic.name,
                data: topic.data,
                color: topic.color,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[10],
                    fillColor: Highcharts.getOptions().colors[10]
                },
                dataLabels: {
                    enabled: false
                },
                yAxis: 0
            });
        }
        // Se asignan los detractores en última posición
        arrTopicsData.push({
            name: 'Peso detractores',
            data: graphic.weightDetractors,
            color: '#eb002a',
            marker: {
                lineWidth: 2,
                //lineColor: Highcharts.getOptions().colors[10],
                lineColor: '#eb002a',
                fillColor: 'white'
            },
            dataLabels: {
                enabled: true,
                format: '{point.y} %'
            },
            yAxis: 1
        });

        this.setState({
            chartOptions: {
                chart: {
                    height: 400,
                    marginRight: 80,
                    marginLeft: 80
                },
                // Opción para exportar gráfica
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG', 'downloadJPEG', 'downloadXLS']
                        }
                    }
                },
                title: {
                    text: ''
                },
                xAxis: [{
                    categories: graphic.periodTime,
                    crosshair: true
                }],
                yAxis: [{
                    // Eje principal
                    min: 0,
                    startOnTick: false,
                    endOnTick: true,
                    title: {
                        text: ''
                    },
                    labels: {
                        format: '{value: .0f}%',
                    }
                }, {
                    // Eje secundario    
                    min: -20,
                    max: Math.max.apply(Math, arrTopicsData[arrTopicsData.length - 1].data) + 0.10,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    gridLineColor: 'rgba(0, 0, 0, 0)',
                    startOnTick: false,
                    endOnTick: false,
                    labels: {
                        format: '{value}%',
                        style: {
                            //color: Highcharts.getOptions().colors[0]
                            color: 'rgba(0, 0, 0, 0)'
                        }
                    },
                    opposite: true
                }],
                credits: {
                    enabled: false
                },
                legend: {
                    align: 'center'
                },
                series: arrTopicsData,
                tooltip: {
                    valueSuffix: '%',
                    shared: true,
                    useHTML: true,
                    pointFormatter: function () {
                        var symbol = '';
                        if (this.graphic && this.graphic.symbolName) {
                            switch (this.graphic.symbolName) {
                                case 'circle':
                                    symbol = '●';
                                    break;
                                case 'diamond':
                                    symbol = '♦';
                                    break;
                                case 'square':
                                    symbol = '■';
                                    break;
                                case 'triangle':
                                    symbol = '▲';
                                    break;
                                case 'triangle-down':
                                    symbol = '▼';
                                    break;
                                default:
                                    break;
                            }
                        }
                        return '<span style="color:' + this.series.color + '">' + symbol + '</span>' + ' ' + this.series.name + ': ' + this.y + ' %' + '<br/>';
                    }
                }
            },
            isLoading: false,
            graphicContainsData: true
        });
    }

    // Función llamada después de renderizado
    componentDidMount() {
        this.getTopicDetractorsData();
    }

    // Función llamada cuando se cambian los valores de Filtro
    componentWillReceiveProps(nextProps) {
        if (nextProps.countrySelected !== this.props.countrySelected
            || nextProps.dynamicFilterSelected !== this.props.dynamicFilterSelected
            || nextProps.periodTimeSelected !== this.props.periodTimeSelected
            || nextProps.segmentSelected !== this.props.segmentSelected
            || nextProps.productTypeSelected !== this.props.productTypeSelected
            || nextProps.regionSelected !== this.props.regionSelected
            || nextProps.contractSelected !== this.props.contractSelected
            || nextProps.municipioSelected !== this.props.municipioSelected
            || nextProps.segmentB2BSelected !== this.props.segmentB2BSelected) {
            this.setState({
                countrySelected: nextProps.countrySelected,
                dynamicFilterSelected: nextProps.dynamicFilterSelected,
                periodTimeSelected: nextProps.periodTimeSelected,
                segmentSelected: nextProps.segmentSelected,
                productTypeSelected: nextProps.productTypeSelected,
                regionSelected: nextProps.regionSelected,
                contractSelected: nextProps.contractSelected,
                municipioSelected: nextProps.municipioSelected,
                segmentB2BSelected: nextProps.segmentB2BSelected,
                isLoading: true
            }, function () { this.getTopicDetractorsData() });
        }
    }

    render() {
        const { chartOptions } = this.state;
        // Decisión de renderizado para Spinner y Gráfico
        if (this.state.isLoading) {
            return (
                <div className="text-center">
                    <Loader
                        type={Constants.DEFAULT_SPINNER_TYPE}
                        color={Constants.DEFAULT_SPINNER_COLOR}
                        height={Constants.DEFAULT_SPINNER_HEIGHT}
                    />
                </div>
            )
        } else {
            if (this.state.graphicContainsData) {
                return (
                    <div>
                        <HighchartsReact
                            Highcharts={Highcharts}
                            options={chartOptions}
                        />
                    </div>
                );
            } else {
                return (
                    <MessageError />
                );
            }
        }
    }
}

export default withRouter(TopicDetractorsChart)