import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import swal from 'sweetalert';
import './App.css';
import ChangePassword from './components/login/ChangePassword';
import LoginForm from './components/login/LoginForm';
import PageHeader from './components/page-header/PageHeader';
import * as Constants from './constants/constants';
import { AuthContext, clearLocalStorage, getAuth/*, refreshToken*/ } from './context/auth';
import Dashboard from './pages/Dashboard';
import Dictionary from './pages/dictionary/Dictionary';
import PrivateEditorRoute from './routing/PrivateEditorRoute';
import PrivateRoute from './routing/PrivateRoute';
import DictionaryJourney from './pages/DictionaryJourney';
import Journey from './pages/Journey';
import Stopwords from './pages/stopwords/Stopwords';

function App() {
  // React Hooks para controlar estados de variables locales
  const [authToken, setAuthToken] = useState();
  //const [notificationState, setNotificationState] = useState(true);

  // Función para guardar Token en LocalStorage
  const setToken = (data) => {
    localStorage.setItem(Constants.TOKEN, JSON.stringify(data));
    setAuthToken(data);
  }

  // Función llamada para determinar si se renderiza PageHeader e inicia TimeOut para validación Token
  function validateToken() {
    if (getAuth() != null) {
      // Cada segundo se invoca el método checkUserSession
      window.setInterval(checkUserSession, 1000);
      if (getAuth().token.user.change_password === 0) {
        return <PageHeader />;
      }
    }
  }

  // Método llamado cada segundo para verificar si alerta 2 minutos antes o cierra sesión
  function checkUserSession() {
    //showAlertSession();
    automaticLogout();
  }

  // Método que ejecuta cierre de sesión cuando expira token
  function automaticLogout() {
    // Se toma fecha actual
    let currentDateTime = new Date().getTime();
    // Se verifica que el usuario tenga Token
    if (getAuth() != null) {
      // convertir la fecha del token
      let tokenExpDate = new Date(getAuth().token.exp * 1000);
      // Validar si el token se venció para cerrar la sesión
      if (tokenExpDate <= currentDateTime) {
        // Se despliega alerta de Token caducado
        swal({
          text: Constants.ALERT_EXPIRED_TOKEN_TEXT,
          icon: Constants.ALERT_DEFAULT_WARNING_ICON,
          button: Constants.ALERT_DEFAULT_OK_BUTTON
        }).then(() => {
          // Se cierra sesión del usuario
          clearLocalStorage();
        });
      }
    }
  }

  // Método para desplegar alerta faltando 2 minutos para vencimiento de token
  /*function showAlertSession() {
    console.log("showAlertSession");
    // Se valida si el usuario está autenticado
    if (getAuth() != null) {
      // Se toma fecha actual
      let currentDateTime = new Date().getTime();
      // Se toma hora de alerta de sesión
      let alertTime = getTimeToAlertUserSession(getAuth().token.exp);
      // Validar si es tiempo de alertar
      if (alertTime <= currentDateTime && notificationState) {
        setNotificationState(false);
        // Se despliega alerta
        swal({
          text: Constants.ALERT_TOKEN_WILL_EXPIRE_TEXT,
          icon: Constants.ALERT_DEFAULT_WARNING_ICON,
          buttons: ["Cancelar", Constants.ALERT_DEFAULT_CONTINUE_BUTTON]
        }).then((continueSession) => {
          if (continueSession) {
            // Se actualiza Token
            setNotificationState(true);
            refreshToken();
          } else {
            setNotificationState(false);
          }
        });
      }
    }
  }*/

  // Se restan 2 minutos a la hora de expiración de Token para definir tiempo de alerta
  /*function getTimeToAlertUserSession(timestamp) {
    // Se convierte timestamp a Date
    var date = new Date(timestamp * 1000);
    // Se retorna la fecha de expiración de token - 2 minutos
    return date.setMinutes(date.getMinutes() - 2);
  }*

  // TEST Método para reducir tiempos de Token
  /*function setTimeExpTest(timestamp) {
    // Se convierte timestamp a Date
    var date = new Date(timestamp * 1000);
    // Se retorna la fecha de expiración de token - 6 minutos
    return date.setMinutes(date.getMinutes() - 6);
  }*/

  return (
    //AuthContext se utiliza para controlar variables globales en el proyecto, investigar createContext y useContext de React
    <AuthContext.Provider value={{ authToken, setAuthToken: setToken }}>
      <BrowserRouter>
        {validateToken()}
        <Switch>
          <PrivateRoute exact path={Constants.PATH_DASHBOARD} component={Dashboard} />
          <PrivateEditorRoute exact path={Constants.PATH_JOURNEY} component={Journey} />
          <PrivateEditorRoute exact path={Constants.PATH_DICTIONARY_JOURNEY} component={DictionaryJourney} />
          <PrivateEditorRoute exact path={Constants.PATH_DICTIONARY} component={Dictionary} />
          <PrivateEditorRoute exact path={Constants.PATH_STOPWORDS} component={Stopwords} />
          <Route path={Constants.PATH_LOGIN} component={LoginForm} />
          <Route path={Constants.PATH_CHANGE_PASS} component={ChangePassword} />
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}
export default App;